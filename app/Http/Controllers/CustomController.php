<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Wig;
use App\Lead;

class CustomController extends Controller
{
    public function saveProgressWig(Request $request){
        // dd($request);

        $result = DB::table('tr_lag')
                    ->select('*')
                    ->where('id_ma_lag', $request->id_ma_lag)
                    ->get();

        $values = array();
        foreach($result as $item){
            $_post_target = 'target_'.$item->id;
            $_post_realisasi = 'realisasi_'.$item->id;
            
            array_push($values, $item->id);
            array_push($values, $request->$_post_target);
            array_push($values, $request->$_post_realisasi);
        }

        // dd($values);

        $questions = implode(',', array_fill(0, count($values)/3, '(?,?,?)'));
        $query = DB::statement("
          INSERT INTO
              tr_lag (id, target, realisasi)
          VALUES
              $questions
          ON DUPLICATE KEY UPDATE
              id = VALUES(id),
              target = VALUES(target),
              realisasi = VALUES(realisasi)
          ", $values);

          if($query){
              return redirect(url('/admin/progress/add'));
          }
    }

    public function saveProgressLead(Request $request){
        // dd($request);

        $result = DB::table('tr_lead')
                    ->select('*')
                    ->where('id_ma_lead', $request->id_ma_lead)
                    ->get();
        // dd($result);

        $values = array();
        foreach($result as $item){
            $_post_target = 'target_'.$item->id;
            $_post_realisasi = 'realisasi_'.$item->id;
            
            array_push($values, $item->id);
            array_push($values, $request->$_post_target);
            array_push($values, $request->$_post_realisasi);
        }

        // dd($values);

        $questions = implode(',', array_fill(0, count($values)/3, '(?,?,?)'));
        $query = DB::statement("
          INSERT INTO
              tr_lead (id, target, realisasi)
          VALUES
              $questions
          ON DUPLICATE KEY UPDATE
              id = VALUES(id),
              target = VALUES(target),
              realisasi = VALUES(realisasi)
          ", $values);

          if($query){
              return redirect(url('/admin/progress/add'));
          }
    }

    public function getWig($id){
        $result = DB::table('tr_lag')
                    ->select('tr_lag.*', 'ma_satuan.label', 'ma_tipe_laporan.tipe_laporan')
                    ->join('ma_lag', 'ma_lag.id', '=', 'tr_lag.id_ma_lag')
                    ->join('ma_satuan', 'ma_lag.id_ma_satuan', '=', 'ma_satuan.id')
                    ->join('ma_tipe_laporan', 'ma_lag.id_ma_tipe_laporan', '=', 'ma_tipe_laporan.id')
                    ->where('tr_lag.id_ma_lag', $id)
                    ->orderby('tanggal', 'desc')
                    ->get();

        

        if(sizeOf($result) > 0){
            $data['api_message'] = 'success';
            $data['api_status'] = 1;
            $data['data'] = $result;
            return $data;
        }else{
            $data['api_message'] = 'failed';
            $data['api_status'] = 0;
            $data['data'] = [];
            return $data;
        }
    }

    public function getLead($id){
        $result = DB::table('tr_lead')
                     ->select('tr_lead.*', 'ma_satuan.label', 'ma_tipe_laporan.tipe_laporan')
                    ->join('ma_lead', 'ma_lead.id', '=', 'tr_lead.id_ma_lead')
                    ->join('ma_satuan', 'ma_lead.id_ma_satuan', '=', 'ma_satuan.id')
                    ->join('ma_tipe_laporan', 'ma_lead.id_ma_tipe_laporan', '=', 'ma_tipe_laporan.id')
                    ->where('tr_lead.id_ma_lead', $id)
                    ->orderby('tanggal', 'desc')
                    ->get();

        if(sizeOf($result) > 0){
            $data['api_message'] = 'success';
            $data['api_status'] = 1;
            $data['data'] = $result;
            return $data;
        }else{
            $data['api_message'] = 'failed';
            $data['api_status'] = 0;
            $data['data'] = [];
            return $data;
        }
    }

    public function getWigLead($unit_id){
        $wig = Wig::where('id_ma_unit', $unit_id)
                    ->whereNull('id_ma_area')
                    ->whereNull('id_ma_rayon')
                    ->get();
                    
        for($i=0;$i<sizeof($wig);$i++){
            for($j=0;$j<sizeof($wig[$i]->lead);$j++){
                $query = DB::select('select a.realisasi/b.target*100 as pencapaian from (select realisasi from tr_lead
                            where id_ma_lead = '.$wig[$i]->lead[$j]->id.' and realisasi is not null
                            order by tanggal desc
                            limit 1) a,(
                            select target from tr_lead
                            where id_ma_lead = 1 and target is not null
                            order by tanggal desc
                            limit 1) b');
        
                $wig[$i]->lead[$j]->pencapaian = $query[0]->pencapaian;
            }
            $wig[$i]->lead = $wig[$i]->lead;
            $wig[$i]->goto = "<button class='btn-xs btn-primary'><i class='fa fa-eye'></i></button>";
        }

        $data = Array("data" => $wig);
        return $data;

    }
}
