<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $table = 'ma_lead';

    public function wig()
    {
        return $this->belongsTo('App\Wig', 'id_ma_lag');
    }
}
