<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wig extends Model
{
    protected $table = 'ma_lag';

    public function lead()
    {
        return $this->hasMany('App\Lead', 'id_ma_lag');
    }
}
