-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 09:00 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `four_dx`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_apicustom`
--

INSERT INTO `cms_apicustom` (`id`, `permalink`, `tabel`, `aksi`, `kolom`, `orderby`, `sub_query_1`, `sql_where`, `nama`, `keterangan`, `parameter`, `created_at`, `updated_at`, `method_type`, `parameters`, `responses`) VALUES
(1, 'gettargetwig', 'tr_lag', 'list', NULL, NULL, NULL, NULL, 'getTargetWig', NULL, NULL, NULL, NULL, 'get', 'a:5:{i:0;a:5:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:1;a:5:{s:4:\"name\";s:9:\"id_ma_lag\";s:4:\"type\";s:7:\"integer\";s:6:\"config\";N;s:8:\"required\";s:1:\"1\";s:4:\"used\";s:1:\"1\";}i:2;a:5:{s:4:\"name\";s:7:\"tanggal\";s:4:\"type\";s:4:\"date\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:3;a:5:{s:4:\"name\";s:6:\"target\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:4;a:5:{s:4:\"name\";s:9:\"realisasi\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}}', 'a:9:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:1;a:4:{s:4:\"name\";s:9:\"id_ma_lag\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:18:\"ma_lag_is_positive\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:3;a:4:{s:4:\"name\";s:11:\"ma_lag_nama\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:4;a:4:{s:4:\"name\";s:17:\"ma_lag_start_date\";s:4:\"type\";s:4:\"date\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:5;a:4:{s:4:\"name\";s:15:\"ma_lag_end_date\";s:4:\"type\";s:4:\"date\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:6;a:4:{s:4:\"name\";s:7:\"tanggal\";s:4:\"type\";s:4:\"date\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:7;a:4:{s:4:\"name\";s:6:\"target\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:8;a:4:{s:4:\"name\";s:9:\"realisasi\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}}'),
(2, 'gettargetlead', 'tr_lead', 'list', NULL, NULL, NULL, NULL, 'getTargetLead', NULL, NULL, NULL, NULL, 'get', 'a:5:{i:0;a:5:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:1;a:5:{s:4:\"name\";s:10:\"id_ma_lead\";s:4:\"type\";s:7:\"integer\";s:6:\"config\";N;s:8:\"required\";s:1:\"1\";s:4:\"used\";s:1:\"1\";}i:2;a:5:{s:4:\"name\";s:7:\"tanggal\";s:4:\"type\";s:4:\"date\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:3;a:5:{s:4:\"name\";s:6:\"target\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:4;a:5:{s:4:\"name\";s:9:\"realisasi\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}}', 'a:9:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:1;a:4:{s:4:\"name\";s:10:\"id_ma_lead\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:19:\"ma_lead_is_positive\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:3;a:4:{s:4:\"name\";s:12:\"ma_lead_nama\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:4;a:4:{s:4:\"name\";s:18:\"ma_lead_start_date\";s:4:\"type\";s:4:\"date\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:5;a:4:{s:4:\"name\";s:16:\"ma_lead_end_date\";s:4:\"type\";s:4:\"date\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:6;a:4:{s:4:\"name\";s:7:\"tanggal\";s:4:\"type\";s:4:\"date\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:7;a:4:{s:4:\"name\";s:6:\"target\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:8;a:4:{s:4:\"name\";s:9:\"realisasi\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}}');

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2018-10-20 20:17:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-20 20:21:10', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/statistic_builder/add-save', 'Add New Data Map at Statistic Builder', '', 1, '2018-10-20 20:23:00', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/add-save', 'Add New Data Strategic Map at Menu Management', '', 1, '2018-10-20 20:27:07', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/1', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>is_dashboard</td><td>0</td><td>1</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 20:29:55', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/1', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 20:35:28', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/1', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>StrategicMapController@showMap</td><td>StrategicMapController</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 20:36:17', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/1', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 20:36:38', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/add-save', 'Add New Data Strategic Map at Menu Management', '', 1, '2018-10-20 20:39:54', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/2', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>StrategicMapController@index</td><td>AdminCmsUsersController</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 20:43:00', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/2', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Controller & Method</td><td>Route</td></tr><tr><td>path</td><td>AdminCmsUsersController</td><td>showMap</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 20:43:40', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/delete/2', 'Delete data Strategic Map at Menu Management', '', 1, '2018-10-20 20:46:43', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/add-save', 'Add New Data Strategic Map at Menu Management', '', 1, '2018-10-20 20:49:00', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/map/add-save', 'Add New Data 2 at map', '', 1, '2018-10-20 21:01:46', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/map/add-save', 'Add New Data 2 at map', '', 1, '2018-10-20 21:03:30', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/4', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Controller & Method</td><td>Route</td></tr><tr><td>path</td><td>AdminMapController@showMap</td><td>showMap</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 21:09:49', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/4', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>showMap</td><td>admin/showMap</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 21:10:10', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/delete/3', 'Delete data map at Menu Management', '', 1, '2018-10-20 21:10:17', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/delete/4', 'Delete data Strategic Map at Menu Management', '', 1, '2018-10-20 21:10:20', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/module_generator/delete/12', 'Delete data map at Module Generator', '', 1, '2018-10-20 21:10:26', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/add-save', 'Add New Data Map at Menu Management', '', 1, '2018-10-20 21:18:53', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/5', 'Update data Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Controller & Method</td><td>Route</td></tr><tr><td>path</td><td>AdminMap13Controller</td><td>AdminMap13ControllerGetIndex</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 21:19:21', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/5', 'Update data Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Route</td><td>Controller & Method</td></tr><tr><td>path</td><td>AdminMap13ControllerGetIndex</td><td>AdminMap13Controller</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 21:33:17', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/5', 'Update data Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Controller & Method</td><td>URL</td></tr><tr><td>path</td><td>AdminMap13Controller</td><td>map</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 21:43:18', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/5', 'Update data Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>map</td><td>/map</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2018-10-20 21:43:40', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/module_generator/delete/13', 'Delete data Map at Module Generator', '', 1, '2018-10-20 21:43:59', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-21 19:53:26', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-22 07:05:20', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/6', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>AdminMap14ControllerGetIndex</td><td>/map</td></tr><tr><td>color</td><td></td><td>normal</td></tr></tbody></table>', 1, '2018-10-22 07:39:17', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/6', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>/map</td><td>map</td></tr></tbody></table>', 1, '2018-10-22 07:39:38', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/6', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>map</td><td>/map</td></tr></tbody></table>', 1, '2018-10-22 07:39:54', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/module_generator/delete/14', 'Delete data Strategic Map at Module Generator', '', 1, '2018-10-22 07:40:14', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Strategic Map at Menu Management', '', 1, '2018-10-22 07:41:59', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/delete/7', 'Delete data Strategic Map at Menu Management', '', 1, '2018-10-22 07:43:09', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data tes at Menu Management', '', 1, '2018-10-22 07:43:25', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_companycode/add-save', 'Add New Data  at Company Code', '', 1, '2018-10-22 07:46:35', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_businessarea/add-save', 'Add New Data Kantor Pusat at Business Area', '', 1, '2018-10-22 07:49:59', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_businessarea/delete/1', 'Delete data Kantor Pusat at Business Area', '', 1, '2018-10-22 07:54:22', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_businessarea/add-save', 'Add New Data Kantor Pusat at Business Area', '', 1, '2018-10-22 07:54:32', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_businessarea/delete/2', 'Delete data Kantor Pusat at Business Area', '', 1, '2018-10-22 07:55:56', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_businessarea/add-save', 'Add New Data Kantor Pusat at Business Area', '', 1, '2018-10-22 07:56:06', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_businessarea/delete/3', 'Delete data Kantor Pusat at Business Area', '', 1, '2018-10-22 07:57:35', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_businessarea/add-save', 'Add New Data Kantor Pusat at Business Area', '', 1, '2018-10-22 07:57:42', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/add-save', 'Add New Data DIVSTI at Unit', '', 1, '2018-10-22 08:02:55', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-22 17:55:14', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/module_generator/delete/15', 'Delete data Company Code at Module Generator', '', 1, '2018-10-22 18:03:38', NULL),
(46, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/module_generator/delete/16', 'Delete data Business Area at Module Generator', '', 1, '2018-10-22 18:03:41', NULL),
(47, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/add-save', 'Add New Data Wilayah Sumatera Barat at Unit', '', 1, '2018-10-22 18:29:09', NULL),
(48, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/delete/1', 'Delete data Distribusi Jakarta Raya at Unit', '', 1, '2018-10-22 18:29:14', NULL),
(49, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_area/add-save', 'Add New Data Area Pariaman at Area', '', 1, '2018-10-22 19:40:04', NULL),
(50, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Data Master at Menu Management', '', 1, '2018-10-22 19:41:19', NULL),
(51, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/13', 'Update data Data Master at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Module</td><td>URL</td></tr><tr><td>path</td><td>ma_area</td><td>/</td></tr></tbody></table>', 1, '2018-10-22 19:42:04', NULL),
(52, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_rayon/add-save', 'Add New Data Rayon Pariaman I at Rayon', '', 1, '2018-10-22 19:45:55', NULL),
(53, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/13', 'Update data Data Master at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>/</td><td>/master</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2018-10-22 20:22:22', NULL),
(54, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/add-save', 'Add New Data  at Satuan', '', 1, '2018-10-22 20:22:43', NULL),
(55, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/add-save', 'Add New Data  at Satuan', '', 1, '2018-10-22 20:22:51', NULL),
(56, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_tipe_laporan/add-save', 'Add New Data  at Tipe Laporan', '', 1, '2018-10-22 20:23:03', NULL),
(57, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_tipe_laporan/add-save', 'Add New Data  at Tipe Laporan', '', 1, '2018-10-22 20:23:10', NULL),
(58, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_tipe_laporan/add-save', 'Add New Data  at Tipe Laporan', '', 1, '2018-10-22 20:23:14', NULL),
(59, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_indikator/add-save', 'Add New Data Meningkatkan Pendapatan at Indikator', '', 1, '2018-10-22 20:35:14', NULL),
(60, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/17', 'Update data Lag Measure at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Indikator</td><td>Lag Measure</td></tr><tr><td>color</td><td></td><td>normal</td></tr></tbody></table>', 1, '2018-10-22 20:45:29', NULL),
(61, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/module_generator/delete/23', 'Delete data Lead Measure at Module Generator', '', 1, '2018-10-22 20:55:03', NULL),
(62, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead24/add-save', 'Add New Data Pengadaan Meter at Lead Measure', '', 1, '2018-10-22 21:01:32', NULL),
(63, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/edit-save/1', 'Update data Meningkatkan Pendapatan at Lag Measure', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2018-10-22 21:12:41', NULL),
(64, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/add-save', 'Add New Data Kantor Pusat at Unit', '', 1, '2018-10-23 00:24:00', NULL),
(65, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/add-save', 'Add New Data Distribusi Bali at Unit', '', 1, '2018-10-23 00:24:32', NULL),
(66, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_area/add-save', 'Add New Data Area Denpasar at Area', '', 1, '2018-10-23 00:24:59', NULL),
(67, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_rayon/add-save', 'Add New Data Rayon Renon at Rayon', '', 1, '2018-10-23 00:25:15', NULL),
(68, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/add-save', 'Add New Data  at Satuan', '', 1, '2018-10-23 00:26:06', NULL),
(69, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/edit-save/3', 'Update data  at Satuan', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>label</td><td>kWh</td><td>kwh</td></tr></tbody></table>', 1, '2018-10-23 00:26:15', NULL),
(70, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/add-save', 'Add New Data Penurunan Susut at Lag Measure', '', 1, '2018-10-23 00:26:50', NULL),
(71, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/add-save', 'Add New Data  at Satuan', '', 1, '2018-10-23 00:27:13', NULL),
(72, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead24/add-save', 'Add New Data Pengecekan Jaringan at Lead Measure', '', 1, '2018-10-23 00:27:50', NULL),
(73, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-23 07:01:33', NULL),
(74, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/21', 'Update data Strategic Map at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>is_dashboard</td><td>0</td><td>1</td></tr></tbody></table>', 1, '2018-10-23 07:06:30', NULL),
(75, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/users/edit-save/1', 'Update data Super Admin at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2018-10/dfg.jpg</td></tr><tr><td>password</td><td>$2y$10$jClAyN6utmxZWUfoQLepR.ltRxLeFD8WbvsRXp/z2kCqqby6IBDMy</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2018-10-23 07:09:00', NULL),
(76, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2018-10-23 07:09:12', NULL),
(77, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-23 07:09:14', NULL),
(78, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/19', 'Update data Lead Measure at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-music</td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2018-10-23 07:09:41', NULL),
(79, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/13', 'Update data Data Master at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-database</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2018-10-23 07:09:52', NULL),
(80, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/20', 'Update data Update Progress at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-star</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2018-10-23 07:10:12', NULL),
(81, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-23 18:12:57', NULL),
(82, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/add-save', 'Add New Data Wilayah Maluku dan Maluku Utara at Unit', '', 1, '2018-10-23 18:29:17', NULL),
(83, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/add-save', 'Add New Data Kantor Induk Distribusi Banten at Unit', '', 1, '2018-10-23 18:33:27', NULL),
(84, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/edit-save/6', 'Update data Unit Induk Distribusi Banten at Unit', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>nama</td><td>Kantor Induk Distribusi Banten</td><td>Unit Induk Distribusi Banten</td></tr></tbody></table>', 1, '2018-10-23 18:34:08', NULL),
(85, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/delete/4', 'Delete data Distribusi Bali at Unit', '', 1, '2018-10-23 18:34:13', NULL),
(86, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_unit/delete/3', 'Delete data Kantor Pusat at Unit', '', 1, '2018-10-23 18:34:17', NULL),
(87, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/delete/1', 'Delete data Meningkatkan Pendapatan at Lag Measure', '', 1, '2018-10-23 18:34:28', NULL),
(88, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/delete/2', 'Delete data Penurunan Susut at Lag Measure', '', 1, '2018-10-23 18:34:32', NULL),
(89, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/add-save', 'Add New Data  at Satuan', '', 1, '2018-10-23 18:37:55', NULL),
(90, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/add-save', 'Add New Data Meningkatkan Penjualan dan Pendapatan Tenaga Listrik 17,11 TWh menjadi 23,56 TWh pada 31 Desember 2018 at Lag Measure', '', 1, '2018-10-23 18:38:39', NULL),
(91, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/add-save', 'Add New Data  at Satuan', '', 1, '2018-10-23 18:40:03', NULL),
(92, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/add-save', 'Add New Data Meningkatkan Pendapatan dari Rp 18,8 Triliun menjadi Rp 26,289 Triliun pada 31 Desember 2018 at Lag Measure', '', 1, '2018-10-23 18:40:27', NULL),
(93, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/add-save', 'Add New Data Menurunkan COP 32,06 hari menjadi 25 hari pada 31 Desember 2018 at Lag Measure', '', 1, '2018-10-23 18:41:29', NULL),
(94, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead/delete/3', 'Delete data Pengecekan Jaringan at Lead Measure', '', 1, '2018-10-23 18:41:42', NULL),
(95, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead/delete/2', 'Delete data Pengadaan Meter at Lead Measure', '', 1, '2018-10-23 18:41:45', NULL),
(96, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_satuan/add-save', 'Add New Data  at Satuan', '', 1, '2018-10-23 18:59:03', NULL),
(97, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-23 18:59:22', NULL),
(98, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/add-save', 'Add New Data Meningkatkan jumlah pelanggan layana khusus premium 141 pelanggan bundling dan 197 murni pada September 2018 menjadi 200 bundling dan 360 pelanggan murni pada 31 Desember 2018 at Lag Measure', '', 1, '2018-10-23 18:59:46', NULL),
(99, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead/add-save', 'Add New Data Melaksanakan monitoring kajian kelayakan dan persetujuan GM 1 kali setiap Senin oleh masing - masing PIC Region Banten dan Tangerang sesuai matrix layanan khusus dan SOP Premium Bundling at Lead Measure', '', 1, '2018-10-23 19:05:04', NULL),
(100, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead/add-save', 'Add New Data Melaksanakan monitoring penginputan dan pengesahan master tarif premium akurasi data pelanggan layanan premium setiap minggu ke-4 oleh AE dan DM sesuai persetujuan GM dan PJBTL at Lead Measure', '', 1, '2018-10-23 19:24:28', NULL),
(101, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lag/add-save', 'Add New Data Menurunkan daftar tunggu untuk segment TR dari 23,262 menjadi 0 pada 31 Desember 2018 at Lag Measure', '', 1, '2018-10-23 19:25:47', NULL),
(102, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead/add-save', 'Add New Data Melaksanakan monitoring laporan kendala penyebab daftar tunggu rapat satu minggu sekali, pantauan group WA, laporan DAFTUNG mingguan sesuai AP2T at Lead Measure', '', 1, '2018-10-23 19:27:21', NULL),
(103, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/ma_lead/add-save', 'Add New Data Melaksanakan monitoring ketersediaan material LPB rapat bulanan sesuai aplikasi Gudang Online dan SAP at Lead Measure', '', 1, '2018-10-23 19:28:28', NULL),
(104, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-23 20:47:58', NULL),
(105, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/11', 'Update data Unit at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-gear</td></tr><tr><td>parent_id</td><td>13</td><td></td></tr></tbody></table>', 1, '2018-10-23 20:55:39', NULL),
(106, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/12', 'Update data Area at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-gears</td></tr><tr><td>parent_id</td><td>13</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2018-10-23 20:56:01', NULL),
(107, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/14', 'Update data Rayon at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-gear</td></tr><tr><td>parent_id</td><td>13</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2018-10-23 20:56:20', NULL),
(108, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/15', 'Update data Satuan at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-gears</td></tr><tr><td>parent_id</td><td>13</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2018-10-23 20:56:29', NULL),
(109, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/16', 'Update data Tipe Laporan at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-gear</td></tr><tr><td>parent_id</td><td>13</td><td></td></tr><tr><td>sorting</td><td>5</td><td></td></tr></tbody></table>', 1, '2018-10-23 20:56:41', NULL),
(110, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/12', 'Update data Area at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-gears</td><td>fa fa-gear</td></tr><tr><td>parent_id</td><td>13</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2018-10-23 20:57:01', NULL),
(111, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/15', 'Update data Satuan at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-gears</td><td>fa fa-gear</td></tr><tr><td>parent_id</td><td>13</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2018-10-23 20:57:12', NULL),
(112, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/17', 'Update data Wild Important Goals at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Lag Measure</td><td>Wild Important Goals</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2018-10-23 23:56:33', NULL),
(113, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-24 18:39:19', NULL),
(114, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data asd at Wild Important Goals', '', 1, '2018-10-25 00:31:08', NULL),
(115, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data asd at Wild Important Goals', '', 1, '2018-10-25 01:46:54', NULL),
(116, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 01:47:59', NULL),
(117, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data asdasd at Wild Important Goals', '', 1, '2018-10-25 02:02:54', NULL),
(118, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data tes_1 at Wild Important Goals', '', 1, '2018-10-25 02:03:40', NULL),
(119, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 02:05:33', NULL),
(120, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 02:05:59', NULL),
(121, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 02:06:31', NULL),
(122, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 02:08:27', NULL),
(123, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 02:09:38', NULL),
(124, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-25 18:20:26', NULL),
(125, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 18:52:02', NULL),
(126, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_3 at Wild Important Goals', '', 1, '2018-10-25 18:52:38', NULL),
(127, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/delete/31', 'Delete data dummy_1 at Wild Important Goals', '', 1, '2018-10-25 18:52:49', NULL),
(128, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/delete/32', 'Delete data dummy_3 at Wild Important Goals', '', 1, '2018-10-25 18:52:57', NULL),
(129, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_2 at Wild Important Goals', '', 1, '2018-10-25 19:01:18', NULL),
(130, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/edit-save/33', 'Update data dummy_2 at Wild Important Goals', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>id_ma_area</td><td></td><td></td></tr><tr><td>id_ma_rayon</td><td></td><td></td></tr><tr><td>id_ma_satuan</td><td></td><td></td></tr><tr><td>satuan</td><td>2</td><td>7</td></tr></tbody></table>', 1, '2018-10-25 19:02:57', NULL),
(131, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/delete/33', 'Delete data dummy_2 at Wild Important Goals', '', 1, '2018-10-25 19:03:05', NULL),
(132, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data dummy_2 at Wild Important Goals', '', 1, '2018-10-25 19:03:21', NULL),
(133, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/add-save', 'Add New Data adasd at Wild Important Goals', '', 1, '2018-10-25 19:05:07', NULL),
(134, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/delete/35', 'Delete data adasd at Wild Important Goals', '', 1, '2018-10-25 19:24:37', NULL),
(135, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/20', 'Update data Update Progress at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>AdminTrLeadControllerGetIndex</td><td>AdminTrLeadControllerGetAdd</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2018-10-25 19:32:41', NULL),
(136, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/tr_lead/add', 'Try add data at Update Progress', '', 1, '2018-10-25 19:32:45', NULL),
(137, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/tr_lead/add', 'Try add data at Update Progress', '', 1, '2018-10-25 19:33:03', NULL),
(138, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/20', 'Update data Update Progress at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2018-10-25 19:33:23', NULL),
(139, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/tr_lead/add', 'Try add data at Update Progress', '', 1, '2018-10-25 19:33:26', NULL),
(140, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lead/add-save', 'Add New Data asdasd at Lead Measure', '', 1, '2018-10-26 00:08:03', NULL),
(141, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-30 20:25:32', NULL),
(142, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'http://localhost:8000/admin/ma_lag/edit-save/34', 'Update data dummy_2 at Wild Important Goals', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>id_ma_area</td><td></td><td></td></tr><tr><td>id_ma_rayon</td><td></td><td></td></tr><tr><td>id_ma_satuan</td><td></td><td>2</td></tr></tbody></table>', 1, '2018-10-30 23:50:23', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(143, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-10-31 18:29:02', NULL),
(144, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-11-05 18:59:12', NULL),
(145, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-11-06 18:40:39', NULL),
(146, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-11-22 18:04:41', NULL),
(147, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2018-11-22 18:09:59', NULL),
(148, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-11-22 18:10:32', NULL),
(149, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2018-11-22 18:11:03', NULL),
(150, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-11-22 18:13:36', NULL),
(151, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2018-11-22 18:13:40', NULL),
(152, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2018-11-22 18:13:48', NULL),
(153, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/users/edit-save/1', 'Update data Super Admin at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email</td><td>admin@crudbooster.com</td><td>admin@admin.com</td></tr><tr><td>password</td><td>$2y$10$jClAyN6utmxZWUfoQLepR.ltRxLeFD8WbvsRXp/z2kCqqby6IBDMy</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2018-11-22 18:14:08', NULL),
(154, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/logout', 'admin@admin.com logout', '', 1, '2018-11-22 18:14:13', NULL),
(155, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@admin.com login with IP Address 127.0.0.1', '', 1, '2018-11-22 18:14:23', NULL),
(156, '192.168.43.246', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://192.168.43.176:8000/admin/login', 'admin@admin.com login with IP Address 192.168.43.246', '', 1, '2018-11-22 19:36:26', NULL),
(157, '192.168.43.246', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://192.168.43.176:8000/admin/ma_lead/delete/8', 'Delete data asdasd at Lead Measure', '', 1, '2018-11-22 20:19:40', NULL),
(158, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@admin.com login with IP Address 127.0.0.1', '', 1, '2018-11-25 23:47:29', NULL),
(159, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@admin.com login with IP Address 127.0.0.1', '', 1, '2018-11-26 20:38:54', NULL),
(160, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'http://localhost/four_dx/public/admin/logout', 'admin@admin.com logout', '', 1, '2018-12-03 02:05:06', NULL),
(161, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'http://localhost/four_dx/public/admin/login', 'admin@admin.com login with IP Address ::1', '', 1, '2018-12-03 02:05:09', NULL),
(162, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'http://localhost/four_dx/public/admin/login', 'admin@admin.com login with IP Address ::1', '', 1, '2018-12-03 08:29:06', NULL),
(163, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'http://localhost/four_dx/public/admin/login', 'admin@admin.com login with IP Address ::1', '', 1, '2018-12-03 19:35:58', NULL),
(164, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'http://localhost/four_dx/public/admin/login', 'admin@admin.com login with IP Address ::1', '', 1, '2018-12-03 23:22:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(8, 'tes', 'URL', '/map', 'normal', NULL, 0, 0, 0, 1, 1, '2018-10-22 07:43:25', NULL),
(11, 'Unit', 'Route', 'AdminMaUnitControllerGetIndex', 'normal', 'fa fa-gear', 13, 1, 0, 1, 1, '2018-10-22 07:58:28', '2018-10-23 20:55:39'),
(12, 'Area', 'Route', 'AdminMaAreaControllerGetIndex', 'normal', 'fa fa-gear', 13, 1, 0, 1, 2, '2018-10-22 08:03:27', '2018-10-23 20:57:01'),
(13, 'Data Master', 'URL', '/master', 'normal', 'fa fa-database', 0, 1, 0, 1, 5, '2018-10-22 19:41:19', '2018-10-23 07:09:52'),
(14, 'Rayon', 'Route', 'AdminMaRayonControllerGetIndex', 'normal', 'fa fa-gear', 13, 1, 0, 1, 3, '2018-10-22 19:42:48', '2018-10-23 20:56:20'),
(15, 'Satuan', 'Route', 'AdminMaSatuanControllerGetIndex', 'normal', 'fa fa-gear', 13, 1, 0, 1, 4, '2018-10-22 20:16:15', '2018-10-23 20:57:12'),
(16, 'Tipe Laporan', 'Route', 'AdminMaTipeLaporanControllerGetIndex', 'normal', 'fa fa-gear', 13, 1, 0, 1, 5, '2018-10-22 20:16:39', '2018-10-23 20:56:41'),
(17, 'Wild Important Goals', 'Route', 'AdminMaIndikatorControllerGetIndex', 'normal', 'fa fa-glass', 0, 1, 0, 1, 2, '2018-10-22 20:17:06', '2018-10-23 23:56:33'),
(19, 'Lead Measure', 'Route', 'AdminMaLead24ControllerGetIndex', 'normal', 'fa fa-music', 0, 1, 0, 1, 3, '2018-10-22 20:55:25', '2018-10-23 07:09:41'),
(20, 'Update Progress', 'Route', 'AdminTrLeadControllerGetAdd', 'normal', 'fa fa-star', 0, 1, 0, 1, 4, '2018-10-23 00:39:53', '2018-10-25 19:33:23'),
(21, 'Strategic Map', 'Route', 'AdminDsMapControllerGetIndex', 'normal', 'fa fa-glass', 0, 1, 1, 1, 1, '2018-10-23 07:06:03', '2018-10-23 07:06:30'),
(22, 'tes', 'Route', 'AdminCmsUsers1ControllerGetIndex', NULL, 'fa fa-glass', 0, 1, 0, 1, 6, '2018-11-26 20:46:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(5, 1, 1),
(8, 2, 1),
(9, 3, 1),
(12, 4, 1),
(17, 5, 1),
(21, 6, 1),
(22, 7, 1),
(23, 8, 1),
(24, 9, 1),
(25, 10, 1),
(35, 18, 1),
(40, 21, 1),
(41, 19, 1),
(42, 13, 1),
(44, 11, 1),
(46, 14, 1),
(48, 16, 1),
(49, 12, 1),
(50, 15, 1),
(51, 17, 1),
(53, 20, 1),
(54, 22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2018-10-20 20:17:21', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2018-10-20 20:17:21', NULL, NULL),
(12, 'map', 'fa fa-glass', 'map', 'map', 'AdminMapController', 0, 0, '2018-10-20 20:47:45', NULL, '2018-10-20 21:10:26'),
(13, 'Map', 'fa fa-glass', 'map13', 'map', 'AdminMap13Controller', 0, 0, '2018-10-20 21:18:21', NULL, '2018-10-20 21:43:59'),
(14, 'Strategic Map', 'fa fa-glass', 'map14', 'map', 'AdminMap14Controller', 0, 0, '2018-10-20 21:58:48', NULL, '2018-10-22 07:40:14'),
(15, 'Company Code', 'fa fa-glass', 'ma_companycode', 'ma_companycode', 'AdminMaCompanycodeController', 0, 0, '2018-10-22 07:45:36', NULL, '2018-10-22 18:03:38'),
(16, 'Business Area', 'fa fa-glass', 'ma_businessarea', 'ma_businessarea', 'AdminMaBusinessareaController', 0, 0, '2018-10-22 07:46:52', NULL, '2018-10-22 18:03:41'),
(17, 'Unit', 'fa fa-glass', 'ma_unit', 'ma_unit', 'AdminMaUnitController', 0, 0, '2018-10-22 07:58:28', NULL, NULL),
(18, 'Area', 'fa fa-glass', 'ma_area', 'ma_area', 'AdminMaAreaController', 0, 0, '2018-10-22 08:03:27', NULL, NULL),
(19, 'Rayon', 'fa fa-glass', 'ma_rayon', 'ma_rayon', 'AdminMaRayonController', 0, 0, '2018-10-22 19:42:48', NULL, NULL),
(20, 'Satuan', 'fa fa-glass', 'ma_satuan', 'ma_satuan', 'AdminMaSatuanController', 0, 0, '2018-10-22 20:16:15', NULL, NULL),
(21, 'Tipe Laporan', 'fa fa-glass', 'ma_tipe_laporan', 'ma_tipe_laporan', 'AdminMaTipeLaporanController', 0, 0, '2018-10-22 20:16:39', NULL, NULL),
(22, 'Wild Important Goals', 'fa fa-glass', 'ma_lag', 'ma_lag', 'AdminMaIndikatorController', 0, 0, '2018-10-22 20:17:06', NULL, NULL),
(23, 'Lead Measure', 'fa fa-glass', 'ma_lead', 'ma_lead', 'AdminMaIndikator23Controller', 0, 0, '2018-10-22 20:39:36', NULL, '2018-10-22 20:55:03'),
(24, 'Lead Measure', 'fa fa-glass', 'ma_lead', 'ma_lead', 'AdminMaLead24Controller', 0, 0, '2018-10-22 20:55:25', NULL, NULL),
(25, 'Update Progress', 'fa fa-glass', 'progress', 'tr_lead', 'AdminTrLeadController', 0, 0, '2018-10-23 00:39:53', NULL, NULL),
(26, 'Strategic Map', 'fa fa-glass', 'ds_map', 'ds_map', 'AdminDsMapController', 0, 0, '2018-10-23 07:06:03', NULL, NULL),
(27, 'tes', 'fa fa-glass', 'cms_users', 'cms_users', 'AdminCmsUsers1Controller', 0, 0, '2018-11-26 20:46:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-black', '2018-10-20 20:17:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(2, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(3, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(4, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(5, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(6, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(7, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(8, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(9, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(10, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(11, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(12, 1, 1, 1, 1, 1, 1, 22, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 23, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 26, NULL, NULL),
(17, 1, 1, 1, 1, 1, 1, 27, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2018-10-20 20:17:21', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2018-10-20 20:17:21', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2018-10-20 20:17:21', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2018-10-20 20:17:21', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'FOUR DX', 'text', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2018-10-20 20:17:21', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', NULL, 'upload_image', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', NULL, 'upload_image', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2018-10-20 20:17:21', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2018-10-20 20:17:21', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_statistics`
--

INSERT INTO `cms_statistics` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Map', 'map', '2018-10-20 20:23:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_statistic_components`
--

INSERT INTO `cms_statistic_components` (`id`, `id_cms_statistics`, `componentID`, `component_name`, `area_name`, `sorting`, `name`, `config`, `created_at`, `updated_at`) VALUES
(1, 1, '7585040596727403777eb3ed58f17640', 'table', NULL, 1, 'Untitled', NULL, '2018-10-20 20:23:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', 'uploads/1/2018-10/dfg.jpg', 'admin@admin.com', '$2y$10$jClAyN6utmxZWUfoQLepR.ltRxLeFD8WbvsRXp/z2kCqqby6IBDMy', 1, '2018-10-20 20:17:21', '2018-11-22 18:14:08', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `ds_map`
--

CREATE TABLE `ds_map` (
  `id` int(11) NOT NULL,
  `label` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ma_area`
--

CREATE TABLE `ma_area` (
  `id` int(11) NOT NULL,
  `id_ma_unit` int(11) DEFAULT NULL,
  `business_area` varchar(10) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_area`
--

INSERT INTO `ma_area` (`id`, `id_ma_unit`, `business_area`, `nama`, `created_at`, `updated_at`) VALUES
(1, 2, '6311', 'Area Pariaman', '2018-10-22 19:40:04', '2018-10-22 19:40:04'),
(2, 4, '5511', 'Area Denpasar', '2018-10-23 00:24:59', '2018-10-23 00:24:59'),
(3, 7, '5011', 'AREA BULUNGAN', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(4, 7, '5021', 'AREA MAMPANG', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(5, 8, '5511', 'AREA DENPASAR', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(6, 8, '5521', 'AREA TABANAN', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(7, 9, '1001', 'PT PLN (PERSERO) KANTOR PUSAT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(8, 10, '4K01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN NUSA TENGGARA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(9, 10, '4K11', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN TIMOR', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(10, 10, '4K12', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN LOMBOK', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(11, 10, '4K13', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN FLORES', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(12, 10, '4K14', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN SUMBAWA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(13, 11, '4R01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN KALIMANTAN BAGIAN BARAT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(14, 11, '4R11', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN BARAT 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(15, 11, '4R12', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN BARAT 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(16, 11, '4R13', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN BARAT 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(17, 11, '4R14', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN BARAT 4', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(18, 12, '4I01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN KALIMANTAN BAGIAN TENGAH', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(19, 12, '4I12', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN TENGAH 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(20, 12, '4I15', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN TENGAH 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(21, 12, '4I17', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN TENGAH 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(22, 12, '4I18', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN TENGAH 4', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(23, 13, '4J01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN KALIMANTAN BAGIAN TIMUR', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(24, 13, '4J13', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN TIMUR 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(25, 13, '4J16', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN TIMUR 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(26, 13, '4J17', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN KALIMANTAN BAGIAN TIMUR 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(27, 14, '4A01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN PEMBANGKITAN SUMATERA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(28, 14, '4A11', 'UNIT PELAKSANA PROYEK PEMBANGKITAN SUMATERA 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(29, 14, '4A12', 'UNIT PELAKSANA PROYEK PEMBANGKITAN SUMATERA 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(30, 14, '4A13', 'UNIT PELAKSANA PROYEK PEMBANGKITAN SUMATERA 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(31, 14, '4A14', 'UNIT PELAKSANA PROYEK PEMBANGKITAN SUMATERA 4', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(32, 14, '4A15', 'UNIT PELAKSANA PROYEK PEMBANGKITAN SUMATERA 5', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(33, 14, '4A16', 'UNIT PELAKSANA PROYEK PEMBANGKITAN SUMATERA 6', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(34, 14, '4A17', 'UNIT PELAKSANA PROYEK PEMBANGKIT SUMATERA 7', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(35, 14, '4A18', 'UNIT PELAKSANA PROYEK PEMBANGKIT SUMATERA 8', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(36, 15, '4C01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN SUMATERA BAGIAN SELATAN', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(37, 15, '4C12', 'UNIT PELAKSANA PROYEK JARINGAN SUMATERA SELATAN', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(38, 15, '4C14', 'UNIT PELAKSANA PROYEK JARINGAN BANGKA BELITUNG', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(39, 15, '4C11', 'UNIT PELAKSANA PROYEK JARINGAN LAMPUNG DAN BENGKUnit Layanan U', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(40, 16, '4Q01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN SUMATERA BAGIAN TENGAH', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(41, 16, '4Q11', 'UNIT PELAKSANA PROYEK JARINGAN RIAU DAN KEPUnit Layanan AUAN RIAU', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(42, 16, '4Q12', 'UNIT PELAKSANA PROYEK JARINGAN SUMATERA BARAT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(43, 16, '4Q13', 'UNIT PELAKSANA PROYEK JARINGAN JAMBI', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(44, 17, '4B01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN SUMATERA BAGIAN UTARA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(45, 17, '4B14', 'UNIT PELAKSANA PROYEK JARINGAN ACEH', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(46, 17, '4B11', 'UNIT PELAKSANA PROYEK JARINGAN SUMATERA UTARA 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(47, 17, '4B15', 'UNIT PELAKSANA PROYEK JARINGAN SUMATERA UTARA 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(48, 18, '4D01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN INTERKONEKSI SUMATERA JAWA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(49, 18, '4D11', 'UNIT PELAKSANA PROYEK JARINGAN INTERKONEKSI SUMATERA JAWA 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(50, 18, '4D12', 'UNIT PELAKSANA PROYEK JARINGAN INTERKONEKSI SUMATERA JAWA 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(51, 18, '4D13', 'UNIT PELAKSANA PROYEK JARINGAN INTERKONEKSI SUMATERA JAWA 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(52, 19, '4E01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN JAWA BAGIAN BARAT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(53, 19, '4E12', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN BARAT 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(54, 19, '4E13', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN BARAT 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(55, 19, '4E14', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN BARAT 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(56, 19, '4E17', 'UNIT PELAKSANA PROYEK PEMBANGKIT JAWA BAGIAN BARAT 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(57, 19, '4E18', 'UNIT PELAKSANA PROYEK PEMBANGKIT JAWA BAGIAN BARAT 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(58, 20, '4F01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN JAWA BAGIAN TENGAH I', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(59, 20, '4F11', 'UNIT PELAKSANA PROYEK PEMBANGKITAN JAWA BAGIAN TENGAH 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(60, 20, '4F12', 'UNIT PELAKSANA PROYEK PEMBANGKITAN JAWA BAGIAN TENGAH 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(61, 20, '4F13', 'UNIT PELAKSANA PROYEK PEMBANGKITAN JAWA BAGIAN TENGAH 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(62, 20, '4F14', 'UNIT PELAKSANA PROYEK PEMBANGKITAN JAWA BAGIAN TENGAH 4', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(63, 21, '4P01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN JAWA BAGIAN TENGAH II', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(64, 21, '4P11', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN TENGAH 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(65, 21, '4P12', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN TENGAH 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(66, 21, '4P13', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN TENGAH 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(67, 21, '4P14', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN TENGAH 4', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(68, 22, '4G01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN JAWA BAGIAN TIMUR DAN BALI I', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(69, 22, '4G12', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN TIMUR DAN BALI 1', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(70, 22, '4G13', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN TIMUR DAN BALI 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(71, 22, '4G14', 'UNIT PELAKSANA PROYEK JARINGAN JAWA BAGIAN TIMUR DAN BALI 3', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(72, 23, '4H01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN JAWA BAGIAN TIMUR DAN BALI II', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(73, 23, '4H15', 'UNIT PELAKSANA PROYEK PEMBANGKIT JAWA BAGIAN TIMUR DAN BALI 2', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(74, 23, '', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN JAWA BAGIAN TIMUR DAN BALI', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(75, 23, '4H12', 'UNIT PELAKSANA PROYEK PEMBANGKIT JAWA BAGIAN TIMUR DAN BALI 4', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(76, 24, '4N01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN PAPUA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(77, 24, '4N11', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN PAPUA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(78, 24, '4N13', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN PAPUA BARAT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(79, 25, '4L01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN SUnit Layanan AWESI BAGIAN UTARA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(80, 25, '4L11', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN SUnit Layanan AWESI UTARA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(81, 25, '', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN GORONTALO', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(82, 25, '4L12', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN SUnit Layanan AWESI TENGAH', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(83, 26, '4M01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN SUnit Layanan AWESI BAGIAN SELATAN', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(84, 26, '4M12', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN SUnit Layanan AWESI BARAT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(85, 26, '4M11', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN SUnit Layanan AWESI SELATAN', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(86, 26, '4M13', 'UNIT PELAKSANA PROYEK PEMBANGKIT DAN JARINGAN SUnit Layanan AWESI TENGGARA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(87, 27, '4O01', 'PT PLN (PERSERO) UNIT INDUK PEMBANGUNAN MALUKU', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(88, 27, '4O11', 'UNIT PELAKSANA PROYEK PEMBANGKITAN DAN JARINGAN MALUKU', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(89, 27, '4O12', 'UNIT PELAKSANA PROYEK PEMBANGKITAN DAN JARINGAN MALUKU UTARA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(90, 28, '3501', 'PT PLN (PERSERO) UNIT INDUK TRANSMISI JAWA BAGIAN TENGAH', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(91, 28, '3511', 'UNIT PELAKSANA TRANSMISI BOGOR', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(92, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BOGOR', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(93, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SUKABUMI', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(94, 28, '3512', 'UNIT PELAKSANA TRANSMISI BANDUNG', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(95, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BANDUNG BARAT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(96, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BANDUNG SELATAN', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(97, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BANDUNG TIMUR', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(98, 28, '3514', 'UNIT PELAKSANA TRANSMISI KARAWANG', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(99, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK CIKARANG', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(100, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KARAWANG', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(101, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PURWAKARTA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(102, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BEKASI', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(103, 28, '3513', 'UNIT PELAKSANA TRANSMISI CIREBON', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(104, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK CIREBON', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(105, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK GARUT', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(106, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK CIAMIS', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(107, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK JATIBARANG', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(108, 28, '3516', 'UNIT PELAKSANA TRANSMISI SALATIGA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(109, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SALATIGA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(110, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SURAKARTA', '2018-11-07 00:05:24', '2018-11-07 00:05:24'),
(111, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK YOGYAKARTA', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(112, 28, '3515', 'UNIT PELAKSANA TRANSMISI PURWOKERTO', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(113, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PURWOKERTO', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(114, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TEGAL', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(115, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK WONOSOBO', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(116, 28, '3517', 'UNIT PELAKSANA TRANSMISI SEMARANG', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(117, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SEMARANG', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(118, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KUDUS', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(119, 28, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK REMBANG', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(120, 29, '3401', 'PT PLN (PERSERO) UNIT INDUK TRANSMISI JAWA BAGIAN BARAT', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(121, 29, '3413', 'UNIT PELAKSANA TRANSMISI DURI KOSAMBI', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(122, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK DURIKOSAMBI', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(123, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK CIKUPA', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(124, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TANGERANG KOTA', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(125, 29, '3412', 'UNIT PELAKSANA TRANSMISI PUnit Layanan OGADUNG', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(126, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KARET', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(127, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PUnit Layanan OGADUNG', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(128, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BEKASI', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(129, 29, '3411', 'UNIT PELAKSANA TRANSMISI CAWANG', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(130, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK GANDUnit Layanan ', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(131, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK CAWANG', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(132, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TANGERANG SELATAN', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(133, 29, '3414', 'UNIT PELAKSANA TRANSMISI CILEGON', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(134, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK CILEGON', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(135, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SURALAYA', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(136, 29, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK RANGKAS', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(137, 30, '3601', 'PT PLN (PERSERO) UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(138, 30, '3611', 'UNIT PELAKSANA TRANSMISI SURABAYA', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(139, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SURABAYA UTARA', '2018-11-07 00:05:25', '2018-11-07 00:05:25'),
(140, 30, '3612', 'UNIT PELAKSANA TRANSMISI MALANG', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(141, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KRIAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(142, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK MALANG', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(143, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK MOJOKERTO', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(144, 30, '3614', 'UNIT PELAKSANA TRANSMISI PROBOLINGGO', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(145, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BANGIL', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(146, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK JEMBER', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(147, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PROBOLINGGO', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(148, 30, '3613', 'UNIT PELAKSANA TRANSMISI MADIUN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(149, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BABAT', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(150, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK MADIUN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(151, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KEDIRI', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(152, 30, '3615', 'UNIT PELAKSANA TRANSMISI BALI', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(153, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BALI UTARA', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(154, 30, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BALI SELATAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(155, 31, '2401', 'PT PLN (PERSERO) UNIT INDUK PEMBANGKITAN DAN PENYALURAN KALIMANTAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(156, 31, '2411', 'UNIT PELAKSANA PENYALURAN DAN PENGATUR BEBAN SISTEM KALIMANTAN BARAT', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(157, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PONTIANAK', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(158, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SINGKAWANG', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(159, 31, '2412', 'UNIT PELAKSANA PENYALURAN DAN PENGATUR BEBAN SISTEM KALIMANTAN SELATAN DAN KALIMANTAN TENGAH', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(160, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BANDARMASIH', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(161, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BANJAR', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(162, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BARABAI', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(163, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PALANGKARAYA', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(164, 31, '2413', 'UNIT PELAKSANA PENYALURAN DAN PENGATUR BEBAN SISTEM KALIMANTAN TIMUR', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(165, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BONTANG', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(166, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BALIKPAPAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(167, 31, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SAMARINDA', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(168, 31, '2414', 'UNIT PELAKSANA PEMBANGKITAN KAPUAS', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(169, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SIANTAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(170, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS SIANTAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(171, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SINGKAWANG', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(172, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SEI RAYA', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(173, 31, '2416', 'UNIT PELAKSANA PEMBANGKITAN BARITO', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(174, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA MESIN GAS BANGKANAI', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(175, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR IR. P. M. NOOR', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(176, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TRISAKTI', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(177, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BENUA LIMA', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(178, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS TRISAKTI', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(179, 31, '2415', 'UNIT PELAKSANA PEMBANGKITAN ASAM-ASAM', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(180, 31, '2417', 'UNIT PELAKSANA PEMBANGKITAN BALIKPAPAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(181, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BALIKPAPAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(182, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL PASER', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(183, 31, '2418', 'UNIT PELAKSANA PEMBANGKITAN MAHAKAM', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(184, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS DAN UAP TANJUNG BATU', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(185, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL DAN MESIN GAS BONTANG', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(186, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KARANG ASAM', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(187, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KELEDANG', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(188, 31, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS SEMBERAH', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(189, 32, '2501', 'PT PLN (PERSERO) UNIT INDUK PEMBANGKITAN DAN PENYALURAN SUnit Layanan AWESI', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(190, 32, '2511', 'UNIT PELAKSANA PENYALURAN DAN PENGATUR BEBAN SISTEM MINAHASA', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(191, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK I LOPANA', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(192, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK II SAWANGAN', '2018-11-07 01:42:10', '2018-11-07 01:42:10'),
(193, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PALU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(194, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK GORONTALO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(195, 32, '2518', 'UNIT PELAKSANA TRANSMISI SUnit Layanan AWESI SELATAN, SUnit Layanan AWESI TENGGARA DAN SUnit Layanan AWESI BARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(196, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PANAKKUKANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(197, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TELLO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(198, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PAREPARE', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(199, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SIDRAP', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(200, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BUnit Layanan UKUMBA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(201, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KENDARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(202, 32, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK MAMUJU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(203, 32, '2517', 'UNIT PELAKSANA PENGATUR BEBAN SUnit Layanan AWESI SELATAN, SUnit Layanan AWESI TENGGARA DAN SUnit Layanan AWESI BARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(204, 32, '2512', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN MINAHASA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(205, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR TANGGARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(206, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR TONSEALAMA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(207, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BITUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(208, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL LOPANA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(209, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA PANAS BUMI LAHENDONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(210, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS MALEO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(211, 32, '2516', 'UNIT PELAKSANA PEMBANGKITAN PUNAGAYA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(212, 32, '2514', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN TELLO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(213, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TELLO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(214, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS DAN UAP TELLO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(215, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SELAYAR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(216, 32, '2515', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN BAKARU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(217, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA MINI HIDRO MAMUJU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(218, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR BAKARU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(219, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR BILI BILI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(220, 32, '2513', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN KENDARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(221, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL WUA-WUA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(222, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BAU-BAU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(223, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KOLAKA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(224, 32, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL POASIA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(225, 33, '3301', 'PT PLN (PERSERO) UNIT INDUK PUSAT PENGATUR BEBAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(226, 33, '3311', 'UNIT PELAKSANA PENGATUR BEBAN DKI JAKARTA DAN BANTEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(227, 33, '3312', 'UNIT PELAKSANA PENGATUR BEBAN JAWA BARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(228, 33, '3313', 'UNIT PELAKSANA PENGATUR BEBAN JAWA TENGAH DAN DIY', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(229, 33, '3314', 'UNIT PELAKSANA PENGATUR BEBAN JAWA TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(230, 33, '3315', 'UNIT PELAKSANA PENGATUR BEBAN BALI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(231, 34, '5601', 'PT PLN (PERSERO) UNIT INDUK DISTRIBUSI BANTEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(232, 34, '', 'UNIT PELAKSANA PENGATUR DISTRIBUSI BANTEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(233, 34, '5616', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANTEN UTARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(234, 34, '', 'UNIT LAYANAN PELANGGAN CIKANDE', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(235, 34, '', 'UNIT LAYANAN PELANGGAN PRIMA KRAKATAU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(236, 34, '', 'UNIT LAYANAN PELANGGAN SERANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(237, 34, '', 'UNIT LAYANAN PELANGGAN CILEGON', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(238, 34, '', 'UNIT LAYANAN PELANGGAN ANYER', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(239, 34, '5617', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANTEN SELATAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(240, 34, '', 'UNIT LAYANAN PELANGGAN RANGKASBITUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(241, 34, '', 'UNIT LAYANAN PELANGGAN PANDEGLANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(242, 34, '', 'UNIT LAYANAN PELANGGAN MALINGPING', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(243, 34, '', 'UNIT LAYANAN PELANGGAN LABUAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(244, 34, '5613', 'UNIT PELAKSANA PELAYANAN PELANGGAN CIKUPA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(245, 34, '5612', 'UNIT PELAKSANA PELAYANAN PELANGGAN CIKOKOL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(246, 34, '5614', 'UNIT PELAKSANA PELAYANAN PELANGGAN TELUK NAGA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(247, 34, '5611', 'UNIT PELAKSANA PELAYANAN PELANGGAN SERPONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(248, 34, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI BANTEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(249, 35, '5401', 'PT PLN (PERSERO) UNIT INDUK DISTRIBUSI JAKARTA RAYA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(250, 35, '5487', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANDENGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(251, 35, '5486', 'UNIT PELAKSANA PELAYANAN PELANGGAN BINTARO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(252, 35, '5483', 'UNIT PELAKSANA PELAYANAN PELANGGAN BUnit Layanan UNGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(253, 35, '5489', 'UNIT PELAKSANA PELAYANAN PELANGGAN CEMPAKA PUTIH', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(254, 35, '5493', 'UNIT PELAKSANA PELAYANAN PELANGGAN CENGKARENG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(255, 35, '5482', 'UNIT PELAKSANA PELAYANAN PELANGGAN CIRACAS', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(256, 35, '5485', 'UNIT PELAKSANA PELAYANAN PELANGGAN CIPUTAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(257, 35, '5477', 'UNIT PELAKSANA PELAYANAN PELANGGAN JATINEGARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(258, 35, '5479', 'UNIT PELAKSANA PELAYANAN PELANGGAN KRAMAJATI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(259, 35, '5484', 'UNIT PELAKSANA PELAYANAN PELANGGAN KEBON JERUK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(260, 35, '5480', 'UNIT PELAKSANA PELAYANAN PELANGGAN LENTENG AGUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(261, 35, '5491', 'UNIT PELAKSANA PELAYANAN PELANGGAN MARUNDA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(262, 35, '5488', 'UNIT PELAKSANA PELAYANAN PELANGGAN MENTENG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(263, 35, '5481', 'UNIT PELAKSANA PELAYANAN PELANGGAN PONDOK GEDE', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(264, 35, '5478', 'UNIT PELAKSANA PELAYANAN PELANGGAN PONDOK KOPI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(265, 35, '5490', 'UNIT PELAKSANA PELAYANAN PELANGGAN TANJUNG PRIOK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(266, 35, '5456', 'UNIT PELAKSANA PENGATUR DISTRIBUSI JAKARTA RAYA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(267, 8, '5501', 'PT PLN (PERSERO) UNIT INDUK DISTRIBUSI BALI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(268, 8, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI BALI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(269, 8, '5581', 'UNIT PELAKSANA PELAYANAN PELANGGAN BALI SELATAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(270, 8, '', 'UNIT LAYANAN PELANGGAN DENPASAR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(271, 8, '', 'UNIT LAYANAN PELANGGAN KUTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(272, 8, '', 'UNIT LAYANAN PELANGGAN MENGWI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(273, 8, '', 'UNIT LAYANAN PELANGGAN TABANAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(274, 8, '', 'UNIT LAYANAN PELANGGAN SANUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(275, 8, '5582', 'UNIT PELAKSANA PELAYANAN PELANGGAN BALI UTARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(276, 8, '', 'UNIT LAYANAN PELANGGAN NEGARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(277, 8, '', 'UNIT LAYANAN PELANGGAN SERIRIT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(278, 8, '', 'UNIT LAYANAN PELANGGAN TEJAKUnit Layanan A', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(279, 8, '', 'UNIT LAYANAN PELANGGAN SINGARAJA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(280, 8, '', 'UNIT LAYANAN PELANGGAN GILIMANUK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(281, 8, '5583', 'UNIT PELAKSANA PELAYANAN PELANGGAN BALI TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(282, 8, '', 'UNIT LAYANAN PELANGGAN KARANGASEM', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(283, 8, '', 'UNIT LAYANAN PELANGGAN GIANYAR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(284, 8, '', 'UNIT LAYANAN PELANGGAN BANGLI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(285, 8, '', 'UNIT LAYANAN PELANGGAN KLUNGKUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(286, 8, '5556', 'UNIT PELAKSANA PENGATUR DISTRIBUSI BALI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(287, 37, '5201', 'PT PLN (PERSERO) UNIT INDUK DISTRIBUSI JAWA TENGAH DAN D.I. YOGYAKARTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(288, 37, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI JAWA TENGAH DAN D.I.YOGYAKARTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(289, 37, '5223', 'UNIT PELAKSANA PELAYANAN PELANGGAN SUKOHARJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(290, 37, '', 'UNIT LAYANAN PELANGGAN SUKOHARJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(291, 37, '', 'UNIT LAYANAN PELANGGAN GROGOL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(292, 37, '', 'UNIT LAYANAN PELANGGAN WONOGIRI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(293, 37, '', 'UNIT LAYANAN PELANGGAN WONOGIRI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(294, 37, '', 'UNIT LAYANAN PELANGGAN WONOGIRI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(295, 37, '5222', 'UNIT PELAKSANA PELAYANAN PELANGGAN DEMAK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(296, 37, '', 'UNIT LAYANAN PELANGGAN DEMAK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(297, 37, '', 'UNIT LAYANAN PELANGGAN TEGOWANU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(298, 37, '', 'UNIT LAYANAN PELANGGAN PURWODADI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(299, 37, '', 'UNIT LAYANAN PELANGGAN WIROSARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(300, 37, '5216', 'UNIT PELAKSANA PELAYANAN PELANGGAN TEGAL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(301, 37, '', 'UNIT LAYANAN PELANGGAN SLAWI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(302, 37, '', 'UNIT LAYANAN PELANGGAN PEMALANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(303, 37, '', 'UNIT LAYANAN PELANGGAN BREBES', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(304, 37, '', 'UNIT LAYANAN PELANGGAN BUMIAYU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(305, 37, '', 'UNIT LAYANAN PELANGGAN BALAPUnit Layanan ANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(306, 37, '', 'UNIT LAYANAN PELANGGAN JATIBARANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(307, 37, '', 'UNIT LAYANAN PELANGGAN COMAL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(308, 37, '', 'UNIT LAYANAN PELANGGAN TEGAL KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(309, 37, '', 'UNIT LAYANAN PELANGGAN TEGAL TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(310, 37, '', 'UNIT LAYANAN PELANGGAN RANDUDONGKAL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(311, 37, '5217', 'UNIT PELAKSANA PELAYANAN PELANGGAN SEMARANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(312, 37, '', 'UNIT LAYANAN PELANGGAN KENDAL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(313, 37, '', 'UNIT LAYANAN PELANGGAN WELERI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(314, 37, '', 'UNIT LAYANAN PELANGGAN BOJA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(315, 37, '', 'UNIT LAYANAN PELANGGAN SEMARANG SELATAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(316, 37, '', 'UNIT LAYANAN PELANGGAN SEMARANG TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(317, 37, '', 'UNIT LAYANAN PELANGGAN SEMARANG BARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(318, 37, '', 'UNIT LAYANAN PELANGGAN SEMARANG TENGAH', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(319, 37, '5220', 'UNIT PELAKSANA PELAYANAN PELANGGAN PEKALONGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(320, 37, '', 'UNIT LAYANAN PELANGGAN BATANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(321, 37, '', 'UNIT LAYANAN PELANGGAN KEDUNGWUNI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(322, 37, '', 'UNIT LAYANAN PELANGGAN PEKALONGAN KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(323, 37, '', 'UNIT LAYANAN PELANGGAN WIRADESA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(324, 37, '5215', 'UNIT PELAKSANA PELAYANAN PELANGGAN PURWOKERTO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(325, 37, '', 'UNIT LAYANAN PELANGGAN WONOSOBO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(326, 37, '', 'UNIT LAYANAN PELANGGAN BANJARNEGARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(327, 37, '', 'UNIT LAYANAN PELANGGAN PURBALINGGA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(328, 37, '', 'UNIT LAYANAN PELANGGAN BANYUMAS', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(329, 37, '', 'UNIT LAYANAN PELANGGAN AJIBARANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(330, 37, '', 'UNIT LAYANAN PELANGGAN WANGON', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(331, 37, '', 'UNIT LAYANAN PELANGGAN PURWOKERTO KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(332, 37, '5221', 'UNIT PELAKSANA PELAYANAN PELANGGAN CILACAP', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(333, 37, '', 'UNIT LAYANAN PELANGGAN KEBUMEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(334, 37, '', 'UNIT LAYANAN PELANGGAN GOMBONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(335, 37, '', 'UNIT LAYANAN PELANGGAN KROYA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(336, 37, '', 'UNIT LAYANAN PELANGGAN SIDAREJA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(337, 37, '', 'UNIT LAYANAN PELANGGAN MAJENANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(338, 37, '', 'UNIT LAYANAN PELANGGAN CILACAP KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(339, 37, '5211', 'UNIT PELAKSANA PELAYANAN PELANGGAN KUDUS', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(340, 37, '', 'UNIT LAYANAN PELANGGAN BANGSRI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(341, 37, '', 'UNIT LAYANAN PELANGGAN JEPARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(342, 37, '', 'UNIT LAYANAN PELANGGAN KUDUS KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(343, 37, '', 'UNIT LAYANAN PELANGGAN PATI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(344, 37, '', 'UNIT LAYANAN PELANGGAN JUWANA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(345, 37, '', 'UNIT LAYANAN PELANGGAN REMBANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(346, 37, '', 'UNIT LAYANAN PELANGGAN BLORA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(347, 37, '', 'UNIT LAYANAN PELANGGAN CEPU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(348, 37, '5219', 'UNIT PELAKSANA PELAYANAN PELANGGAN KLATEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(349, 37, '', 'UNIT LAYANAN PELANGGAN BOYOLALI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(350, 37, '', 'UNIT LAYANAN PELANGGAN KLATEN KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(351, 37, '', 'UNIT LAYANAN PELANGGAN TUnit Layanan UNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(352, 37, '', 'UNIT LAYANAN PELANGGAN PEDAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(353, 37, '', 'UNIT LAYANAN PELANGGAN DELANGGU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(354, 37, '5214', 'UNIT PELAKSANA PELAYANAN PELANGGAN MAGELANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(355, 37, '', 'UNIT LAYANAN PELANGGAN BOROBUDUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(356, 37, '', 'UNIT LAYANAN PELANGGAN PURWOREJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(357, 37, '', 'UNIT LAYANAN PELANGGAN KUTOARJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(358, 37, '', 'UNIT LAYANAN PELANGGAN TEMANGGUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(359, 37, '', 'UNIT LAYANAN PELANGGAN PARAKAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(360, 37, '', 'UNIT LAYANAN PELANGGAN TEGALREJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(361, 37, '', 'UNIT LAYANAN PELANGGAN MAGELANG KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(362, 37, '5213', 'UNIT PELAKSANA PELAYANAN PELANGGAN YOGYAKARTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(363, 37, '', 'UNIT LAYANAN PELANGGAN YOGYAKARTA KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(364, 37, '', 'UNIT LAYANAN PELANGGAN BANTUnit Layanan ', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(365, 37, '', 'UNIT LAYANAN PELANGGAN WATES', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(366, 37, '', 'UNIT LAYANAN PELANGGAN WONOSARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(367, 37, '', 'UNIT LAYANAN PELANGGAN SLEMAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(368, 37, '', 'UNIT LAYANAN PELANGGAN SEDAYU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(369, 37, '', 'UNIT LAYANAN PELANGGAN KALASAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(370, 37, '5212', 'UNIT PELAKSANA PELAYANAN PELANGGAN SURAKARTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(371, 37, '', 'UNIT LAYANAN PELANGGAN SRAGEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(372, 37, '', 'UNIT LAYANAN PELANGGAN PALUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(373, 37, '', 'UNIT LAYANAN PELANGGAN KARTASURA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(374, 37, '', 'UNIT LAYANAN PELANGGAN SUMBERLAWANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(375, 37, '', 'UNIT LAYANAN PELANGGAN MANAHAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(376, 37, '', 'UNIT LAYANAN PELANGGAN SURAKARTA KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(377, 37, '5218', 'UNIT PELAKSANA PELAYANAN PELANGGAN SALATIGA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(378, 37, '', 'UNIT LAYANAN PELANGGAN SALATIGA KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(379, 37, '', 'UNIT LAYANAN PELANGGAN UNGARAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(380, 37, '', 'UNIT LAYANAN PELANGGAN AMBARAWA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(381, 37, '5256', 'UNIT PELAKSANA PENGATUR DISTRIBUSI JAWA TENGAH DAN D.I.YOGYAKARTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(382, 38, '5101', 'PT PLN (PERSERO) UNIT INDUK DISTRIBUSI JAWA TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(383, 38, '5120', 'UNIT PELAKSANA PELAYANAN PELANGGAN SURABAYA UTARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(384, 38, '', 'UNIT LAYANAN PELANGGAN EMBONG WUNGSU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(385, 38, '', 'UNIT LAYANAN PELANGGAN TANDES', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(386, 38, '', 'UNIT LAYANAN PELANGGAN PERAK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(387, 38, '', 'UNIT LAYANAN PELANGGAN PLOSO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(388, 38, '', 'UNIT LAYANAN PELANGGAN KENJERAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(389, 38, '', 'UNIT LAYANAN PELANGGAN INDRAPURA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(390, 38, '5121', 'UNIT PELAKSANA PELAYANAN PELANGGAN SURABAYA SELATAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(391, 38, '', 'UNIT LAYANAN PELANGGAN GEDANGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(392, 38, '', 'UNIT LAYANAN PELANGGAN NGAGEL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(393, 38, '', 'UNIT LAYANAN PELANGGAN DARMO PERMAI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(394, 38, '', 'UNIT LAYANAN PELANGGAN DUKUH KUPANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(395, 38, '', 'UNIT LAYANAN PELANGGAN RUNGKUT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(396, 38, '5126', 'UNIT PELAKSANA PELAYANAN PELANGGAN SURABAYA BARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(397, 38, '', 'UNIT LAYANAN PELANGGAN TAMAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(398, 38, '', 'UNIT LAYANAN PELANGGAN KARANGPILANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(399, 38, '', 'UNIT LAYANAN PELANGGAN MENGANTI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(400, 38, '5124', 'UNIT PELAKSANA PELAYANAN PELANGGAN GRESIK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(401, 38, '', 'UNIT LAYANAN PELANGGAN SIDAYU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(402, 38, '', 'UNIT LAYANAN PELANGGAN BENJENG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(403, 38, '', 'UNIT LAYANAN PELANGGAN BAWEAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(404, 38, '', 'UNIT LAYANAN PELANGGAN GIRI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(405, 38, '5123', 'UNIT PELAKSANA PELAYANAN PELANGGAN SIDOARJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(406, 38, '', 'UNIT LAYANAN PELANGGAN KRIAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(407, 38, '', 'UNIT LAYANAN PELANGGAN PORONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(408, 38, '', 'UNIT LAYANAN PELANGGAN SIDOARJO KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(409, 38, '5118', 'UNIT PELAKSANA PELAYANAN PELANGGAN MOJOKERTO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(410, 38, '', 'UNIT LAYANAN PELANGGAN MOJOAGUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(411, 38, '', 'UNIT LAYANAN PELANGGAN JOMBANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(412, 38, '', 'UNIT LAYANAN PELANGGAN NGORO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(413, 38, '', 'UNIT LAYANAN PELANGGAN PLOSO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(414, 38, '', 'UNIT LAYANAN PELANGGAN MOJOSARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(415, 38, '', 'UNIT LAYANAN PELANGGAN PACET', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(416, 38, '', 'UNIT LAYANAN PELANGGAN KERTOSONO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(417, 38, '', 'UNIT LAYANAN PELANGGAN WARUJAYENG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(418, 38, '', 'UNIT LAYANAN PELANGGAN NGANJUK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(419, 38, '', 'UNIT LAYANAN PELANGGAN MOJOKERTO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(420, 38, '5115', 'UNIT PELAKSANA PELAYANAN PELANGGAN MALANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(421, 38, '', 'UNIT LAYANAN PELANGGAN BATU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(422, 38, '', 'UNIT LAYANAN PELANGGAN LAWANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(423, 38, '', 'UNIT LAYANAN PELANGGAN BUnit Layanan Unit Layanan AWANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(424, 38, '', 'UNIT LAYANAN PELANGGAN SINGOSARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(425, 38, '', 'UNIT LAYANAN PELANGGAN KEPANJEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(426, 38, '', 'UNIT LAYANAN PELANGGAN TUMPANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(427, 38, '', 'UNIT LAYANAN PELANGGAN NGANTANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(428, 38, '', 'UNIT LAYANAN PELANGGAN GONDANGLEGI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(429, 38, '', 'UNIT LAYANAN PELANGGAN MALANG KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(430, 38, '', 'UNIT LAYANAN PELANGGAN KEBONAGUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(431, 38, '', 'UNIT LAYANAN PELANGGAN DINOYO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(432, 38, '', 'UNIT LAYANAN PELANGGAN BLIMBING', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(433, 38, '', 'UNIT LAYANAN PELANGGAN DAMPIT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(434, 38, '', 'UNIT LAYANAN PELANGGAN SUMBERPUCUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(435, 38, '5111', 'UNIT PELAKSANA PELAYANAN PELANGGAN PASURUAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(436, 38, '', 'UNIT LAYANAN PELANGGAN BANGIL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(437, 38, '', 'UNIT LAYANAN PELANGGAN PANDAAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(438, 38, '', 'UNIT LAYANAN PELANGGAN PRIGEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(439, 38, '', 'UNIT LAYANAN PELANGGAN PROBOLINGGO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(440, 38, '', 'UNIT LAYANAN PELANGGAN KRAKSAAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(441, 38, '', 'UNIT LAYANAN PELANGGAN SUKOREJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(442, 38, '', 'UNIT LAYANAN PELANGGAN PASURUAN KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(443, 38, '', 'UNIT LAYANAN PELANGGAN GONDANG WETAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(444, 38, '', 'UNIT LAYANAN PELANGGAN GRATI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(445, 38, '5116', 'UNIT PELAKSANA PELAYANAN PELANGGAN KEDIRI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(446, 38, '', 'UNIT LAYANAN PELANGGAN BLITAR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(447, 38, '', 'UNIT LAYANAN PELANGGAN TUnit Layanan UNGAGUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(448, 38, '', 'UNIT LAYANAN PELANGGAN PARE', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(449, 38, '', 'UNIT LAYANAN PELANGGAN WLINGI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(450, 38, '', 'UNIT LAYANAN PELANGGAN CAMPURDARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(451, 38, '', 'UNIT LAYANAN PELANGGAN SUTOJAYAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(452, 38, '', 'UNIT LAYANAN PELANGGAN SRENGAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(453, 38, '', 'UNIT LAYANAN PELANGGAN NGUNUT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(454, 38, '', 'UNIT LAYANAN PELANGGAN KEDIRI KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(455, 38, '', 'UNIT LAYANAN PELANGGAN NGADILUWIH', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(456, 38, '', 'UNIT LAYANAN PELANGGAN GROGOL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(457, 38, '5117', 'UNIT PELAKSANA PELAYANAN PELANGGAN MADIUN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(458, 38, '', 'UNIT LAYANAN PELANGGAN NGAWI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(459, 38, '', 'UNIT LAYANAN PELANGGAN MAGETAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(460, 38, '', 'UNIT LAYANAN PELANGGAN MAOSPATI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(461, 38, '', 'UNIT LAYANAN PELANGGAN CARUBAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(462, 38, '', 'UNIT LAYANAN PELANGGAN DOLOPO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(463, 38, '', 'UNIT LAYANAN PELANGGAN MANTINGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(464, 38, '', 'UNIT LAYANAN PELANGGAN MADIUN KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(465, 38, '5122', 'UNIT PELAKSANA PELAYANAN PELANGGAN BOJONEGORO', '2018-11-07 01:42:11', '2018-11-07 01:42:11');
INSERT INTO `ma_area` (`id`, `id_ma_unit`, `business_area`, `nama`, `created_at`, `updated_at`) VALUES
(466, 38, '', 'UNIT LAYANAN PELANGGAN LAMONGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(467, 38, '', 'UNIT LAYANAN PELANGGAN BABAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(468, 38, '', 'UNIT LAYANAN PELANGGAN TUBAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(469, 38, '', 'UNIT LAYANAN PELANGGAN PADANGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(470, 38, '', 'UNIT LAYANAN PELANGGAN JATIROGO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(471, 38, '', 'UNIT LAYANAN PELANGGAN BRONDONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(472, 38, '', 'UNIT LAYANAN PELANGGAN BOJONEGORO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(473, 38, '', 'UNIT LAYANAN PELANGGAN SUMBERREJO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(474, 38, '5114', 'UNIT PELAKSANA PELAYANAN PELANGGAN JEMBER', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(475, 38, '', 'UNIT LAYANAN PELANGGAN KALISAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(476, 38, '', 'UNIT LAYANAN PELANGGAN AMBUnit Layanan U', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(477, 38, '', 'UNIT LAYANAN PELANGGAN RAMBIPUJI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(478, 38, '', 'UNIT LAYANAN PELANGGAN TANGGUnit Layanan ', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(479, 38, '', 'UNIT LAYANAN PELANGGAN KENCONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(480, 38, '', 'UNIT LAYANAN PELANGGAN LUMAJANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(481, 38, '', 'UNIT LAYANAN PELANGGAN KLAKAH', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(482, 38, '', 'UNIT LAYANAN PELANGGAN TEMPEH', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(483, 38, '', 'UNIT LAYANAN PELANGGAN JEMBER KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(484, 38, '5112', 'UNIT PELAKSANA PELAYANAN PELANGGAN SITUBONDO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(485, 38, '', 'UNIT LAYANAN PELANGGAN ASEMBAGUS', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(486, 38, '', 'UNIT LAYANAN PELANGGAN BESUKI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(487, 38, '', 'UNIT LAYANAN PELANGGAN BONDOWOSO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(488, 38, '', 'UNIT LAYANAN PELANGGAN PANARUKAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(489, 38, '', 'UNIT LAYANAN PELANGGAN WONOSARI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(490, 38, '5113', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANYUWANGI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(491, 38, '', 'UNIT LAYANAN PELANGGAN ROGOJAMPI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(492, 38, '', 'UNIT LAYANAN PELANGGAN GENTENG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(493, 38, '', 'UNIT LAYANAN PELANGGAN MUNCAR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(494, 38, '', 'UNIT LAYANAN PELANGGAN JAJAG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(495, 38, '', 'UNIT LAYANAN PELANGGAN BYWG KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(496, 38, '5119', 'UNIT PELAKSANA PELAYANAN PELANGGAN PAMEKASAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(497, 38, '', 'UNIT LAYANAN PELANGGAN KEPUnit Layanan AUAN KANGEAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(498, 38, '', 'UNIT LAYANAN PELANGGAN KAMAL', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(499, 38, '', 'UNIT LAYANAN PELANGGAN BANGKALAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(500, 38, '', 'UNIT LAYANAN PELANGGAN BLEGA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(501, 38, '', 'UNIT LAYANAN PELANGGAN KETAPANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(502, 38, '', 'UNIT LAYANAN PELANGGAN SAMPANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(503, 38, '', 'UNIT LAYANAN PELANGGAN PRENDUAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(504, 38, '', 'UNIT LAYANAN PELANGGAN SUMENEP', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(505, 38, '', 'UNIT LAYANAN PELANGGAN AMBUNTEN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(506, 38, '', 'UNIT LAYANAN PELANGGAN WARU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(507, 38, '', 'UNIT LAYANAN PELANGGAN PAMEKASAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(508, 38, '5125', 'UNIT PELAKSANA PELAYANAN PELANGGAN PONOROGO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(509, 38, '', 'UNIT LAYANAN PELANGGAN PONOROGO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(510, 38, '', 'UNIT LAYANAN PELANGGAN PACITAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(511, 38, '', 'UNIT LAYANAN PELANGGAN BALONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(512, 38, '', 'UNIT LAYANAN PELANGGAN TRENGGALEK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(513, 38, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN JAWA TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(514, 38, '5156', 'UNIT PELAKSANA PENGATUR DISTRIBUSI JAWA TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(515, 39, '5301', 'PT PLN (PERSERO) UNIT INDUK DISTRIBUSI JAWA BARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(516, 39, '5324', 'UNIT PELAKSANA PELAYANAN PELANGGAN KARAWANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(517, 39, '', 'UNIT LAYANAN PELANGGAN CIKAMPEK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(518, 39, '', 'UNIT LAYANAN PELANGGAN KARAWANG KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(519, 39, '', 'UNIT LAYANAN PELANGGAN KOSAMBI', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(520, 39, '', 'UNIT LAYANAN PELANGGAN PRIMA KARAWANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(521, 39, '', 'UNIT LAYANAN PELANGGAN RENGASDENGKLOK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(522, 39, '5320', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANDUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(523, 39, '', 'UNIT LAYANAN PELANGGAN PRIMA PRIANGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(524, 39, '', 'UNIT LAYANAN PELANGGAN BANDUNG UTARA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(525, 39, '', 'UNIT LAYANAN PELANGGAN BANDUNG SELATAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(526, 39, '', 'UNIT LAYANAN PELANGGAN BANDUNG TIMUR', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(527, 39, '', 'UNIT LAYANAN PELANGGAN BANDUNG BARAT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(528, 39, '', 'UNIT LAYANAN PELANGGAN UJUNG BERUNG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(529, 39, '', 'UNIT LAYANAN PELANGGAN KOPO', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(530, 39, '', 'UNIT LAYANAN PELANGGAN CIJAWURA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(531, 39, '', 'UNIT PELAKSANA PELAYANAN PELANGGAN DEPOK', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(532, 39, '', 'UNIT LAYANAN PELANGGAN DEPOK KOTA', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(533, 39, '', 'UNIT LAYANAN PELANGGAN SAWANGAN', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(534, 39, '', 'UNIT LAYANAN PELANGGAN CIBINONG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(535, 39, '', 'UNIT LAYANAN PELANGGAN CIMANGGIS', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(536, 39, '', 'UNIT LAYANAN PELANGGAN BOJONG GEDE', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(537, 39, '5320', 'UNIT PELAKSANA PELAYANAN PELANGGAN GARUT', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(538, 39, '', 'UNIT LAYANAN PELANGGAN CIBATU', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(539, 39, '', 'UNIT LAYANAN PELANGGAN CIKAJANG', '2018-11-07 01:42:11', '2018-11-07 01:42:11'),
(540, 39, '', 'UNIT LAYANAN PELANGGAN GARUT KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(541, 39, '', 'UNIT LAYANAN PELANGGAN LELES', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(542, 39, '', 'UNIT LAYANAN PELANGGAN PAMEUNGPEUK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(543, 39, '5327', 'UNIT PELAKSANA PELAYANAN PELANGGAN GUNUNG PUTRI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(544, 39, '', 'UNIT LAYANAN PELANGGAN CILEUNGSI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(545, 39, '', 'UNIT LAYANAN PELANGGAN CITEUREUP', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(546, 39, '', 'UNIT LAYANAN PELANGGAN JONGGOL', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(547, 39, '5316', 'UNIT PELAKSANA PELAYANAN PELANGGAN BOGOR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(548, 39, '', 'UNIT LAYANAN PELANGGAN BOGOR BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(549, 39, '', 'UNIT LAYANAN PELANGGAN BOGOR KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(550, 39, '', 'UNIT LAYANAN PELANGGAN BOGOR TIMUR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(551, 39, '', 'UNIT LAYANAN PELANGGAN CIPAYUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(552, 39, '', 'UNIT LAYANAN PELANGGAN JASINGA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(553, 39, '', 'UNIT LAYANAN PELANGGAN LEUWILIANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(554, 39, '', 'UNIT LAYANAN PELANGGAN PAKUAN BOGOR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(555, 39, '5321', 'UNIT PELAKSANA PELAYANAN PELANGGAN MAJALAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(556, 39, '', 'UNIT LAYANAN PELANGGAN BALE ENDAH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(557, 39, '', 'UNIT LAYANAN PELANGGAN BANJARAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(558, 39, '', 'UNIT LAYANAN PELANGGAN MAJALAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(559, 39, '', 'UNIT LAYANAN PELANGGAN PRIMA MAJALAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(560, 39, '', 'UNIT LAYANAN PELANGGAN RANCAEKEK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(561, 39, '', 'UNIT LAYANAN PELANGGAN SOREANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(562, 39, '5312', 'UNIT PELAKSANA PELAYANAN PELANGGAN TASIKMALAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(563, 39, '', 'UNIT LAYANAN PELANGGAN TASIKMALAYA KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(564, 39, '', 'UNIT LAYANAN PELANGGAN SINGAPARNA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(565, 39, '', 'UNIT LAYANAN PELANGGAN KARANGNUNGGAL', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(566, 39, '', 'UNIT LAYANAN PELANGGAN RAJAPOLAH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(567, 39, '', 'UNIT LAYANAN PELANGGAN CIAMIS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(568, 39, '', 'UNIT LAYANAN PELANGGAN BANJAR KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(569, 39, '', 'UNIT LAYANAN PELANGGAN PANGANDARAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(570, 39, '5315', 'UNIT PELAKSANA PELAYANAN PELANGGAN SUKABUMI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(571, 39, '', 'UNIT LAYANAN PELANGGAN CIBADAK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(572, 39, '', 'UNIT LAYANAN PELANGGAN SUKABUMI KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(573, 39, '', 'UNIT LAYANAN PELANGGAN CIKEMBAR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(574, 39, '', 'UNIT LAYANAN PELANGGAN SUKARAJA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(575, 39, '', 'UNIT LAYANAN PELANGGAN PELABUHAN RATU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(576, 39, '', 'UNIT LAYANAN PELANGGAN CICURUG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(577, 39, '5319', 'UNIT PELAKSANA PELAYANAN PELANGGAN CIMAHI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(578, 39, '', 'UNIT LAYANAN PELANGGAN CILILIN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(579, 39, '', 'UNIT LAYANAN PELANGGAN CIMAHI KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(580, 39, '', 'UNIT LAYANAN PELANGGAN CIMAHI SELATAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(581, 39, '', 'UNIT LAYANAN PELANGGAN LEMBANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(582, 39, '', 'UNIT LAYANAN PELANGGAN PADALARANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(583, 39, '', 'UNIT LAYANAN PELANGGAN PRIMA CIBABAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(584, 39, '', 'UNIT LAYANAN PELANGGAN RAJAMANDALA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(585, 39, '5314', 'UNIT PELAKSANA PELAYANAN PELANGGAN CIANJUR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(586, 39, '', 'UNIT LAYANAN PELANGGAN CIANJUR KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(587, 39, '', 'UNIT LAYANAN PELANGGAN CIPANAS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(588, 39, '', 'UNIT LAYANAN PELANGGAN MANDE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(589, 39, '', 'UNIT LAYANAN PELANGGAN SUKANAGARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(590, 39, '', 'UNIT LAYANAN PELANGGAN TANGGEUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(591, 39, '5322', 'UNIT PELAKSANA PELAYANAN PELANGGAN BEKASI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(592, 39, '', 'UNIT LAYANAN PELANGGAN BEKASI KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(593, 39, '', 'UNIT LAYANAN PELANGGAN BABELAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(594, 39, '', 'UNIT LAYANAN PELANGGAN BANTAR GEBANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(595, 39, '', 'UNIT LAYANAN PELANGGAN CIBITUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(596, 39, '', 'UNIT LAYANAN PELANGGAN CIKARANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(597, 39, '', 'UNIT LAYANAN PELANGGAN LEMAH ABANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(598, 39, '', 'UNIT LAYANAN PELANGGAN MEDAN SATRIA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(599, 39, '', 'UNIT LAYANAN PELANGGAN MUSTIKA JAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(600, 39, '', 'UNIT LAYANAN PELANGGAN PRIMA BEKASI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(601, 39, '', 'UNIT LAYANAN PELANGGAN TAMBUN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(602, 39, '5318', 'UNIT PELAKSANA PELAYANAN PELANGGAN PURWAKARTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(603, 39, '', 'UNIT LAYANAN PELANGGAN PAGADEN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(604, 39, '', 'UNIT LAYANAN PELANGGAN PAMANUKAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(605, 39, '', 'UNIT LAYANAN PELANGGAN PLERED', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(606, 39, '', 'UNIT LAYANAN PELANGGAN PURWAKARTA KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(607, 39, '', 'UNIT LAYANAN PELANGGAN SUBANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(608, 39, '', 'UNIT PELAKSANA PELAYANAN PELANGGAN SUMEDANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(609, 39, '', 'UNIT LAYANAN PELANGGAN JATIWANGI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(610, 39, '', 'UNIT LAYANAN PELANGGAN MAJALENGKA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(611, 39, '', 'UNIT LAYANAN PELANGGAN SUMEDANG KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(612, 39, '', 'UNIT LAYANAN PELANGGAN TANJUNG SARI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(613, 39, '5325', 'UNIT PELAKSANA PELAYANAN PELANGGAN CIREBON', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(614, 39, '', 'UNIT LAYANAN PELANGGAN CILEDUG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(615, 39, '', 'UNIT LAYANAN PELANGGAN CILIMUS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(616, 39, '', 'UNIT LAYANAN PELANGGAN CIREBON KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(617, 39, '', 'UNIT LAYANAN PELANGGAN HAURGEUnit Layanan IS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(618, 39, '', 'UNIT LAYANAN PELANGGAN INDRAMAYU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(619, 39, '', 'UNIT LAYANAN PELANGGAN JATIBARANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(620, 39, '', 'UNIT LAYANAN PELANGGAN KUNINGAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(621, 39, '', 'UNIT LAYANAN PELANGGAN SUMBER', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(622, 39, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI JAWA BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(623, 39, '5325', 'UNIT PELAKSANA PENGATUR DISTRIBUSI JAWA BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(624, 40, '6701', 'PT PLN (PERSERO) UNIT INDUK DISTRIBUSI LAMPUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(625, 40, '', 'UNIT PELAKSANA PROYEK KETENAGALISTIKAN LAMPUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(626, 40, '6756', 'UNIT PELAKSANA PENGATUR DISTRIBUSI LAMPUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(627, 40, '6712', 'UNIT PELAKSANA PELAYANAN PELANGGAN KOTABUMI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(628, 40, '', 'UNIT LAYANAN PELANGGAN PUnit Layanan UNG KENCANA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(629, 40, '', 'UNIT LAYANAN PELANGGAN BLAMBANGAN UMPU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(630, 40, '', 'UNIT LAYANAN PELANGGAN BUKIT KEMUNING', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(631, 40, '', 'UNIT LAYANAN PELANGGAN BUMI ABUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(632, 40, '', 'UNIT LAYANAN PELANGGAN LIWA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(633, 40, '', 'UNIT LAYANAN PELANGGAN MENGGALA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(634, 40, '6713', 'UNIT PELAKSANA PELAYANAN PELANGGAN METRO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(635, 40, '', 'UNIT LAYANAN PELANGGAN BANDARJAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(636, 40, '', 'UNIT LAYANAN PELANGGAN KALIREJO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(637, 40, '', 'UNIT LAYANAN PELANGGAN KOTA AGUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(638, 40, '', 'UNIT LAYANAN PELANGGAN KOTA METRO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(639, 40, '', 'UNIT LAYANAN PELANGGAN PRINGSEWU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(640, 40, '', 'UNIT LAYANAN PELANGGAN RUMBIA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(641, 40, '', 'UNIT LAYANAN PELANGGAN SRIBHAWONO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(642, 40, '', 'UNIT LAYANAN PELANGGAN SUKADANA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(643, 40, '', 'UNIT LAYANAN PELANGGAN TALANG PADANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(644, 40, '6711', 'UNIT PELAKSANA PELAYANAN PELANGGAN TANJUNG KARANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(645, 40, '', 'UNIT LAYANAN PELANGGAN KALIANDA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(646, 40, '', 'UNIT LAYANAN PELANGGAN KARANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(647, 40, '', 'UNIT LAYANAN PELANGGAN NATAR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(648, 40, '', 'UNIT LAYANAN PELANGGAN SIDOMUnit Layanan YO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(649, 40, '', 'UNIT LAYANAN PELANGGAN SUTAMI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(650, 40, '', 'UNIT LAYANAN PELANGGAN TELUK BETUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(651, 40, '', 'UNIT LAYANAN PELANGGAN WAY HALIM', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(652, 41, '7301', 'PT PLN (PERSERO) UNIT INDUK WILAYAH SUnit Layanan AWESI UTARA, SUnit Layanan AWESI TENGAH DAN GORONTALO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(653, 41, '7356', 'UNIT PELAKSANA PENGATUR DISTRIBUSI SUnit Layanan AWESI UTARA, SUnit Layanan AWESI TENGAH DAN GORONTALO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(654, 41, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUnit Layanan AWESI TENGAH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(655, 41, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUnit Layanan AWESI UTARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(656, 41, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI GORONTALO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(657, 41, '', 'UNIT PELAKSANA PELAYANAN PELANGGAN GORONTALO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(658, 41, '', 'UNIT LAYANAN PELANGGAN KWANDANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(659, 41, '', 'UNIT LAYANAN PELANGGAN LIMBOTO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(660, 41, '', 'UNIT LAYANAN PELANGGAN MARISA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(661, 41, '', 'UNIT LAYANAN PELANGGAN TELAGA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(662, 41, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TELAGA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(663, 41, '7314', 'UNIT PELAKSANA PELAYANAN PELANGGAN KOTAMOBAGU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(664, 41, '', 'UNIT LAYANAN PELANGGAN BOLMUT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(665, 41, '', 'UNIT LAYANAN PELANGGAN IMANDI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(666, 41, '', 'UNIT LAYANAN PELANGGAN INOBONTO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(667, 41, '', 'UNIT LAYANAN PELANGGAN MODAYAG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(668, 41, '', 'UNIT LAYANAN PELANGGAN MOLIBAGU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(669, 41, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KOTAMOBAGU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(670, 41, '7316', 'UNIT PELAKSANA PELAYANAN PELANGGAN LUWUK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(671, 41, '', 'UNIT LAYANAN PELANGGAN AMPANA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(672, 41, '', 'UNIT LAYANAN PELANGGAN BANGGAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(673, 41, '', 'UNIT LAYANAN PELANGGAN TOILI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(674, 41, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL LUWUK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(675, 41, '7311', 'UNIT PELAKSANA PELAYANAN PELANGGAN MANADO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(676, 41, '', 'UNIT LAYANAN PELANGGAN AIRMADIDI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(677, 41, '', 'UNIT LAYANAN PELANGGAN AMURANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(678, 41, '', 'UNIT LAYANAN PELANGGAN BITUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(679, 41, '', 'UNIT LAYANAN PELANGGAN KAWANGKOAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(680, 41, '', 'UNIT LAYANAN PELANGGAN MANADO UTARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(681, 41, '', 'UNIT LAYANAN PELANGGAN MOTOLING', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(682, 41, '', 'UNIT LAYANAN PELANGGAN PANIKI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(683, 41, '', 'UNIT LAYANAN PELANGGAN RATAHAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(684, 41, '', 'UNIT LAYANAN PELANGGAN TOMOHON', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(685, 41, '', 'UNIT LAYANAN PELANGGAN TONDANO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(686, 41, '', 'UNIT LAYANAN PELANGGAN MANADO SELATAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(687, 41, '7312', 'UNIT PELAKSANA PELAYANAN PELANGGAN PALU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(688, 41, '', 'UNIT LAYANAN PELANGGAN POSO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(689, 41, '', 'UNIT LAYANAN PELANGGAN KAMONJI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(690, 41, '', 'UNIT LAYANAN PELANGGAN KOLONEDALE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(691, 41, '', 'UNIT LAYANAN PELANGGAN PALU KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(692, 41, '', 'UNIT LAYANAN PELANGGAN PARIGI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(693, 41, '', 'UNIT LAYANAN PELANGGAN TENTENA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(694, 41, '', 'UNIT LAYANAN PELANGGAN TAMBU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(695, 41, '', 'UNIT LAYANAN PELANGGAN TAWAELI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(696, 41, '', 'UNIT LAYANAN PELANGGAN DONGGALA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(697, 41, '', 'UNIT LAYANAN PELANGGAN BUNGKU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(698, 41, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SILAE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(699, 41, '7315', 'UNIT PELAKSANA PELAYANAN PELANGGAN TAHUNA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(700, 41, '', 'UNIT LAYANAN PELANGGAN BEO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(701, 41, '', 'UNIT LAYANAN PELANGGAN LIRUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(702, 41, '', 'UNIT LAYANAN PELANGGAN MELONGUANE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(703, 41, '', 'UNIT LAYANAN PELANGGAN TAGUnit Layanan ANDANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(704, 41, '', 'UNIT LAYANAN PELANGGAN TAMAKO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(705, 41, '', 'UNIT LAYANAN PELANGGAN PETTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(706, 41, '', 'UNIT LAYANAN PELANGGAN SIAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(707, 41, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TAHUNA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(708, 41, '7317', 'UNIT PELAKSANA PELAYANAN PELANGGAN TOLI-TOLI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(709, 41, '', 'UNIT LAYANAN PELANGGAN BANGKIR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(710, 41, '', 'UNIT LAYANAN PELANGGAN KOTARAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(711, 41, '', 'UNIT LAYANAN PELANGGAN LEOK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(712, 41, '', 'UNIT LAYANAN PELANGGAN MOUTONG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(713, 41, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TOLI-TOLI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(714, 42, '7401', 'PT PLN (PERSERO) UNIT INDUK WILAYAH SUnit Layanan AWESI SELATAN,SUnit Layanan AWESI TENGGARA DAN SUnit Layanan AWESI BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(715, 42, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUnit Layanan SEL', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(716, 42, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUnit Layanan TRA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(717, 42, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUnit Layanan BAR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(718, 42, '7428', 'UNIT PELAKSANA PELAYANAN PELANGGAN MAKASSAR SELATAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(719, 42, '', 'UNIT LAYANAN PELANGGAN PANAKKUKANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(720, 42, '', 'UNIT LAYANAN PELANGGAN MALINO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(721, 42, '', 'UNIT LAYANAN PELANGGAN KALEBAJENG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(722, 42, '', 'UNIT LAYANAN PELANGGAN MATTOANGING', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(723, 42, '', 'UNIT LAYANAN PELANGGAN SUNGGUMINASA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(724, 42, '', 'UNIT LAYANAN PELANGGAN TAKALAR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(725, 42, '7427', 'UNIT PELAKSANA PELAYANAN PELANGGAN MAKASSAR UTARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(726, 42, '', 'UNIT LAYANAN PELANGGAN DAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(727, 42, '', 'UNIT LAYANAN PELANGGAN PANGKEP', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(728, 42, '', 'UNIT LAYANAN PELANGGAN MAROS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(729, 42, '', 'UNIT LAYANAN PELANGGAN KAREBOSI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(730, 42, '7412', 'UNIT PELAKSANA PELAYANAN PELANGGAN PARE-PARE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(731, 42, '', 'UNIT LAYANAN PELANGGAN MATTIROTASI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(732, 42, '', 'UNIT LAYANAN PELANGGAN BARRU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(733, 42, '', 'UNIT LAYANAN PELANGGAN PANGSID', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(734, 42, '', 'UNIT LAYANAN PELANGGAN RAPPANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(735, 42, '', 'UNIT LAYANAN PELANGGAN TANRU TEDONG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(736, 42, '', 'UNIT LAYANAN PELANGGAN PAJALESANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(737, 42, '', 'UNIT LAYANAN PELANGGAN WATANG SOPPENG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(738, 42, '7413', 'UNIT PELAKSANA PELAYANAN PELANGGAN WATAMPONE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(739, 42, '', 'UNIT LAYANAN PELANGGAN HASANUDDIN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(740, 42, '', 'UNIT LAYANAN PELANGGAN SENGKANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(741, 42, '', 'UNIT LAYANAN PELANGGAN PARIA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(742, 42, '', 'UNIT LAYANAN PELANGGAN PATANGKAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(743, 42, '', 'UNIT LAYANAN PELANGGAN Unit Layanan OE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(744, 42, '', 'UNIT LAYANAN PELANGGAN TELLU BOCCOE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(745, 42, '7414', 'UNIT PELAKSANA PELAYANAN PELANGGAN KENDARI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(746, 42, '', 'UNIT LAYANAN PELANGGAN WUA-WUA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(747, 42, '', 'UNIT LAYANAN PELANGGAN BENUBENUA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(748, 42, '', 'UNIT LAYANAN PELANGGAN KOLAKA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(749, 42, '', 'UNIT LAYANAN PELANGGAN UNAAHA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(750, 42, '', 'UNIT LAYANAN PELANGGAN KONAWE SELATAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(751, 42, '', 'UNIT LAYANAN PELANGGAN BOMBANA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(752, 42, '', 'UNIT LAYANAN PELANGGAN KOLAKA UTARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(753, 42, '7416', 'UNIT PELAKSANA PELAYANAN PELANGGAN PALOPO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(754, 42, '', 'UNIT LAYANAN PELANGGAN PALOPO KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(755, 42, '', 'UNIT LAYANAN PELANGGAN MASAMBA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(756, 42, '', 'UNIT LAYANAN PELANGGAN RANTEPAO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(757, 42, '', 'UNIT LAYANAN PELANGGAN MAKALE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(758, 42, '', 'UNIT LAYANAN PELANGGAN BELOPA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(759, 42, '', 'UNIT LAYANAN PELANGGAN MALILI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(760, 42, '', 'UNIT LAYANAN PELANGGAN TOMONI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(761, 42, '7415', 'UNIT PELAKSANA PELAYANAN PELANGGAN PINRANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(762, 42, '', 'UNIT LAYANAN PELANGGAN WATANG SAWITTO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(763, 42, '', 'UNIT LAYANAN PELANGGAN KARIANGO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(764, 42, '', 'UNIT LAYANAN PELANGGAN PEKKABATA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(765, 42, '', 'UNIT LAYANAN PELANGGAN LAKAWAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(766, 42, '', 'UNIT LAYANAN PELANGGAN ENREKANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(767, 42, '7417', 'UNIT PELAKSANA PELAYANAN PELANGGAN BUnit Layanan UKUMBA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(768, 42, '', 'UNIT LAYANAN PELANGGAN PANRITA LOPI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(769, 42, '', 'UNIT LAYANAN PELANGGAN SINJAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(770, 42, '', 'UNIT LAYANAN PELANGGAN JENEPONTO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(771, 42, '', 'UNIT LAYANAN PELANGGAN BANTAENG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(772, 42, '', 'UNIT LAYANAN PELANGGAN SELAYAR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(773, 42, '', 'UNIT LAYANAN PELANGGAN TANETE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(774, 42, '', 'UNIT LAYANAN PELANGGAN KALUMPANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(775, 42, '7418', 'UNIT PELAKSANA PELAYANAN PELANGGAN BAU-BAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(776, 42, '', 'UNIT LAYANAN PELANGGAN BAU-BAU KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(777, 42, '', 'UNIT LAYANAN PELANGGAN RAHA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(778, 42, '', 'UNIT LAYANAN PELANGGAN WANGI-WANGI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(779, 42, '', 'UNIT LAYANAN PELANGGAN PASAR WAJO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(780, 42, '', 'UNIT LAYANAN PELANGGAN MAWASANGKA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(781, 42, '7419', 'UNIT PELAKSANA PELAYANAN PELANGGAN MAMUJU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(782, 42, '', 'UNIT LAYANAN PELANGGAN MANAKARRA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(783, 42, '', 'UNIT LAYANAN PELANGGAN MAJENE', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(784, 42, '', 'UNIT LAYANAN PELANGGAN WONOMUnit Layanan YO', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(785, 42, '', 'UNIT LAYANAN PELANGGAN POLEWALI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(786, 42, '', 'UNIT LAYANAN PELANGGAN MAMASA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(787, 42, '', 'UNIT LAYANAN PELANGGAN PASANGKAYU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(788, 42, '7424', 'UNIT PELAKSANA PENGATUR DISTRIBUSI MAKASSAR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(789, 43, '6801', 'PT PLN (PERSERO) UNIT INDUK WILAYAH KALIMANTAN BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(790, 43, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI KALIMANTAN BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(791, 43, '6856', 'UNIT PELAKSANA PENGATUR DISTRIBUSI KALIMANTAN BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(792, 43, '6812', 'UNIT PELAKSANA PELAYANAN PELANGGAN PONTIANAK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(793, 43, '', 'UNIT LAYANAN PELANGGAN KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(794, 43, '', 'UNIT LAYANAN PELANGGAN MEMPAWAH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(795, 43, '', 'UNIT LAYANAN PELANGGAN NGABANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(796, 43, '', 'UNIT LAYANAN PELANGGAN RASAU JAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(797, 43, '', 'UNIT LAYANAN PELANGGAN SIANTAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(798, 43, '', 'UNIT LAYANAN PELANGGAN SUNGAI JAWI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(799, 43, '', 'UNIT LAYANAN PELANGGAN SUNGAI KAKAP', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(800, 43, '6811', 'UNIT PELAKSANA PELAYANAN PELANGGAN SINGKAWANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(801, 43, '', 'UNIT LAYANAN PELANGGAN SINGKAWANG KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(802, 43, '', 'UNIT LAYANAN PELANGGAN BENGKAYANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(803, 43, '', 'UNIT LAYANAN PELANGGAN PEMANGKAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(804, 43, '', 'UNIT LAYANAN PELANGGAN SAMBAS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(805, 43, '', 'UNIT LAYANAN PELANGGAN SEI DURI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(806, 43, '', 'UNIT LAYANAN PELANGGAN SEKURA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(807, 43, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SAMBAS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(808, 43, '6813', 'UNIT PELAKSANA PELAYANAN PELANGGAN SANGGAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(809, 43, '', 'UNIT LAYANAN PELANGGAN SANGGAU KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(810, 43, '', 'UNIT LAYANAN PELANGGAN BALAI KARANGAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(811, 43, '', 'UNIT LAYANAN PELANGGAN NANGA PINOH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(812, 43, '', 'UNIT LAYANAN PELANGGAN PUTUSSIBAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(813, 43, '', 'UNIT LAYANAN PELANGGAN SEKADAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(814, 43, '', 'UNIT LAYANAN PELANGGAN SINTANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(815, 43, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL MENYURAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(816, 43, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SEMBOJA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(817, 43, '6814', 'UNIT PELAKSANA PELAYANAN PELANGGAN KETAPANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(818, 43, '', 'UNIT LAYANAN PELANGGAN KETAPANG KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(819, 43, '', 'UNIT LAYANAN PELANGGAN SANDAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(820, 43, '', 'UNIT LAYANAN PELANGGAN SUKADANA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(821, 43, '', 'UNIT LAYANAN PELANGGAN TUMBANG TITI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(822, 43, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SUKAHARJA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(823, 44, '7101', 'PT PLN (PERSERO) UNIT INDUK WILAYAH KALIMANTAN SELATAN DAN KALIMANTAN TENGAH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(824, 44, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI KALIMANTAN TENGAH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(825, 44, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI KALIMANTAN SELATAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(826, 44, '7156', 'UNIT PELAKSANA PENGATUR DISTRIBUSI KALIMANTAN SELATAN DAN KALIMANTAN TENGAH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(827, 44, '7111', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANJARMASIN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(828, 44, '', 'UNIT LAYANAN PELANGGAN LAMBUNG MANGKURAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(829, 44, '', 'UNIT LAYANAN PELANGGAN AHMAD YANI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(830, 44, '', 'UNIT LAYANAN PELANGGAN MARTAPURA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(831, 44, '', 'UNIT LAYANAN PELANGGAN BANJARBARU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(832, 44, '', 'UNIT LAYANAN PELANGGAN PELAIHARI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(833, 44, '', 'UNIT LAYANAN PELANGGAN MARABAHAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(834, 44, '', 'UNIT LAYANAN PELANGGAN GAMBUT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(835, 44, '7113', 'UNIT PELAKSANA PELAYANAN PELANGGAN BARABAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(836, 44, '', 'UNIT LAYANAN PELANGGAN TANJUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(837, 44, '', 'UNIT LAYANAN PELANGGAN AMUNTAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(838, 44, '', 'UNIT LAYANAN PELANGGAN KANDANGAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(839, 44, '', 'UNIT LAYANAN PELANGGAN DAHA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(840, 44, '', 'UNIT LAYANAN PELANGGAN RANTAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(841, 44, '', 'UNIT LAYANAN PELANGGAN BINUANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(842, 44, '', 'UNIT LAYANAN PELANGGAN PARINGIN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(843, 44, '7115', 'UNIT PELAKSANA PELAYANAN PELANGGAN KOTA BARU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(844, 44, '', 'UNIT LAYANAN PELANGGAN BATU LICIN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(845, 44, '', 'UNIT LAYANAN PELANGGAN SATUI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(846, 44, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KOTA BARU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(847, 44, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL PAGATAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(848, 44, '7114', 'UNIT PELAKSANA PELAYANAN PELANGGAN KUALA KAPUAS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(849, 44, '', 'UNIT LAYANAN PELANGGAN BUNTOK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(850, 44, '', 'UNIT LAYANAN PELANGGAN MUARA TEWEH', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(851, 44, '', 'UNIT LAYANAN PELANGGAN PUnit Layanan ANG PISAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(852, 44, '', 'UNIT LAYANAN PELANGGAN TAMIANG LAYANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(853, 44, '', 'UNIT LAYANAN PELANGGAN PURUK CAHU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(854, 44, '7112', 'UNIT PELAKSANA PELAYANAN PELANGGAN PALANGKA RAYA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(855, 44, '', 'UNIT LAYANAN PELANGGAN PANGKALAN BUN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(856, 44, '', 'UNIT LAYANAN PELANGGAN SAMPIT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(857, 44, '', 'UNIT LAYANAN PELANGGAN KUALA PEMBUANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(858, 44, '', 'UNIT LAYANAN PELANGGAN KASONGAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(859, 44, '', 'UNIT LAYANAN PELANGGAN KUALA KURUN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(860, 44, '', 'UNIT LAYANAN PELANGGAN NANGA BUnit Layanan IK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(861, 44, '', 'UNIT LAYANAN PELANGGAN SUKAMARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(862, 44, '', 'UNIT LAYANAN PELANGGAN PALANGKARAYA BARAT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(863, 44, '', 'UNIT LAYANAN PELANGGAN PALANGKARAYA TIMUR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(864, 44, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BAAMANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(865, 44, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KUMAI', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(866, 44, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KAHAYAN BARU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(867, 45, '7201', 'PT PLN (PERSERO) UNIT INDUK WILAYAH KALIMANTAN TIMUR DAN KALIMANTAN UTARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(868, 45, '7211', 'UNIT PELAKSANA PELAYANAN PELANGGAN BALIKPAPAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(869, 45, '', 'UNIT LAYANAN PELANGGAN BALIKPAPAN SELATAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(870, 45, '', 'UNIT LAYANAN PELANGGAN BALIKPAPAN UTARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(871, 45, '', 'UNIT LAYANAN PELANGGAN LONGIKIS', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(872, 45, '', 'UNIT LAYANAN PELANGGAN PETUNG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(873, 45, '', 'UNIT LAYANAN PELANGGAN SAMBOJA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(874, 45, '', 'UNIT LAYANAN PELANGGAN TANAH GROGOT', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(875, 45, '7212', 'UNIT PELAKSANA PELAYANAN PELANGGAN SAMARINDA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(876, 45, '', 'UNIT LAYANAN PELANGGAN KOTA BANGUN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(877, 45, '', 'UNIT LAYANAN PELANGGAN MELAK', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(878, 45, '', 'UNIT LAYANAN PELANGGAN SAMARINDA ILIR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(879, 45, '', 'UNIT LAYANAN PELANGGAN SAMARINDA KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(880, 45, '', 'UNIT LAYANAN PELANGGAN SAMARINDA SEBERANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(881, 45, '', 'UNIT LAYANAN PELANGGAN SAMARINDA Unit Layanan U', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(882, 45, '', 'UNIT LAYANAN PELANGGAN TENGGARONG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(883, 45, '7213', 'UNIT PELAKSANA PELAYANAN PELANGGAN BERAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(884, 45, '', 'UNIT LAYANAN PELANGGAN MALINAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(885, 45, '', 'UNIT LAYANAN PELANGGAN NUNUKAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(886, 45, '', 'UNIT LAYANAN PELANGGAN TANJUNG SELOR', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(887, 45, '', 'UNIT LAYANAN PELANGGAN TANJUNG REDEB', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(888, 45, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BERAU', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(889, 45, '7214', 'UNIT PELAKSANA PELAYANAN PELANGGAN BONTANG', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(890, 45, '', 'UNIT LAYANAN PELANGGAN SANGATTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(891, 45, '', 'UNIT LAYANAN PELANGGAN BONTANG KOTA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(892, 45, '', 'UNIT PELAKSANA PELAYANAN PELANGGAN TARAKAN', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(893, 45, '7256', 'UNIT PELAKSANA PENGATUR DISTRIBUSI KALIMANTAN TIMUR DAN KALIMANTAN UTARA', '2018-11-07 01:42:12', '2018-11-07 01:42:12'),
(894, 45, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI KALIMANTAN TIMUR', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(895, 45, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI KALIMANTAN UTARA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(896, 46, '7501', 'PT PLN (PERSERO) UNIT INDUK WILAYAH MALUKU DAN MALUKU UTARA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(897, 46, '7512', 'UNIT PELAKSANA PELAYANAN PELANGGAN AMBON', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(898, 46, '', 'UNIT LAYANAN PELANGGAN MAKO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(899, 46, '', 'UNIT LAYANAN PELANGGAN NAMROLE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(900, 46, '', 'UNIT LAYANAN PELANGGAN BAGUALA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(901, 46, '', 'UNIT LAYANAN PELANGGAN BANDA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(902, 46, '', 'UNIT LAYANAN PELANGGAN HARUKU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(903, 46, '', 'UNIT LAYANAN PELANGGAN HITU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(904, 46, '', 'UNIT LAYANAN PELANGGAN NAMLEA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(905, 46, '', 'UNIT LAYANAN PELANGGAN NUSANIWE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(906, 46, '', 'UNIT LAYANAN PELANGGAN TUnit Layanan EHU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(907, 46, '', 'UNIT LAYANAN PELANGGAN SAPARUA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(908, 46, '', 'UNIT LAYANAN PELANGGAN AMBON KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(909, 46, '7515', 'UNIT PELAKSANA PELAYANAN PELANGGAN MASOHI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(910, 46, '', 'UNIT LAYANAN PELANGGAN BUnit Layanan A', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(911, 46, '', 'UNIT LAYANAN PELANGGAN PIRU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(912, 46, '', 'UNIT LAYANAN PELANGGAN MASOHI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(913, 46, '', 'UNIT LAYANAN PELANGGAN KOBISONTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(914, 46, '', 'UNIT LAYANAN PELANGGAN KAIRATU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(915, 46, '7514', 'UNIT PELAKSANA PELAYANAN PELANGGAN SOFIFI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(916, 46, '', 'UNIT LAYANAN PELANGGAN JAILOLO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(917, 46, '', 'UNIT LAYANAN PELANGGAN TOBELO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(918, 46, '', 'UNIT LAYANAN PELANGGAN SOFIFI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(919, 46, '', 'UNIT LAYANAN PELANGGAN MABA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(920, 46, '', 'UNIT LAYANAN PELANGGAN WEDA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(921, 46, '', 'UNIT LAYANAN PELANGGAN MABA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(922, 46, '7511', 'UNIT PELAKSANA PELAYANAN PELANGGAN TERNATE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(923, 46, '', 'UNIT LAYANAN PELANGGAN BOBONG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(924, 46, '', 'UNIT LAYANAN PELANGGAN LAIWUI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(925, 46, '', 'UNIT LAYANAN PELANGGAN SAKETA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(926, 46, '', 'UNIT LAYANAN PELANGGAN DOFA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(927, 46, '', 'UNIT LAYANAN PELANGGAN BACAN', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(928, 46, '', 'UNIT LAYANAN PELANGGAN SANANA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(929, 46, '', 'UNIT LAYANAN PELANGGAN SOA SIU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(930, 46, '', 'UNIT LAYANAN PELANGGAN TERNATE SELATAN', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(931, 46, '7513', 'UNIT PELAKSANA PELAYANAN PELANGGAN TUAL', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(932, 46, '', 'UNIT LAYANAN PELANGGAN ELAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(933, 46, '', 'UNIT LAYANAN PELANGGAN MOA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(934, 46, '', 'UNIT LAYANAN PELANGGAN DOBO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(935, 46, '', 'UNIT LAYANAN PELANGGAN TUAL KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(936, 46, '', 'UNIT LAYANAN PELANGGAN SAUMLAKI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(937, 46, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI MALUKU UTARA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(938, 46, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI MALUKU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(939, 46, '', 'UNIT PELAKSANA PEMBANGKITAN MALUKU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(940, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL NAMLEA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(941, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL MASOHI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(942, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KAIRATU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(943, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL HATIVE KECIL', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(944, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL POKA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(945, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KAYU MERAH', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(946, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TOBELO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(947, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL LANGGUR', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(948, 46, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA UAP TIDORE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(949, 47, '7601', 'PT PLN (PERSERO) UNIT INDUK WILAYAH PAPUA DAN PAPUA BARAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(950, 47, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI PAPUA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(951, 47, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI PAPUA BARAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(952, 47, '7616', 'UNIT PELAKSANA PELAYANAN PELANGGAN TIMIKA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(953, 47, '', 'UNIT LAYANAN PELANGGAN AGATS', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(954, 47, '', 'UNIT LAYANAN PELANGGAN TIMIKA JAYA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(955, 47, '', 'UNIT LAYANAN PELANGGAN TIMIKA KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(956, 47, '7614', 'UNIT PELAKSANA PELAYANAN PELANGGAN JAYAPURA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(957, 47, '', 'UNIT LAYANAN PELANGGAN JAYAPURA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(958, 47, '', 'UNIT LAYANAN PELANGGAN ABEPURA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(959, 47, '', 'UNIT LAYANAN PELANGGAN SENTANI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(960, 47, '', 'UNIT LAYANAN PELANGGAN ARSO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(961, 47, '', 'UNIT LAYANAN PELANGGAN GENYEM', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(962, 47, '', 'UNIT LAYANAN PELANGGAN SARMI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(963, 47, '', 'UNIT LAYANAN PELANGGAN WAMENA', '2018-11-07 01:42:13', '2018-11-07 01:42:13');
INSERT INTO `ma_area` (`id`, `id_ma_unit`, `business_area`, `nama`, `created_at`, `updated_at`) VALUES
(964, 47, '7612', 'UNIT PELAKSANA PELAYANAN PELANGGAN SORONG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(965, 47, '', 'UNIT LAYANAN PELANGGAN SORONG KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(966, 47, '', 'UNIT LAYANAN PELANGGAN WAISAI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(967, 47, '', 'UNIT LAYANAN PELANGGAN FAKFAK', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(968, 47, '', 'UNIT LAYANAN PELANGGAN KAIMANA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(969, 47, '', 'UNIT LAYANAN PELANGGAN AIMAS', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(970, 47, '', 'UNIT LAYANAN PELANGGAN TEMINABUAN', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(971, 47, '7613', 'UNIT PELAKSANA PELAYANAN PELANGGAN BIAK', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(972, 47, '', 'UNIT LAYANAN PELANGGAN BIAK KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(973, 47, '', 'UNIT LAYANAN PELANGGAN WAROPEN', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(974, 47, '', 'UNIT LAYANAN PELANGGAN YOMDORI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(975, 47, '', 'UNIT LAYANAN PELANGGAN SERUI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(976, 47, '7615', 'UNIT PELAKSANA PELAYANAN PELANGGAN MANOKWARI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(977, 47, '', 'UNIT LAYANAN PELANGGAN MANOKWARI KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(978, 47, '', 'UNIT LAYANAN PELANGGAN WASIOR', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(979, 47, '', 'UNIT LAYANAN PELANGGAN PRAFI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(980, 47, '', 'UNIT LAYANAN PELANGGAN NABIRE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(981, 47, '', 'UNIT LAYANAN PELANGGAN BINTUNI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(982, 47, '7611', 'UNIT PELAKSANA PELAYANAN PELANGGAN MERAUKE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(983, 47, '', 'UNIT LAYANAN PELANGGAN MERAUKE KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(984, 47, '', 'UNIT LAYANAN PELANGGAN KEPI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(985, 47, '', 'UNIT LAYANAN PELANGGAN KUPRIK', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(986, 47, '', 'UNIT LAYANAN PELANGGAN KURIK', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(987, 47, '', 'UNIT LAYANAN PELANGGAN TANAH MERAH', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(988, 47, '', 'UNIT PELAKSANA PEMBANGKITAN PAPUA DAN PAPUA BARAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(989, 47, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK JAYAPURA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(990, 47, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR ORYA GENYEM', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(991, 47, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL JAYAPURA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(992, 47, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SORONG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(993, 47, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BIAK', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(994, 47, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL MANOKWARI', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(995, 47, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL MERAUKE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(996, 48, '7701', 'PT PLN (PERSERO) UNIT INDUK WILAYAH NUSA TENGGARA BARAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(997, 48, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI NUSA TENGGARA BARAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(998, 48, '7731', 'UNIT PELAKSANA PEMBANGKITAN LOMBOK', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(999, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA MIKRO HIDRO/SURYA TANJUNG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1000, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL AMPENAN', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1001, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL PAOKMOTONG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1002, 48, '7732', 'UNIT PELAKSANA PEMBANGKITAN TAMBORA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1003, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TALIWANG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1004, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA UAP SUMBAWA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1005, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BIMA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1006, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL LABUAN', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1007, 48, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL DOMPU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1008, 48, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK INDUK BIMA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1009, 48, '7741', 'UNIT PELAKSANA PENYALURAN DAN PENGATUR BEBAN MATARAM', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1010, 48, '7711', 'UNIT PELAKSANA PELAYANAN PELANGGAN MATARAM', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1011, 48, '', 'UNIT LAYANAN PELANGGAN AMPENAN', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1012, 48, '', 'UNIT LAYANAN PELANGGAN CAKRANEGARA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1013, 48, '', 'UNIT LAYANAN PELANGGAN PRAYA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1014, 48, '', 'UNIT LAYANAN PELANGGAN PRINGGABAYA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1015, 48, '', 'UNIT LAYANAN PELANGGAN SELONG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1016, 48, '', 'UNIT LAYANAN PELANGGAN TANJUNG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1017, 48, '7713', 'UNIT PELAKSANA PELAYANAN PELANGGAN BIMA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1018, 48, '', 'UNIT LAYANAN PELANGGAN DOMPU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1019, 48, '', 'UNIT LAYANAN PELANGGAN SAPE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1020, 48, '', 'UNIT LAYANAN PELANGGAN WOHA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1021, 48, '', 'UNIT LAYANAN PELANGGAN BIMA KOTA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1022, 48, '7712', 'UNIT PELAKSANA PELAYANAN PELANGGAN SUMBAWA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1023, 48, '', 'UNIT LAYANAN PELANGGAN ALAS', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1024, 48, '', 'UNIT LAYANAN PELANGGAN TALIWANG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1025, 48, '', 'UNIT LAYANAN PELANGGAN EMPANG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1026, 48, '', 'UNIT LAYANAN PELANGGAN SAMAWA REA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1027, 49, '7801', 'PT PLN (PERSERO) UNIT INDUK WILAYAH NUSA TENGGARA TIMUR', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1028, 49, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN KUPANG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1029, 49, '7812', 'UNIT PELAKSANA PELAYANAN PELANGGAN FLORES BAGIAN BARAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1030, 49, '', 'UNIT LAYANAN PELANGGAN BAJAWA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1031, 49, '', 'UNIT LAYANAN PELANGGAN LABUAN BAJO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1032, 49, '', 'UNIT LAYANAN PELANGGAN RUTENG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1033, 49, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL ENDE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1034, 49, '7814', 'UNIT PELAKSANA PELAYANAN PELANGGAN FLORES BAGIAN TIMUR', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1035, 49, '', 'UNIT LAYANAN PELANGGAN ADONARA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1036, 49, '', 'UNIT LAYANAN PELANGGAN LARANTUKA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1037, 49, '', 'UNIT LAYANAN PELANGGAN LEMBATA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1038, 49, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL MAUMERE', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1039, 49, '7813', 'UNIT PELAKSANA PELAYANAN PELANGGAN SUMBA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1040, 49, '', 'UNIT LAYANAN PELANGGAN SUMBA BARAT', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1041, 49, '', 'UNIT LAYANAN PELANGGAN SUMBA TIMUR', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1042, 49, '', 'UNIT LAYANAN PELANGGAN SUMBA JAYA', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1043, 49, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL WAINGAPU', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1044, 49, '7811', 'UNIT PELAKSANA PELAYANAN PELANGGAN KUPANG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1045, 49, '', 'UNIT LAYANAN PELANGGAN KUPANG', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1046, 49, '', 'UNIT LAYANAN PELANGGAN OESAO', '2018-11-07 01:42:13', '2018-11-07 01:42:13'),
(1047, 49, '', 'UNIT LAYANAN PELANGGAN SO\"E', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1048, 49, '', 'UNIT LAYANAN PELANGGAN KEFAMENANU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1049, 49, '', 'UNIT LAYANAN PELANGGAN ATAMBUA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1050, 49, '', 'UNIT LAYANAN PELANGGAN KALABAHI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1051, 49, '', 'UNIT LAYANAN PELANGGAN ROTE NDAO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1052, 49, '7831', 'UNIT PELAKSANA PEMBANGKITAN NUSA TENGGARA TIMUR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1053, 49, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KUPANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1054, 49, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL ATAMBUA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1055, 49, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA PANAS BUMI MATALOKO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1056, 49, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KUPANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1057, 49, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK FLORES', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1058, 50, '6301', 'PT PLN (PERSERO) UNIT INDUK WILAYAH SUMATERA BARAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1059, 50, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUMATERA BARAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1060, 50, '', 'UNIT PELAKSANA PENGATUR DISTRIBUSI SUMATERA BARAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1061, 50, '6356', 'UNIT PELAKSANA PELAYANAN PELANGGAN PADANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1062, 50, '', 'UNIT LAYANAN PELANGGAN TABING', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1063, 50, '', 'UNIT LAYANAN PELANGGAN BELANTI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1064, 50, '', 'UNIT LAYANAN PELANGGAN INDARUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1065, 50, '', 'UNIT LAYANAN PELANGGAN TUA PEJAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1066, 50, '', 'UNIT LAYANAN PELANGGAN PAINAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1067, 50, '', 'UNIT LAYANAN PELANGGAN BALAI SELASA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1068, 50, '', 'UNIT LAYANAN PELANGGAN LUBUK ALUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1069, 50, '', 'UNIT LAYANAN PELANGGAN SICINCIN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1070, 50, '', 'UNIT LAYANAN PELANGGAN PARIAMAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1071, 50, '', 'UNIT LAYANAN PELANGGAN SUNGAI PENUH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1072, 50, '', 'UNIT LAYANAN PELANGGAN KERSIK TUO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1073, 50, '', 'UNIT LAYANAN PELANGGAN KURANJI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1074, 50, '6311', 'UNIT PELAKSANA PELAYANAN PELANGGAN BUKITTINGGI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1075, 50, '', 'UNIT LAYANAN PELANGGAN BUKITTINGGI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1076, 50, '', 'UNIT LAYANAN PELANGGAN BASO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1077, 50, '', 'UNIT LAYANAN PELANGGAN PADANG PANJANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1078, 50, '', 'UNIT LAYANAN PELANGGAN LUBUK BASUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1079, 50, '', 'UNIT LAYANAN PELANGGAN LUBUK SIKAPING', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1080, 50, '', 'UNIT LAYANAN PELANGGAN SIMPANG EMPAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1081, 50, '', 'UNIT LAYANAN PELANGGAN KOTO TUO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1082, 50, '6313', 'UNIT PELAKSANA PELAYANAN PELANGGAN SOLOK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1083, 50, '', 'UNIT LAYANAN PELANGGAN SOLOK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1084, 50, '', 'UNIT LAYANAN PELANGGAN SINGKARAK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1085, 50, '', 'UNIT LAYANAN PELANGGAN SILUNGKANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1086, 50, '', 'UNIT LAYANAN PELANGGAN SAWAH LUNTO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1087, 50, '', 'UNIT LAYANAN PELANGGAN SIJUNJUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1088, 50, '', 'UNIT LAYANAN PELANGGAN SITIUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1089, 50, '', 'UNIT LAYANAN PELANGGAN SUNGAI RUMBAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1090, 50, '', 'UNIT LAYANAN PELANGGAN KAYU ARO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1091, 50, '', 'UNIT LAYANAN PELANGGAN MUARA LABUH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1092, 50, '6314', 'UNIT PELAKSANA PELAYANAN PELANGGAN PAYAKUMBUH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1093, 50, '', 'UNIT LAYANAN PELANGGAN PAYAKUMBUH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1094, 50, '', 'UNIT LAYANAN PELANGGAN BATUSANGKAR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1095, 50, '', 'UNIT LAYANAN PELANGGAN LIMA PUnit Layanan UH KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1096, 50, '', 'UNIT LAYANAN PELANGGAN LINTAU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1097, 51, '6401', 'PT PLN (PERSERO) UNIT INDUK WILAYAH RIAU DAN KEPUnit Layanan AUAN RIAU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1098, 51, '6456', 'UNIT PELAKSANA PENGATUR DISTRIBUSI RIAU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1099, 51, '6411', 'UNIT PELAKSANA PELAYANAN PELANGGAN PEKANBARU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1100, 51, '', 'UNIT LAYANAN PELANGGAN KAMPAR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1101, 51, '', 'UNIT LAYANAN PELANGGAN LIPAT KAIN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1102, 51, '', 'UNIT LAYANAN PELANGGAN UJUNG BATU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1103, 51, '', 'UNIT LAYANAN PELANGGAN BANGKINANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1104, 51, '', 'UNIT LAYANAN PELANGGAN PANAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1105, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN KERINCI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1106, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN PASIR PANGARAIAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1107, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN PEKANBARU KOTA BARAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1108, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN PEKANBARU KOTA TIMUR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1109, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN PERAWANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1110, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN RUMBAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1111, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN SIAK SRI INDRAPURA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1112, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN SIMPANG TIGA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1113, 51, '6412', 'UNIT PELAKSANA PELAYANAN PELANGGAN DUMAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1114, 51, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL BAGAN BESAR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1115, 51, '', 'UNIT LAYANAN PELANGGAN BAGAN BATU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1116, 51, '', 'UNIT LAYANAN PELANGGAN BAGAN SIAPI API', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1117, 51, '', 'UNIT LAYANAN PELANGGAN BENGKALIS', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1118, 51, '', 'UNIT LAYANAN PELANGGAN DUMAI KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1119, 51, '', 'UNIT LAYANAN PELANGGAN DURI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1120, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN SELAT PANJANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1121, 51, '6413', 'UNIT PELAKSANA PELAYANAN PELANGGAN TANJUNG PINANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1122, 51, '', 'UNIT LAYANAN PELANGGAN BELAKANG PADANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1123, 51, '', 'UNIT LAYANAN PELANGGAN BINTAN CENTER', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1124, 51, '', 'UNIT LAYANAN PELANGGAN DABO SINGKEP', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1125, 51, '', 'UNIT LAYANAN PELANGGAN KIJANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1126, 51, '', 'UNIT LAYANAN PELANGGAN ANAMBAS', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1127, 51, '', 'UNIT LAYANAN PELANGGAN NATUNA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1128, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN TANJUNG BALAI KARIMUN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1129, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN TANJUNG BATU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1130, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN TANJUNG PINANG KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1131, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN TANJUNG UBAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1132, 51, '6414', 'UNIT PELAKSANA PELAYANAN PELANGGAN RENGAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1133, 51, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL RENGAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1134, 51, '', 'UNIT LAYANAN PELANGGAN AIR MOLEK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1135, 51, '', 'UNIT LAYANAN PELANGGAN KUALA ENOK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1136, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN TALUK KUANTAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1137, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN TEMBILAHAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1138, 51, '', 'UNIT LAYANAN PELANGGAN PANGKALAN RENGAT KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1139, 51, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI RIAU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1140, 51, '', 'UNIT PELAKSANA PROYEK KETENEGALISTRIKAN PROVINSI KEPUnit Layanan AUAN RIAU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1141, 51, '', 'UNIT PELAKSANA PEMBANGKITAN KEPUnit Layanan AUAN RIAU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1142, 51, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TANJUNG PINANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1143, 51, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA UAP TANJUNG BALAI KARIMUN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1144, 51, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK INDUK BINTAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1145, 52, '6601', 'PT PLN (PERSERO) UNIT INDUK WILAYAH BANGKA BELITUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1146, 52, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI BANGKA BELITUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1147, 52, '6612', 'UNIT PELAKSANA PELAYANAN PELANGGAN BELITUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1148, 52, '', 'UNIT LAYANAN PELANGGAN MANGGAR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1149, 52, '', 'UNIT LAYANAN PELANGGAN TANJUNG PANDAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1150, 52, '6611', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANGKA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1151, 52, '', 'UNIT LAYANAN PELANGGAN PANGKALPINANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1152, 52, '', 'UNIT LAYANAN PELANGGAN SUNGAILIAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1153, 52, '', 'UNIT LAYANAN PELANGGAN KOBA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1154, 52, '', 'UNIT LAYANAN PELANGGAN TOBOALI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1155, 52, '', 'UNIT LAYANAN PELANGGAN MENTOK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1156, 52, '6613', 'UNIT PELAKSANA PEMBANGKITAN BANGKA BELITUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1157, 52, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL MERAWANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1158, 52, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL PILANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1159, 53, '6501', 'PT PLN (PERSERO) UNIT INDUK WILAYAH SUMATERA SELATAN, JAMBI DAN BENGKUnit Layanan U', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1160, 53, '6514', 'UNIT PELAKSANA PELAYANAN PELANGGAN PALEMBANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1161, 53, '', 'UNIT LAYANAN PELANGGAN RIVAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1162, 53, '', 'UNIT LAYANAN PELANGGAN AMPERA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1163, 53, '', 'UNIT LAYANAN PELANGGAN SUKARAMI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1164, 53, '', 'UNIT LAYANAN PELANGGAN KENTEN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1165, 53, '', 'UNIT LAYANAN PELANGGAN KAYU AGUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1166, 53, '', 'UNIT LAYANAN PELANGGAN SEKAYU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1167, 53, '', 'UNIT LAYANAN PELANGGAN MARIANA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1168, 53, '', 'UNIT LAYANAN PELANGGAN PANGKALAN BALAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1169, 53, '', 'UNIT LAYANAN PELANGGAN INDERALAYA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1170, 53, '', 'UNIT LAYANAN PELANGGAN TUGU MUnit Layanan YO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1171, 53, '6512', 'UNIT PELAKSANA PELAYANAN PELANGGAN BENGKUnit Layanan U', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1172, 53, '', 'UNIT LAYANAN PELANGGAN NUSA INDAH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1173, 53, '', 'UNIT LAYANAN PELANGGAN TELUK SEGARA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1174, 53, '', 'UNIT LAYANAN PELANGGAN CURUP', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1175, 53, '', 'UNIT LAYANAN PELANGGAN KEPAHIANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1176, 53, '', 'UNIT LAYANAN PELANGGAN MUARA AMAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1177, 53, '', 'UNIT LAYANAN PELANGGAN TAIS', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1178, 53, '', 'UNIT LAYANAN PELANGGAN ARGA MAKMUR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1179, 53, '', 'UNIT LAYANAN PELANGGAN MANNA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1180, 53, '', 'UNIT LAYANAN PELANGGAN BINTUHAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1181, 53, '', 'UNIT LAYANAN PELANGGAN MUKO MUKO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1182, 53, '6513', 'UNIT PELAKSANA PELAYANAN PELANGGAN JAMBI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1183, 53, '', 'UNIT LAYANAN PELANGGAN KOTA BARU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1184, 53, '', 'UNIT LAYANAN PELANGGAN SEBERANG KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1185, 53, '', 'UNIT LAYANAN PELANGGAN TELANAI PURA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1186, 53, '', 'UNIT LAYANAN PELANGGAN KUALA TUNGKAL', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1187, 53, '', 'UNIT LAYANAN PELANGGAN MUARA BUnit Layanan IAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1188, 53, '', 'UNIT LAYANAN PELANGGAN MUARA SABAK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1189, 53, '6511', 'UNIT PELAKSANA PELAYANAN PELANGGAN LAHAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1190, 53, '', 'UNIT LAYANAN PELANGGAN LEMBAYUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1191, 53, '', 'UNIT LAYANAN PELANGGAN LUBUK LINGGAU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1192, 53, '', 'UNIT LAYANAN PELANGGAN PAGAR ALAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1193, 53, '', 'UNIT LAYANAN PELANGGAN MUARA ENIM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1194, 53, '', 'UNIT LAYANAN PELANGGAN PRABUMUnit Layanan IH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1195, 53, '', 'UNIT LAYANAN PELANGGAN BATURAJA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1196, 53, '', 'UNIT LAYANAN PELANGGAN MARTAPURA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1197, 53, '', 'UNIT LAYANAN PELANGGAN MUARADUA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1198, 53, '', 'UNIT LAYANAN PELANGGAN MUARA BELITI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1199, 53, '', 'UNIT LAYANAN PELANGGAN PENDOPO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1200, 53, '', 'UNIT LAYANAN PELANGGAN TEBING TINGGI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1201, 53, '6515', 'UNIT PELAKSANA PELAYANAN PELANGGAN MUARA BUNGO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1202, 53, '', 'UNIT LAYANAN PELANGGAN SAROLANGUN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1203, 53, '', 'UNIT LAYANAN PELANGGAN BANGKO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1204, 53, '', 'UNIT LAYANAN PELANGGAN MUARA TEBO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1205, 53, '', 'UNIT LAYANAN PELANGGAN RIMBO BUJANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1206, 53, '', 'UNIT LAYANAN PELANGGAN KOTA BUNGO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1207, 53, '6556', 'UNIT PELAKSANA PENGATUR DISTRIBUSI SUMATERA SELATAN, JAMBI, DAN BENGKUnit Layanan U', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1208, 53, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUMATERA SELATAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1209, 53, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI JAMBI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1210, 53, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI BENGKUnit Layanan U', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1211, 54, '6101', 'PT PLN (PERSERO) UNIT INDUK WILAYAH ACEH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1212, 54, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI NAD', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1213, 54, '', 'UNIT PELAKSANA PENGATUR DISTRIBUSI ACEH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1214, 54, '6112', 'UNIT PELAKSANA PELAYANAN PELANGGAN BANDA ACEH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1215, 54, '', 'UNIT LAYANAN PELANGGAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1216, 54, '', 'UNIT LAYANAN PELANGGAN JANTHO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1217, 54, '', 'UNIT LAYANAN PELANGGAN KEUDE BIENG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1218, 54, '', 'UNIT LAYANAN PELANGGAN LAMBARO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1219, 54, '', 'UNIT LAYANAN PELANGGAN MERDUATI KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1220, 54, '', 'UNIT LAYANAN PELANGGAN SABANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1221, 54, '6111', 'UNIT PELAKSANA PELAYANAN PELANGGAN LANGSA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1222, 54, '', 'UNIT LAYANAN PELANGGAN BLANG KEUJEREN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1223, 54, '', 'UNIT LAYANAN PELANGGAN IDI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1224, 54, '', 'UNIT LAYANAN PELANGGAN KUALA SIMPANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1225, 54, '', 'UNIT LAYANAN PELANGGAN KUTACANE', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1226, 54, '', 'UNIT LAYANAN PELANGGAN PEUREUnit Layanan AK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1227, 54, '', 'UNIT LAYANAN PELANGGAN LANGSA KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1228, 54, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL KUNING', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1229, 54, '6113', 'UNIT PELAKSANA PELAYANAN PELANGGAN LHOKSEUMAWE', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1230, 54, '', 'UNIT LAYANAN PELANGGAN LHOKSEUMAWE KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1231, 54, '', 'UNIT LAYANAN PELANGGAN BIREUEN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1232, 54, '', 'UNIT LAYANAN PELANGGAN TAKENGON', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1233, 54, '', 'UNIT LAYANAN PELANGGAN JANARATA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1234, 54, '', 'UNIT LAYANAN PELANGGAN SAMALANGA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1235, 54, '', 'UNIT LAYANAN PELANGGAN LHOKSUKON', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1236, 54, '', 'UNIT LAYANAN PELANGGAN PANTON LABU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1237, 54, '', 'UNIT LAYANAN PELANGGAN KRUENG GEUKUEH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1238, 54, '', 'UNIT LAYANAN PELANGGAN GANDAPURA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1239, 54, '', 'UNIT LAYANAN PELANGGAN GEUDONG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1240, 54, '', 'UNIT LAYANAN PELANGGAN MATANG GEUnit Layanan UMPANG DUA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1241, 54, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL AYANGAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1242, 54, '6114', 'UNIT PELAKSANA PELAYANAN PELANGGAN MEUnit Layanan ABOH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1243, 54, '', 'UNIT LAYANAN PELANGGAN CALANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1244, 54, '', 'UNIT LAYANAN PELANGGAN JEURAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1245, 54, '', 'UNIT LAYANAN PELANGGAN MEUnit Layanan ABOH KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1246, 54, '', 'UNIT LAYANAN PELANGGAN SINABANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1247, 54, '', 'UNIT LAYANAN PELANGGAN TEUNOM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1248, 54, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SEUNEBOK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1249, 54, '6116', 'UNIT PELAKSANA PELAYANAN PELANGGAN SIGLI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1250, 54, '', 'UNIT LAYANAN PELANGGAN SIGLI KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1251, 54, '', 'UNIT LAYANAN PELANGGAN BEUREUNUN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1252, 54, '', 'UNIT LAYANAN PELANGGAN MEUREUDU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1253, 54, '6115', 'UNIT PELAKSANA PELAYANAN PELANGGAN SUBUnit Layanan USSALAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1254, 54, '', 'UNIT LAYANAN PELANGGAN SUBUnit Layanan USSALAM KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1255, 54, '', 'UNIT LAYANAN PELANGGAN SINGKIL', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1256, 54, '', 'UNIT LAYANAN PELANGGAN RIMO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1257, 54, '', 'UNIT LAYANAN PELANGGAN KOTA FAJAR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1258, 54, '', 'UNIT LAYANAN PELANGGAN TAPAK TUAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1259, 54, '', 'UNIT LAYANAN PELANGGAN LABUHAN HAJI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1260, 54, '', 'UNIT LAYANAN PELANGGAN BLANG PIDIE', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1261, 54, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL SUAK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1262, 55, '6201', 'PT PLN (PERSERO) UNIT INDUK WILAYAH SUMATERA UTARA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1263, 55, '', 'UNIT PELAKSANA PROYEK KETENAGALISTRIKAN PROVINSI SUMATERA UTARA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1264, 55, '', 'UNIT PELAKSANA PENGATUR DISTRIBUSI SUMATERA UTARA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1265, 55, '6214', 'UNIT PELAKSANA PELAYANAN PELANGGAN MEDAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1266, 55, '', 'UNIT LAYANAN PELANGGAN BELAWAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1267, 55, '', 'UNIT LAYANAN PELANGGAN HELVETIA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1268, 55, '', 'UNIT LAYANAN PELANGGAN JOHOR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1269, 55, '', 'UNIT LAYANAN PELANGGAN LABUHAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1270, 55, '', 'UNIT LAYANAN PELANGGAN MEDAN BARU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1271, 55, '', 'UNIT LAYANAN PELANGGAN MEDAN KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1272, 55, '', 'UNIT LAYANAN PELANGGAN MEDAN SELATAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1273, 55, '', 'UNIT LAYANAN PELANGGAN MEDAN TIMUR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1274, 55, '', 'UNIT LAYANAN PELANGGAN SUNGGAL', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1275, 55, '6213', 'UNIT PELAKSANA PELAYANAN PELANGGAN BINJAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1276, 55, '', 'UNIT LAYANAN PELANGGAN BINJAI BARAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1277, 55, '', 'UNIT LAYANAN PELANGGAN BINJAI KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1278, 55, '', 'UNIT LAYANAN PELANGGAN BINJAI TIMUR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1279, 55, '', 'UNIT LAYANAN PELANGGAN BRASTAGI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1280, 55, '', 'UNIT LAYANAN PELANGGAN GEBANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1281, 55, '', 'UNIT LAYANAN PELANGGAN KABAN JAHE', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1282, 55, '', 'UNIT LAYANAN PELANGGAN KUALA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1283, 55, '', 'UNIT LAYANAN PELANGGAN PANGKALAN BRANDAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1284, 55, '', 'UNIT LAYANAN PELANGGAN PANGKALAN SUSU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1285, 55, '', 'UNIT LAYANAN PELANGGAN SIDIKALANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1286, 55, '', 'UNIT LAYANAN PELANGGAN STABAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1287, 55, '', 'UNIT LAYANAN PELANGGAN TANJUNG PURA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1288, 55, '', 'UNIT LAYANAN PELANGGAN TIGA BINANGA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1289, 55, '6217', 'UNIT PELAKSANA PELAYANAN PELANGGAN LUBUK PAKAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1290, 55, '', 'UNIT LAYANAN PELANGGAN DELITUA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1291, 55, '', 'UNIT LAYANAN PELANGGAN GALANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1292, 55, '', 'UNIT LAYANAN PELANGGAN LUBUK PAKAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1293, 55, '', 'UNIT LAYANAN PELANGGAN MEDAN DENAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1294, 55, '', 'UNIT LAYANAN PELANGGAN PANCUR BATU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1295, 55, '', 'UNIT LAYANAN PELANGGAN PERBAUNGAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1296, 55, '', 'UNIT LAYANAN PELANGGAN SEI REMPAH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1297, 55, '', 'UNIT LAYANAN PELANGGAN TANJUNG MORAWA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1298, 55, '6211', 'UNIT PELAKSANA PELAYANAN PELANGGAN PEMATANG SIANTAR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1299, 55, '', 'UNIT LAYANAN PELANGGAN LIMA PUnit Layanan UH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1300, 55, '', 'UNIT LAYANAN PELANGGAN DOLOK MASIHUnit Layanan ', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1301, 55, '', 'UNIT LAYANAN PELANGGAN INDRA PURA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1302, 55, '', 'UNIT LAYANAN PELANGGAN KISARAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1303, 55, '', 'UNIT LAYANAN PELANGGAN PANGURURAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1304, 55, '', 'UNIT LAYANAN PELANGGAN PARAPAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1305, 55, '', 'UNIT LAYANAN PELANGGAN PERDAGANGAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1306, 55, '', 'UNIT LAYANAN PELANGGAN SIANTAR KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1307, 55, '', 'UNIT LAYANAN PELANGGAN SIDAMANIK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1308, 55, '', 'UNIT LAYANAN PELANGGAN TANAH JAWA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1309, 55, '', 'UNIT LAYANAN PELANGGAN TANJUNG TIRAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1310, 55, '', 'UNIT LAYANAN PELANGGAN TEBING TINGGI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1311, 55, '6216', 'UNIT PELAKSANA PELAYANAN PELANGGAN RANTAU PRAPAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1312, 55, '', 'UNIT LAYANAN PELANGGAN SIMPANG KAWAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1313, 55, '', 'UNIT LAYANAN PELANGGAN AEK KANOPAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1314, 55, '', 'UNIT LAYANAN PELANGGAN AEK KOTABATU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1315, 55, '', 'UNIT LAYANAN PELANGGAN AEK NABARA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1316, 55, '', 'UNIT LAYANAN PELANGGAN KOTA PINANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1317, 55, '', 'UNIT LAYANAN PELANGGAN LABUHAN BILIK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1318, 55, '', 'UNIT LAYANAN PELANGGAN RANTAU KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1319, 55, '', 'UNIT LAYANAN PELANGGAN TANJUNG BALAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1320, 55, '6212', 'UNIT PELAKSANA PELAYANAN PELANGGAN SIBOLGA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1321, 55, '', 'UNIT LAYANAN PELANGGAN BALIGE', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1322, 55, '', 'UNIT LAYANAN PELANGGAN BARUS', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1323, 55, '', 'UNIT LAYANAN PELANGGAN DOLOK SANGGUnit Layanan ', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1324, 55, '', 'UNIT LAYANAN PELANGGAN PORSEA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1325, 55, '', 'UNIT LAYANAN PELANGGAN SIBOLGA KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1326, 55, '', 'UNIT LAYANAN PELANGGAN SIBORONG BORONG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1327, 55, '', 'UNIT LAYANAN PELANGGAN TARUTUNG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1328, 55, '6215', 'UNIT PELAKSANA PELAYANAN PELANGGAN PADANG SIDEMPUAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1329, 55, '', 'UNIT LAYANAN PELANGGAN GUNUNG TUA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1330, 55, '', 'UNIT LAYANAN PELANGGAN KOTA NOPAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1331, 55, '', 'UNIT LAYANAN PELANGGAN NATAL', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1332, 55, '', 'UNIT LAYANAN PELANGGAN PENYABUNGAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1333, 55, '', 'UNIT LAYANAN PELANGGAN SIBUHUAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1334, 55, '', 'UNIT LAYANAN PELANGGAN SIDEMPUAN KOTA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1335, 55, '', 'UNIT LAYANAN PELANGGAN SIPIROK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1336, 55, '6218', 'UNIT PELAKSANA PELAYANAN PELANGGAN NIAS', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1337, 55, '', 'UNIT LAYANAN PELANGGAN NIAS BARAT', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1338, 55, '', 'UNIT LAYANAN PELANGGAN GUNUNG SITOLI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1339, 55, '', 'UNIT LAYANAN PELANGGAN TELUK DALAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1340, 55, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL GUNUNG SITOLI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1341, 55, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TELUK DALAM', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1342, 56, '8201', 'PT PLN (PERSERO) PUSAT PENDIDIKAN DAN PELATIHAN (CORPORATE UNIVERSITY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1343, 56, '', 'UNIT PELAKSANA SERTIFIKASI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1344, 56, '', 'UNIT PELAKSANA ASSESSMENT CENTRE', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1345, 56, '8219', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN BANJARBARU (LEARNING UNIT)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1346, 56, '8218', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN PADANG (LEARNING UNIT)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1347, 56, '8215', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN TUNTUNGAN (LEARNING UNIT)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1348, 56, '8220', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN PALEMBANG (ACADEMY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1349, 56, '8217', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN SURALAYA (ACADEMY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1350, 56, '8216', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN MAKASSAR (ACADEMY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1351, 56, '8214', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN PANDAAN (ACADEMY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1352, 56, '8213', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN SEMARANG (ACADEMY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1353, 56, '8212', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN JAKARTA (ACADEMY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1354, 56, '8211', 'UNIT PELAKSANA PENDIDIKAN DAN PELATIHAN BOGOR (ACADEMY)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1355, 56, '8223', 'UNIT PELAKSANA MUSEUM LISTRIK DAN ENERGI BARU', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1356, 57, '8601', 'PT PLN (PERSERO) PUSAT SERTIFIKASI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1357, 58, '8501', 'PT PLN (PERSERO) PUSAT MANAJEMEN PROYEK', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1358, 58, '', 'UNIT PELAKSANA MANAJEMEN KONTRUKSI 1', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1359, 58, '', 'UNIT PELAKSANA MANAJEMEN KONTRUKSI 2', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1360, 58, '', 'UNIT PELAKSANA MANAJEMEN KONTRUKSI 3', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1361, 58, '', 'UNIT PELAKSANA MANAJEMEN KONTRUKSI 4', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1362, 58, '', 'UNIT PELAKSANA MANAJEMEN KONTRUKSI 5', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1363, 59, '8301', 'PT PLN (PERSERO) PUSAT ENJINIRING KETENAGALISTRIKAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1364, 60, '8101', 'PT PLN (PERSERO) PUSAT PENELITIAN DAN PENGEMBANGAN (RESEARCH INSTITUTE)', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1365, 61, '8401', 'PT PLN (PERSERO) PUSAT PEMELIHARAAN KETENAGALISTRIKAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1366, 61, '8411', 'UNIT PELAKSANA PRODUKSI DAN WORKSHOP I', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1367, 61, '8412', 'UNIT PELAKSANA PRODUKSI DAN WORKSHOP II', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1368, 61, '8413', 'UNIT PELAKSANA PRODUKSI DAN WORKSHOP III', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1369, 61, '8414', 'UNIT PELAKSANA PRODUKSI DAN WORKSHOP IV', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1370, 61, '8415', 'UNIT PELAKSANA PRODUKSI DAN WORKSHOP V', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1371, 61, '8416', 'UNIT PELAKSANA PRODUKSI DAN WORKSHOP VI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1372, 62, '3201', 'PT PLN (PERSERO) UNIT INDUK PENYALURAN DAN PUSAT PENGATUR BEBAN SUMATERA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1373, 62, '3222', 'UNIT PELAKSANA TRANSMISI JAMBI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1374, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK AUR DURI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1375, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK MUARA BUNGO', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1376, 62, '3216', 'UNIT PELAKSANA TRANSMISI BANDA ACEH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1377, 62, '', 'UNIT LAYANAN TRANSMISI GARDU INDUK MEUnit Layanan ABOH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1378, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BANDA ACEH', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1379, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK LANGSA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1380, 62, '3212', 'UNIT PELAKSANA TRANSMISI MEDAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1381, 62, '', 'UNIT LAYANAN TRANSMISI GARDU INDUK NIAS', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1382, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK GLUGUR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1383, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PAYA PASIR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1384, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SEI ROTAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1385, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BINJAI', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1386, 62, '3213', 'UNIT PELAKSANA TRANSMISI PEMATANG SIANTAR', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1387, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK DOLOK SANGGUnit Layanan ', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1388, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KISARAN', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1389, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SIDIKALANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1390, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK SIBOLGA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1391, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TOBA', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1392, 62, '3211', 'UNIT PELAKSANA TRANSMISI PADANG', '2018-11-07 01:47:36', '2018-11-07 01:47:36'),
(1393, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PAYAKUMBUH', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1394, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KILIRANJAO', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1395, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PADANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1396, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PARIAMAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1397, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BUKIT TINGGI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1398, 62, '3214', 'UNIT PELAKSANA TRANSMISI PALEMBANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1399, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KERAMASAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1400, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BOOM BARU', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1401, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BORANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1402, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PRABUMUnit Layanan IH', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1403, 62, '3215', 'UNIT PELAKSANA TRANSMISI TANJUNG KARANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1404, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PAGELARAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1405, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TARAHAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1406, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TEGINENENG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1407, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK KOTA BUMI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1408, 62, '3217', 'UNIT PELAKSANA TRANSMISI PEKANBARU', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1409, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PASIR PUTIH', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1410, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK DURI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1411, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK TELUK LEMBU', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1412, 62, '3218', 'UNIT PELAKSANA TRANSMISI BENGKUnit Layanan U', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1413, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK BATURAJA', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1414, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK LAHAT', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1415, 62, '', 'UNIT LAYANAN TRANSMISI DAN GARDU INDUK PEKALONGAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1416, 62, '3219', 'UNIT PELAKSANA PENGATUR BEBAN SUMATERA BAGIAN UTARA', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1417, 62, '3220', 'UNIT PELAKSANA PENGATUR BEBAN SUMATERA BAGIAN TENGAH', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1418, 62, '3221', 'UNIT PELAKSANA PENGATUR BEBAN SUMATERA BAGIAN SELATAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1419, 63, '2201', 'PT PLN (PERSERO) UNIT INDUK PEMBANGKITAN TANJUNG JATI B', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1420, 64, '2101', 'PT PLN (PERSERO) UNIT INDUK PEMBANGKITAN SUMATERA BAGIAN SELATAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1421, 64, '2115', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN BANDAR LAMPUNG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1422, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TANJUNG KARANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1423, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA PANAS BUMI/AIR TANGGAMUS', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1424, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL TEGINENENG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1425, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR WAY BESAI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1426, 64, '2116', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN BENGKUnit Layanan U', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1427, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR TES', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1428, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR MUSI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1429, 64, '2113', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN KERAMASAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1430, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS/UAP INDERALAYA', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1431, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS/UAP KERAMASAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1432, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL/GAS MERAH MATA', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1433, 64, '2118', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN JAMBI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1434, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL/GAS PAYO SELINCAH', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1435, 64, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA MESIN GAS SUNGAI GELAM', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1436, 64, '2111', 'UNIT PELAKSANA PEMBANGKITAN BUKITTINGGI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1437, 64, '2112', 'UNIT PELAKSANA PEMBANGKITAN OMBILIN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1438, 64, '2114', 'UNIT PELAKSANA PEMBANGKITAN BUKIT ASAM', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1439, 64, '2117', 'UNIT PELAKSANA PEMBANGKITAN TARAHAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37');
INSERT INTO `ma_area` (`id`, `id_ma_unit`, `business_area`, `nama`, `created_at`, `updated_at`) VALUES
(1440, 64, '2119', 'UNIT PELAKSANA PEMBANGKITAN TELUK SIRIH', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1441, 64, '2120', 'UNIT PELAKSANA PEMBANGKITAN SEBALANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1442, 65, '2001', 'PT PLN (PERSERO) UNIT INDUK PEMBANGKITAN SUMATERA BAGIAN UTARA', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1443, 65, '2017', 'UNIT PELAKSANA PEMBANGKITAN PANGKALAN SUSU', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1444, 65, '2012', 'UNIT PELAKSANA PEMBANGKITAN BELAWAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1445, 65, '', 'UNIT LAYANAN PEMBANGKIT LISTRIK TENAGA LISTRIK GLUGUR', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1446, 65, '', 'UNIT LAYANAN PEMBANGKIT LISTRIK TENAGA LISTRIK DIESEL TITI KUNING', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1447, 65, '', 'UNIT LAYANAN PEMBANGKIT LISTRIK TENAGA LISTRIK GAS PAYA PASIR', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1448, 65, '2013', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN PEKANBARU', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1449, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR KOTO PANJANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1450, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS TELUK LEMBU', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1451, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA GAS DURI', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1452, 65, '2014', 'UNIT PELAKSANA PENGENDALIAN PEMBANGKITAN PANDAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1453, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR SIPANSIHAPORAS', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1454, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA AIR RENUN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1455, 65, '2015', 'UNIT PELAKSANA PEMELIHARAAN PEMBANGKITAN MEDAN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1456, 65, '2016', 'UNIT PELAKSANA PEMBANGKITAN LABUHAN ANGIN', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1457, 65, '2011', 'UNIT PELAKSANA PEMBANGKITAN NAGAN RAYA', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1458, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL COT TRUENG', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1459, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL LUENG BATA', '2018-11-07 01:47:37', '2018-11-07 01:47:37'),
(1460, 65, '', 'UNIT LAYANAN PUSAT LISTRIK TENAGA DIESEL PUnit Layanan O PISANG', '2018-11-07 01:47:37', '2018-11-07 01:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `ma_lag`
--

CREATE TABLE `ma_lag` (
  `id` int(11) NOT NULL,
  `id_ma_unit` int(11) DEFAULT NULL,
  `id_ma_area` int(11) DEFAULT NULL,
  `id_ma_rayon` int(11) DEFAULT NULL,
  `id_ma_satuan` int(11) DEFAULT NULL,
  `is_positive` int(11) DEFAULT NULL,
  `id_ma_tipe_laporan` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_lag`
--

INSERT INTO `ma_lag` (`id`, `id_ma_unit`, `id_ma_area`, `id_ma_rayon`, `id_ma_satuan`, `is_positive`, `id_ma_tipe_laporan`, `nama`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(3, 6, NULL, NULL, 5, 1, 2, 'Meningkatkan Penjualan dan Pendapatan Tenaga Listrik 17,11 TWh menjadi 23,56 TWh pada 31 Desember 2018', '2018-09-01', '2018-12-31', '2018-10-23 18:38:39', '2018-10-25 18:54:11'),
(4, 6, NULL, NULL, 6, 1, 2, 'Meningkatkan Pendapatan dari Rp 18,8 Triliun menjadi Rp 26,289 Triliun pada 31 Desember 2018', '2018-09-01', '2018-12-31', '2018-10-23 18:40:27', '2018-10-25 18:54:12'),
(5, 6, NULL, NULL, 2, 2, 2, 'Menurunkan COP 32,06 hari menjadi 25 hari pada 31 Desember 2018', '2018-09-01', '2018-12-31', '2018-10-23 18:41:29', '2018-10-25 18:54:13'),
(6, 6, NULL, NULL, 7, 1, 2, 'Meningkatkan jumlah pelanggan layana khusus premium 141 pelanggan bundling dan 197 murni pada September 2018 menjadi 200 bundling dan 360 pelanggan murni pada 31 Desember 2018', '2018-09-01', '2018-12-31', '2018-10-23 18:59:46', '2018-10-25 18:54:14'),
(7, 6, NULL, NULL, 7, 2, 2, 'Menurunkan daftar tunggu untuk segment TR dari 23,262 menjadi 0 pada 31 Desember 2018', '2018-09-01', '2018-12-31', '2018-10-23 19:25:47', '2018-10-25 18:54:15'),
(30, 6, NULL, NULL, 2, 1, 1, 'dummy_1', '2018-10-01', '2018-10-31', '2018-10-25 02:09:38', '2018-10-25 18:54:16'),
(34, 6, NULL, NULL, 2, 1, 3, 'dummy_2', '2018-10-23', '2018-12-20', '2018-10-25 19:03:21', '2018-10-30 23:50:23');

-- --------------------------------------------------------

--
-- Table structure for table `ma_lead`
--

CREATE TABLE `ma_lead` (
  `id` int(11) NOT NULL,
  `id_ma_unit` int(11) DEFAULT NULL,
  `id_ma_area` int(11) DEFAULT NULL,
  `id_ma_rayon` int(11) DEFAULT NULL,
  `id_ma_satuan` int(11) DEFAULT NULL,
  `is_positive` int(11) DEFAULT NULL,
  `id_ma_tipe_laporan` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ma_lag` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_lead`
--

INSERT INTO `ma_lead` (`id`, `id_ma_unit`, `id_ma_area`, `id_ma_rayon`, `id_ma_satuan`, `is_positive`, `id_ma_tipe_laporan`, `nama`, `start_date`, `end_date`, `created_at`, `updated_at`, `id_ma_lag`) VALUES
(4, 6, NULL, NULL, 4, 1, 2, 'Melaksanakan monitoring kajian kelayakan dan persetujuan GM 1 kali setiap Senin oleh masing - masing PIC Region Banten dan Tangerang sesuai matrix layanan khusus dan SOP Premium Bundling', '2018-09-01', '2018-12-01', '2018-10-23 19:05:04', '2018-10-23 19:05:04', 6),
(5, 6, NULL, NULL, 4, 1, 2, 'Melaksanakan monitoring penginputan dan pengesahan master tarif premium akurasi data pelanggan layanan premium setiap minggu ke-4 oleh AE dan DM sesuai persetujuan GM dan PJBTL', '2018-09-01', '2018-12-31', '2018-10-23 19:24:28', '2018-10-23 19:24:28', 6),
(6, 6, NULL, NULL, 4, 1, 2, 'Melaksanakan monitoring laporan kendala penyebab daftar tunggu rapat satu minggu sekali, pantauan group WA, laporan DAFTUNG mingguan sesuai AP2T', '2018-09-01', '2018-12-31', '2018-10-23 19:27:20', '2018-10-23 19:27:21', 7),
(7, 6, NULL, NULL, 4, 1, 2, 'Melaksanakan monitoring ketersediaan material LPB rapat bulanan sesuai aplikasi Gudang Online dan SAP', '2018-09-01', '2018-12-31', '2018-10-23 19:28:28', '2018-10-23 19:28:28', 7);

-- --------------------------------------------------------

--
-- Table structure for table `ma_lm`
--

CREATE TABLE `ma_lm` (
  `id` int(11) NOT NULL,
  `id_ma_wig` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ma_polarisasi`
--

CREATE TABLE `ma_polarisasi` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_polarisasi`
--

INSERT INTO `ma_polarisasi` (`id`, `label`, `created_at`, `updated_at`) VALUES
(1, 'positive', '2018-10-22 20:33:38', '2018-10-22 20:33:38'),
(2, 'negative', '2018-10-22 20:33:42', '2018-10-22 20:33:42');

-- --------------------------------------------------------

--
-- Table structure for table `ma_rayon`
--

CREATE TABLE `ma_rayon` (
  `id` int(11) NOT NULL,
  `id_ma_area` int(11) DEFAULT NULL,
  `id_ma_unit` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_rayon`
--

INSERT INTO `ma_rayon` (`id`, `id_ma_area`, `id_ma_unit`, `nama`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Rayon Pariaman I', '2018-10-22 19:45:55', '2018-10-22 19:45:55'),
(2, 2, 4, 'Rayon Renon', '2018-10-23 00:25:15', '2018-10-23 00:25:15'),
(3, 92, 28, 'Unit Layanan Transmisi dan Gardu Induk BOGOR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(4, 93, 28, 'Unit Layanan Transmisi dan Gardu Induk SUKABUMI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(5, 95, 28, 'Unit Layanan Transmisi dan Gardu Induk BANDUNG BARAT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(6, 96, 28, 'Unit Layanan Transmisi dan Gardu Induk BANDUNG SELATAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(7, 97, 28, 'Unit Layanan Transmisi dan Gardu Induk BANDUNG TIMUR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(8, 99, 28, 'Unit Layanan Transmisi dan Gardu Induk CIKARANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(9, 100, 28, 'Unit Layanan Transmisi dan Gardu Induk KARAWANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(10, 101, 28, 'Unit Layanan Transmisi dan Gardu Induk PURWAKARTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(11, 102, 28, 'Unit Layanan Transmisi dan Gardu Induk BEKASI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(12, 104, 28, 'Unit Layanan Transmisi dan Gardu Induk CIREBON', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(13, 105, 28, 'Unit Layanan Transmisi dan Gardu Induk GARUT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(14, 106, 28, 'Unit Layanan Transmisi dan Gardu Induk CIAMIS', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(15, 107, 28, 'Unit Layanan Transmisi dan Gardu Induk JATIBARANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(16, 109, 28, 'Unit Layanan Transmisi dan Gardu Induk SALATIGA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(17, 110, 28, 'Unit Layanan Transmisi dan Gardu Induk SURAKARTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(18, 111, 28, 'Unit Layanan Transmisi dan Gardu Induk YOGYAKARTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(19, 113, 28, 'Unit Layanan Transmisi dan Gardu Induk PURWOKERTO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(20, 114, 28, 'Unit Layanan Transmisi dan Gardu Induk TEGAL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(21, 115, 28, 'Unit Layanan Transmisi dan Gardu Induk WONOSOBO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(22, 117, 28, 'Unit Layanan Transmisi dan Gardu Induk SEMARANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(23, 118, 28, 'Unit Layanan Transmisi dan Gardu Induk KUDUS', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(24, 119, 28, 'Unit Layanan Transmisi dan Gardu Induk REMBANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(25, 122, 29, 'Unit Layanan Transmisi dan Gardu Induk DURIKOSMBI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(26, 123, 29, 'Unit Layanan Transmisi dan Gardu Induk CIKUPA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(27, 124, 29, 'Unit Layanan Transmisi dan Gardu Induk TANGERANG KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(28, 126, 29, 'Unit Layanan Transmisi dan Gardu Induk KARET', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(29, 127, 29, 'Unit Layanan Transmisi dan Gardu Induk PUnit Layanan OGADUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(30, 102, 28, 'Unit Layanan Transmisi dan Gardu Induk BEKASI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(31, 130, 29, 'Unit Layanan Transmisi dan Gardu Induk GANDUnit Layanan ', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(32, 131, 29, 'Unit Layanan Transmisi dan Gardu Induk CAWANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(33, 132, 29, 'Unit Layanan Transmisi dan Gardu Induk TANGERANG SELATAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(34, 134, 29, 'Unit Layanan Transmisi dan Gardu Induk CILEGON', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(35, 135, 29, 'Unit Layanan Transmisi dan Gardu Induk SURALAYA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(36, 136, 29, 'Unit Layanan Transmisi dan Gardu Induk RANGKAS', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(37, 139, 30, 'Unit Layanan Transmisi dan Gardu Induk SURABAYA UTARA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(38, 141, 30, 'Unit Layanan Transmisi dan Gardu Induk KRIAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(39, 142, 30, 'Unit Layanan Transmisi dan Gardu Induk MALANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(40, 143, 30, 'Unit Layanan Transmisi dan Gardu Induk MOJOKERTO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(41, 145, 30, 'Unit Layanan Transmisi dan Gardu Induk BANGIL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(42, 146, 30, 'Unit Layanan Transmisi dan Gardu Induk JEMBER', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(43, 147, 30, 'Unit Layanan Transmisi dan Gardu Induk PROBOLINGGO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(44, 149, 30, 'Unit Layanan Transmisi dan Gardu Induk BABAT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(45, 150, 30, 'Unit Layanan Transmisi dan Gardu Induk MADIUN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(46, 151, 30, 'Unit Layanan Transmisi dan Gardu Induk KEDIRI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(47, 153, 30, 'Unit Layanan Transmisi dan Gardu Induk BALI UTARA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(48, 154, 30, 'Unit Layanan Transmisi dan Gardu Induk BALI SELATAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(49, 157, 31, 'Unit Layanan Transmisi dan Gardu Induk PONTIANAK', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(50, 158, 31, 'Unit Layanan Transmisi dan Gardu Induk SINGKAWANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(51, 160, 31, 'Unit Layanan Transmisi dan Gardu Induk BANDARMASIH', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(52, 161, 31, 'Unit Layanan Transmisi dan Gardu Induk BANJAR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(53, 162, 31, 'Unit Layanan Transmisi dan Gardu Induk BARABAI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(54, 163, 31, 'Unit Layanan Transmisi dan Gardu Induk PALANGKARAYA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(55, 165, 31, 'Unit Layanan Transmisi dan Gardu Induk BONTANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(56, 166, 31, 'Unit Layanan Transmisi dan Gardu Induk BALIKPAPAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(57, 167, 31, 'Unit Layanan Transmisi dan Gardu Induk SAMARINDA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(58, 169, 31, 'Unit Layanan  PLTD SIANTAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(59, 170, 31, 'Unit Layanan  PLTG SIANTAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(60, 171, 31, 'Unit Layanan  PLTD SINGKAWANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(61, 172, 31, 'Unit Layanan  PLTD SEI RAYA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(62, 174, 31, 'Unit Layanan  PLTMG BANGKANAI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(63, 175, 31, 'Unit Layanan  PLTA IR. P. M. NOOR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(64, 176, 31, 'Unit Layanan  PLTD TRISAKTI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(65, 177, 31, 'Unit Layanan  PLTD BENUA LIMA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(66, 178, 31, 'Unit Layanan  PLTG TRISAKTI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(67, 181, 31, 'Unit Layanan  PLTD BALIKPAPAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(68, 182, 31, 'Unit Layanan  PLTD PASER', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(69, 184, 31, 'Unit Layanan  PLTGU TANJUNG BATU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(70, 185, 31, 'Unit Layanan  PLTD-MG BONTANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(71, 186, 31, 'Unit Layanan  PLTD KARANG ASAM', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(72, 187, 31, 'Unit Layanan  PLTD KELEDANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(73, 188, 31, 'Unit Layanan  PLTG SEMBERAH', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(74, 191, 32, 'Unit Layanan Transmisi dan Gardu Induk I LOPANA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(75, 192, 32, 'Unit Layanan Transmisi dan Gardu Induk II SAWANGAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(76, 193, 32, 'Unit Layanan Transmisi dan Gardu Induk PALU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(77, 194, 32, 'Unit Layanan Transmisi dan Gardu Induk GORONTALO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(78, 196, 32, 'Unit Layanan Transmisi dan Gardu Induk PANAKKUKANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(79, 197, 32, 'Unit Layanan Transmisi dan Gardu Induk TELLO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(80, 198, 32, 'Unit Layanan Transmisi dan Gardu Induk PAREPARE', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(81, 199, 32, 'Unit Layanan Transmisi dan Gardu Induk SIDRAP', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(82, 200, 32, 'Unit Layanan Transmisi dan Gardu Induk BUnit Layanan UKUMBA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(83, 201, 32, 'Unit Layanan Transmisi dan Gardu Induk KENDARI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(84, 202, 32, 'Unit Layanan Transmisi dan Gardu Induk MAMUJU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(85, 205, 32, 'Unit Layanan  PLTA TANGGARI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(86, 206, 32, 'Unit Layanan  PLTA TONSEALAMA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(87, 207, 32, 'Unit Layanan  PLTD BITUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(88, 208, 32, 'Unit Layanan  PLTD LOPANA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(89, 209, 32, 'Unit Layanan  PLTP LAHENDONG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(90, 210, 32, 'Unit Layanan  PLTG MALEO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(91, 213, 32, 'Unit Layanan  PLTD TELLO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(92, 214, 32, 'Unit Layanan  PLTGU TELLO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(93, 215, 32, 'Unit Layanan  PLTD SELAYAR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(94, 217, 32, 'Unit Layanan  PLTM MAMUJU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(95, 218, 32, 'Unit Layanan  PLTA BAKARU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(96, 219, 32, 'Unit Layanan  PLTA BILI BILI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(97, 221, 32, 'Unit Layanan  PLTD WUA-WUA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(98, 222, 32, 'Unit Layanan  PLTD BAU-BAU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(99, 223, 32, 'Unit Layanan  PLTD KOLAKA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(100, 224, 32, 'Unit Layanan  PLTD POASIA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(101, 234, 34, 'Unit Layanan Pelanggan CIKANDE', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(102, 235, 34, 'Unit Layanan Pelanggan PRIMA KRAKATAU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(103, 236, 34, 'Unit Layanan Pelanggan SERANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(104, 237, 34, 'Unit Layanan Pelanggan CILEGON', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(105, 238, 34, 'Unit Layanan Pelanggan ANYER', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(106, 240, 34, 'Unit Layanan Pelanggan RANGKASBITUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(107, 241, 34, 'Unit Layanan Pelanggan PANDEGLANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(108, 242, 34, 'Unit Layanan Pelanggan MALINGPING', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(109, 243, 34, 'Unit Layanan Pelanggan LABUAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(110, 270, 8, 'Unit Layanan Pelanggan DENPASAR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(111, 271, 8, 'Unit Layanan Pelanggan KUTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(112, 272, 8, 'Unit Layanan Pelanggan MENGWI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(113, 273, 8, 'Unit Layanan Pelanggan TABANAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(114, 274, 8, 'Unit Layanan Pelanggan SANUR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(115, 276, 8, 'Unit Layanan Pelanggan NEGARA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(116, 277, 8, 'Unit Layanan Pelanggan SERIRIT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(117, 278, 8, 'Unit Layanan Pelanggan TEJAKUnit Layanan A', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(118, 279, 8, 'Unit Layanan Pelanggan SINGARAJA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(119, 280, 8, 'Unit Layanan Pelanggan GILIMANUK', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(120, 282, 8, 'Unit Layanan Pelanggan KARANGASEM', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(121, 283, 8, 'Unit Layanan Pelanggan GIANYAR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(122, 284, 8, 'Unit Layanan Pelanggan BANGLI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(123, 285, 8, 'Unit Layanan Pelanggan KLUNGKUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(124, 290, 37, 'Unit Layanan Pelanggan SUKOHARJO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(125, 291, 37, 'Unit Layanan Pelanggan GROGOL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(126, 292, 37, 'Unit Layanan Pelanggan WONOGIRI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(127, 292, 37, 'Unit Layanan Pelanggan JATISRONO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(128, 292, 37, 'Unit Layanan Pelanggan KARANGANYAR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(129, 296, 37, 'Unit Layanan Pelanggan DEMAK', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(130, 297, 37, 'Unit Layanan Pelanggan TEGOWANU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(131, 298, 37, 'Unit Layanan Pelanggan PURWODADI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(132, 299, 37, 'Unit Layanan Pelanggan WIROSARI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(133, 301, 37, 'Unit Layanan Pelanggan SLAWI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(134, 302, 37, 'Unit Layanan Pelanggan PEMALANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(135, 303, 37, 'Unit Layanan Pelanggan BREBES', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(136, 304, 37, 'Unit Layanan Pelanggan BUMIAYU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(137, 305, 37, 'Unit Layanan Pelanggan BALAPUnit Layanan ANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(138, 306, 37, 'Unit Layanan Pelanggan JATIBARANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(139, 307, 37, 'Unit Layanan Pelanggan COMAL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(140, 308, 37, 'Unit Layanan Pelanggan TEGAL KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(141, 309, 37, 'Unit Layanan Pelanggan TEGAL TIMUR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(142, 310, 37, 'Unit Layanan Pelanggan RANDUDONGKAL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(143, 312, 37, 'Unit Layanan Pelanggan KENDAL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(144, 313, 37, 'Unit Layanan Pelanggan WELERI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(145, 314, 37, 'Unit Layanan Pelanggan BOJA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(146, 315, 37, 'Unit Layanan Pelanggan SEMARANG SELATAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(147, 316, 37, 'Unit Layanan Pelanggan SEMARANG TIMUR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(148, 317, 37, 'Unit Layanan Pelanggan SEMARANG BARAT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(149, 318, 37, 'Unit Layanan Pelanggan SEMARANG TENGAH', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(150, 320, 37, 'Unit Layanan Pelanggan BATANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(151, 321, 37, 'Unit Layanan Pelanggan KEDUNGWUNI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(152, 322, 37, 'Unit Layanan Pelanggan PEKALONGAN KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(153, 323, 37, 'Unit Layanan Pelanggan WIRADESA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(154, 325, 37, 'Unit Layanan Pelanggan WONOSOBO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(155, 326, 37, 'Unit Layanan Pelanggan BANJARNEGARA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(156, 327, 37, 'Unit Layanan Pelanggan PURBALINGGA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(157, 328, 37, 'Unit Layanan Pelanggan BANYUMAS', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(158, 329, 37, 'Unit Layanan Pelanggan AJIBARANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(159, 330, 37, 'Unit Layanan Pelanggan WANGON', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(160, 331, 37, 'Unit Layanan Pelanggan PURWOKERTO KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(161, 333, 37, 'Unit Layanan Pelanggan KEBUMEN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(162, 334, 37, 'Unit Layanan Pelanggan GOMBONG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(163, 335, 37, 'Unit Layanan Pelanggan KROYA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(164, 336, 37, 'Unit Layanan Pelanggan SIDAREJA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(165, 337, 37, 'Unit Layanan Pelanggan MAJENANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(166, 338, 37, 'Unit Layanan Pelanggan CILACAP KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(167, 340, 37, 'Unit Layanan Pelanggan BANGSRI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(168, 341, 37, 'Unit Layanan Pelanggan JEPARA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(169, 342, 37, 'Unit Layanan Pelanggan KUDUS KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(170, 343, 37, 'Unit Layanan Pelanggan PATI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(171, 344, 37, 'Unit Layanan Pelanggan JUWANA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(172, 345, 37, 'Unit Layanan Pelanggan REMBANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(173, 346, 37, 'Unit Layanan Pelanggan BLORA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(174, 347, 37, 'Unit Layanan Pelanggan CEPU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(175, 349, 37, 'Unit Layanan Pelanggan BOYOLALI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(176, 350, 37, 'Unit Layanan Pelanggan KLATEN KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(177, 351, 37, 'Unit Layanan Pelanggan TUnit Layanan UNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(178, 352, 37, 'Unit Layanan Pelanggan PEDAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(179, 353, 37, 'Unit Layanan Pelanggan DELANGGU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(180, 355, 37, 'Unit Layanan Pelanggan BOROBUDUR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(181, 356, 37, 'Unit Layanan Pelanggan PURWOREJO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(182, 357, 37, 'Unit Layanan Pelanggan KUTOARJO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(183, 358, 37, 'Unit Layanan Pelanggan TEMANGGUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(184, 359, 37, 'Unit Layanan Pelanggan PARAKAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(185, 360, 37, 'Unit Layanan Pelanggan TEGALREJO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(186, 361, 37, 'Unit Layanan Pelanggan MAGELANG KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(187, 363, 37, 'Unit Layanan Pelanggan YOGYAKARTA KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(188, 364, 37, 'Unit Layanan Pelanggan BANTUnit Layanan ', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(189, 365, 37, 'Unit Layanan Pelanggan WATES', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(190, 366, 37, 'Unit Layanan Pelanggan WONOSARI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(191, 367, 37, 'Unit Layanan Pelanggan SLEMAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(192, 368, 37, 'Unit Layanan Pelanggan SEDAYU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(193, 369, 37, 'Unit Layanan Pelanggan KALASAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(194, 371, 37, 'Unit Layanan Pelanggan SRAGEN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(195, 372, 37, 'Unit Layanan Pelanggan PALUR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(196, 373, 37, 'Unit Layanan Pelanggan KARTASURA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(197, 374, 37, 'Unit Layanan Pelanggan SUMBERLAWANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(198, 375, 37, 'Unit Layanan Pelanggan MANAHAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(199, 376, 37, 'Unit Layanan Pelanggan SURAKARTA KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(200, 378, 37, 'Unit Layanan Pelanggan SALATIGA KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(201, 379, 37, 'Unit Layanan Pelanggan UNGARAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(202, 380, 37, 'Unit Layanan Pelanggan AMBARAWA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(203, 384, 38, 'Unit Layanan Pelanggan EMBONG WUNGSU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(204, 385, 38, 'Unit Layanan Pelanggan TANDES', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(205, 386, 38, 'Unit Layanan Pelanggan PERAK', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(206, 387, 38, 'Unit Layanan Pelanggan PLOSO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(207, 388, 38, 'Unit Layanan Pelanggan KENJERAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(208, 389, 38, 'Unit Layanan Pelanggan INDRAPURA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(209, 391, 38, 'Unit Layanan Pelanggan GEDANGAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(210, 392, 38, 'Unit Layanan Pelanggan NGAGEL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(211, 393, 38, 'Unit Layanan Pelanggan DARMO PERMAI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(212, 394, 38, 'Unit Layanan Pelanggan DUKUH KUPANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(213, 395, 38, 'Unit Layanan Pelanggan RUNGKUT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(214, 397, 38, 'Unit Layanan Pelanggan TAMAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(215, 398, 38, 'Unit Layanan Pelanggan KARANGPILANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(216, 399, 38, 'Unit Layanan Pelanggan MENGANTI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(217, 401, 38, 'Unit Layanan Pelanggan SIDAYU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(218, 402, 38, 'Unit Layanan Pelanggan BENJENG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(219, 403, 38, 'Unit Layanan Pelanggan BAWEAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(220, 404, 38, 'Unit Layanan Pelanggan GIRI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(221, 406, 38, 'Unit Layanan Pelanggan KRIAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(222, 407, 38, 'Unit Layanan Pelanggan PORONG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(223, 408, 38, 'Unit Layanan Pelanggan SIDOARJO KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(224, 410, 38, 'Unit Layanan Pelanggan MOJOAGUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(225, 411, 38, 'Unit Layanan Pelanggan JOMBANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(226, 412, 38, 'Unit Layanan Pelanggan NGORO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(227, 387, 38, 'Unit Layanan Pelanggan PLOSO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(228, 414, 38, 'Unit Layanan Pelanggan MOJOSARI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(229, 415, 38, 'Unit Layanan Pelanggan PACET', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(230, 416, 38, 'Unit Layanan Pelanggan KERTOSONO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(231, 417, 38, 'Unit Layanan Pelanggan WARUJAYENG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(232, 418, 38, 'Unit Layanan Pelanggan NGANJUK', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(233, 419, 38, 'Unit Layanan Pelanggan MOJOKERTO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(234, 421, 38, 'Unit Layanan Pelanggan BATU', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(235, 422, 38, 'Unit Layanan Pelanggan LAWANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(236, 423, 38, 'Unit Layanan Pelanggan BUnit Layanan Unit Layanan AWANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(237, 424, 38, 'Unit Layanan Pelanggan SINGOSARI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(238, 425, 38, 'Unit Layanan Pelanggan KEPANJEN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(239, 426, 38, 'Unit Layanan Pelanggan TUMPANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(240, 427, 38, 'Unit Layanan Pelanggan NGANTANG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(241, 428, 38, 'Unit Layanan Pelanggan GONDANGLEGI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(242, 429, 38, 'Unit Layanan Pelanggan MALANG KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(243, 430, 38, 'Unit Layanan Pelanggan KEBONAGUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(244, 431, 38, 'Unit Layanan Pelanggan DINOYO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(245, 432, 38, 'Unit Layanan Pelanggan BLIMBING', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(246, 433, 38, 'Unit Layanan Pelanggan DAMPIT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(247, 434, 38, 'Unit Layanan Pelanggan SUMBERPUCUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(248, 436, 38, 'Unit Layanan Pelanggan BANGIL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(249, 437, 38, 'Unit Layanan Pelanggan PANDAAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(250, 438, 38, 'Unit Layanan Pelanggan PRIGEN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(251, 439, 38, 'Unit Layanan Pelanggan PROBOLINGGO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(252, 440, 38, 'Unit Layanan Pelanggan KRAKSAAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(253, 441, 38, 'Unit Layanan Pelanggan SUKOREJO', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(254, 442, 38, 'Unit Layanan Pelanggan PASURUAN KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(255, 443, 38, 'Unit Layanan Pelanggan GONDANG WETAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(256, 444, 38, 'Unit Layanan Pelanggan GRATI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(257, 446, 38, 'Unit Layanan Pelanggan BLITAR', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(258, 447, 38, 'Unit Layanan Pelanggan TUnit Layanan UNGAGUNG', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(259, 448, 38, 'Unit Layanan Pelanggan PARE', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(260, 449, 38, 'Unit Layanan Pelanggan WLINGI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(261, 450, 38, 'Unit Layanan Pelanggan CAMPURDARAT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(262, 451, 38, 'Unit Layanan Pelanggan SUTOJAYAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(263, 452, 38, 'Unit Layanan Pelanggan SRENGAT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(264, 453, 38, 'Unit Layanan Pelanggan NGUNUT', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(265, 454, 38, 'Unit Layanan Pelanggan KEDIRI KOTA', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(266, 455, 38, 'Unit Layanan Pelanggan NGADILUWIH', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(267, 291, 37, 'Unit Layanan Pelanggan GROGOL', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(268, 458, 38, 'Unit Layanan Pelanggan NGAWI', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(269, 459, 38, 'Unit Layanan Pelanggan MAGETAN', '2018-11-07 02:02:34', '2018-11-07 02:02:34'),
(270, 460, 38, 'Unit Layanan Pelanggan MAOSPATI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(271, 461, 38, 'Unit Layanan Pelanggan CARUBAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(272, 462, 38, 'Unit Layanan Pelanggan DOLOPO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(273, 463, 38, 'Unit Layanan Pelanggan MANTINGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(274, 464, 38, 'Unit Layanan Pelanggan MADIUN KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(275, 466, 38, 'Unit Layanan Pelanggan LAMONGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(276, 467, 38, 'Unit Layanan Pelanggan BABAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(277, 468, 38, 'Unit Layanan Pelanggan TUBAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(278, 469, 38, 'Unit Layanan Pelanggan PADANGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(279, 470, 38, 'Unit Layanan Pelanggan JATIROGO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(280, 471, 38, 'Unit Layanan Pelanggan BRONDONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(281, 472, 38, 'Unit Layanan Pelanggan BOJONEGORO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(282, 473, 38, 'Unit Layanan Pelanggan SUMBERREJO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(283, 475, 38, 'Unit Layanan Pelanggan KALISAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(284, 476, 38, 'Unit Layanan Pelanggan AMBUnit Layanan U', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(285, 477, 38, 'Unit Layanan Pelanggan RAMBIPUJI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(286, 478, 38, 'Unit Layanan Pelanggan TANGGUnit Layanan ', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(287, 479, 38, 'Unit Layanan Pelanggan KENCONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(288, 480, 38, 'Unit Layanan Pelanggan LUMAJANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(289, 481, 38, 'Unit Layanan Pelanggan KLAKAH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(290, 482, 38, 'Unit Layanan Pelanggan TEMPEH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(291, 483, 38, 'Unit Layanan Pelanggan JEMBER KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(292, 485, 38, 'Unit Layanan Pelanggan ASEMBAGUS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(293, 486, 38, 'Unit Layanan Pelanggan BESUKI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(294, 487, 38, 'Unit Layanan Pelanggan BONDOWOSO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(295, 488, 38, 'Unit Layanan Pelanggan PANARUKAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(296, 366, 37, 'Unit Layanan Pelanggan WONOSARI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(297, 491, 38, 'Unit Layanan Pelanggan ROGOJAMPI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(298, 492, 38, 'Unit Layanan Pelanggan GENTENG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(299, 493, 38, 'Unit Layanan Pelanggan MUNCAR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(300, 494, 38, 'Unit Layanan Pelanggan JAJAG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(301, 495, 38, 'Unit Layanan Pelanggan BYWG KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(302, 497, 38, 'Unit Layanan Pelanggan KEP KANGEAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(303, 498, 38, 'Unit Layanan Pelanggan KAMAL', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(304, 499, 38, 'Unit Layanan Pelanggan BANGKALAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(305, 500, 38, 'Unit Layanan Pelanggan BLEGA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(306, 501, 38, 'Unit Layanan Pelanggan KETAPANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(307, 502, 38, 'Unit Layanan Pelanggan SAMPANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(308, 503, 38, 'Unit Layanan Pelanggan PRENDUAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(309, 504, 38, 'Unit Layanan Pelanggan SUMENEP', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(310, 505, 38, 'Unit Layanan Pelanggan AMBUNTEN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(311, 506, 38, 'Unit Layanan Pelanggan WARU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(312, 507, 38, 'Unit Layanan Pelanggan PAMEKASAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(313, 509, 38, 'Unit Layanan Pelanggan PONOROGO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(314, 510, 38, 'Unit Layanan Pelanggan PACITAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(315, 511, 38, 'Unit Layanan Pelanggan BALONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(316, 512, 38, 'Unit Layanan Pelanggan TRENGGALEK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(317, 517, 39, 'Unit Layanan Pelanggan CIKAMPEK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(318, 518, 39, 'Unit Layanan Pelanggan KARAWANG KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(319, 519, 39, 'Unit Layanan Pelanggan KOSAMBI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(320, 520, 39, 'Unit Layanan Pelanggan PRIMA KARAWANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(321, 521, 39, 'Unit Layanan Pelanggan RENGASDENGKLOK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(322, 523, 39, 'Unit Layanan Pelanggan PRIMA PRIANGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(323, 524, 39, 'Unit Layanan Pelanggan BANDUNG UTARA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(324, 525, 39, 'Unit Layanan Pelanggan BANDUNG SELATAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(325, 526, 39, 'Unit Layanan Pelanggan BANDUNG TIMUR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(326, 527, 39, 'Unit Layanan Pelanggan BANDUNG BARAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(327, 528, 39, 'Unit Layanan Pelanggan UJUNG BERUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(328, 529, 39, 'Unit Layanan Pelanggan KOPO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(329, 530, 39, 'Unit Layanan Pelanggan CIJAWURA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(330, 532, 39, 'Unit Layanan Pelanggan DEPOK KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(331, 533, 39, 'Unit Layanan Pelanggan SAWANGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(332, 534, 39, 'Unit Layanan Pelanggan CIBINONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(333, 535, 39, 'Unit Layanan Pelanggan CIMANGGIS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(334, 536, 39, 'Unit Layanan Pelanggan BOJONG GEDE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(335, 538, 39, 'Unit Layanan Pelanggan CIBATU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(336, 539, 39, 'Unit Layanan Pelanggan CIKAJANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(337, 540, 39, 'Unit Layanan Pelanggan GARUT KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(338, 541, 39, 'Unit Layanan Pelanggan LELES', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(339, 542, 39, 'Unit Layanan Pelanggan PAMEUNGPEUK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(340, 544, 39, 'Unit Layanan Pelanggan CILEUNGSI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(341, 545, 39, 'Unit Layanan Pelanggan CITEUREUP', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(342, 546, 39, 'Unit Layanan Pelanggan JONGGOL', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(343, 548, 39, 'Unit Layanan Pelanggan BOGOR BARAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(344, 549, 39, 'Unit Layanan Pelanggan BOGOR KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(345, 550, 39, 'Unit Layanan Pelanggan BOGOR TIMUR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(346, 551, 39, 'Unit Layanan Pelanggan CIPAYUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(347, 552, 39, 'Unit Layanan Pelanggan JASINGA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(348, 553, 39, 'Unit Layanan Pelanggan LEUWILIANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(349, 554, 39, 'Unit Layanan Pelanggan PAKUAN BOGOR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(350, 556, 39, 'Unit Layanan Pelanggan BALE ENDAH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(351, 557, 39, 'Unit Layanan Pelanggan BANJARAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(352, 558, 39, 'Unit Layanan Pelanggan MAJALAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(353, 559, 39, 'Unit Layanan Pelanggan PRIMA MAJALAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(354, 560, 39, 'Unit Layanan Pelanggan RANCAEKEK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(355, 561, 39, 'Unit Layanan Pelanggan SOREANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(356, 563, 39, 'Unit Layanan Pelanggan TASIKMALAYA KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(357, 564, 39, 'Unit Layanan Pelanggan SINGAPARNA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(358, 565, 39, 'Unit Layanan Pelanggan KARANGNUNGGAL', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(359, 566, 39, 'Unit Layanan Pelanggan RAJAPOLAH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(360, 567, 39, 'Unit Layanan Pelanggan CIAMIS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(361, 568, 39, 'Unit Layanan Pelanggan BANJAR KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(362, 569, 39, 'Unit Layanan Pelanggan PANGANDARAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(363, 571, 39, 'Unit Layanan Pelanggan CIBADAK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(364, 572, 39, 'Unit Layanan Pelanggan SUKABUMI KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(365, 573, 39, 'Unit Layanan Pelanggan CIKEMBAR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(366, 574, 39, 'Unit Layanan Pelanggan SUKARAJA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(367, 575, 39, 'Unit Layanan Pelanggan PELABUHAN RATU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(368, 576, 39, 'Unit Layanan Pelanggan CICURUG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(369, 578, 39, 'Unit Layanan Pelanggan CILILIN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(370, 579, 39, 'Unit Layanan Pelanggan CIMAHI KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(371, 580, 39, 'Unit Layanan Pelanggan CIMAHI SELATAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(372, 581, 39, 'Unit Layanan Pelanggan LEMBANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(373, 582, 39, 'Unit Layanan Pelanggan PADALARANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(374, 583, 39, 'Unit Layanan Pelanggan PRIMA CIBABAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(375, 584, 39, 'Unit Layanan Pelanggan RAJAMANDALA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(376, 586, 39, 'Unit Layanan Pelanggan CIANJUR KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(377, 587, 39, 'Unit Layanan Pelanggan CIPANAS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(378, 588, 39, 'Unit Layanan Pelanggan MANDE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(379, 589, 39, 'Unit Layanan Pelanggan SUKANAGARA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(380, 590, 39, 'Unit Layanan Pelanggan TANGGEUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(381, 592, 39, 'Unit Layanan Pelanggan BEKASI KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(382, 593, 39, 'Unit Layanan Pelanggan BABELAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(383, 594, 39, 'Unit Layanan Pelanggan BANTAR GEBANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(384, 595, 39, 'Unit Layanan Pelanggan CIBITUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(385, 596, 39, 'Unit Layanan Pelanggan CIKARANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(386, 597, 39, 'Unit Layanan Pelanggan LEMAH ABANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(387, 598, 39, 'Unit Layanan Pelanggan MEDAN SATRIA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(388, 599, 39, 'Unit Layanan Pelanggan MUSTIKA JAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(389, 600, 39, 'Unit Layanan Pelanggan PRIMA BEKASI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(390, 601, 39, 'Unit Layanan Pelanggan TAMBUN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(391, 603, 39, 'Unit Layanan Pelanggan PAGADEN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(392, 604, 39, 'Unit Layanan Pelanggan PAMANUKAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(393, 605, 39, 'Unit Layanan Pelanggan PLERED', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(394, 606, 39, 'Unit Layanan Pelanggan PURWAKARTA KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(395, 607, 39, 'Unit Layanan Pelanggan SUBANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(396, 609, 39, 'Unit Layanan Pelanggan JATIWANGI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(397, 610, 39, 'Unit Layanan Pelanggan MAJALENGKA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(398, 611, 39, 'Unit Layanan Pelanggan SUMEDANG KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(399, 612, 39, 'Unit Layanan Pelanggan TANJUNG SARI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(400, 614, 39, 'Unit Layanan Pelanggan CILEDUG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(401, 615, 39, 'Unit Layanan Pelanggan CILIMUS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(402, 616, 39, 'Unit Layanan Pelanggan CIREBON KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(403, 617, 39, 'Unit Layanan Pelanggan HAURGEUnit Layanan IS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(404, 618, 39, 'Unit Layanan Pelanggan INDRAMAYU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(405, 306, 37, 'Unit Layanan Pelanggan JATIBARANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(406, 620, 39, 'Unit Layanan Pelanggan KUNINGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(407, 621, 39, 'Unit Layanan Pelanggan SUMBER', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(408, 628, 40, 'Unit Layanan Pelanggan PUnit Layanan UNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(409, 629, 40, 'Unit Layanan Pelanggan BLAMBANGAN UMPU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(410, 630, 40, 'Unit Layanan Pelanggan BUKIT KEMUNING', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(411, 631, 40, 'Unit Layanan Pelanggan BUMI ABUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(412, 632, 40, 'Unit Layanan Pelanggan LIWA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(413, 633, 40, 'Unit Layanan Pelanggan MENGGALA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(414, 635, 40, 'Unit Layanan Pelanggan BANDARJAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(415, 636, 40, 'Unit Layanan Pelanggan KALIREJO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(416, 637, 40, 'Unit Layanan Pelanggan KOTA AGUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(417, 638, 40, 'Unit Layanan Pelanggan KOTA METRO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(418, 639, 40, 'Unit Layanan Pelanggan PRINGSEWU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(419, 640, 40, 'Unit Layanan Pelanggan RUMBIA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(420, 641, 40, 'Unit Layanan Pelanggan SRIBHAWONO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(421, 642, 40, 'Unit Layanan Pelanggan SUKADANA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(422, 643, 40, 'Unit Layanan Pelanggan TALANG PADANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(423, 645, 40, 'Unit Layanan Pelanggan KALIANDA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(424, 646, 40, 'Unit Layanan Pelanggan KARANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(425, 647, 40, 'Unit Layanan Pelanggan NATAR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(426, 648, 40, 'Unit Layanan Pelanggan SIDOMUnit Layanan YO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(427, 649, 40, 'Unit Layanan Pelanggan SUTAMI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(428, 650, 40, 'Unit Layanan Pelanggan TELUK BETUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(429, 651, 40, 'Unit Layanan Pelanggan WAY HALIM', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(430, 658, 41, 'Unit Layanan Pelanggan KWANDANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(431, 659, 41, 'Unit Layanan Pelanggan LIMBOTO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(432, 660, 41, 'Unit Layanan Pelanggan MARISA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(433, 661, 41, 'Unit Layanan Pelanggan TELAGA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(434, 662, 41, 'Unit Layanan  PLTD TELAGA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(435, 664, 41, 'Unit Layanan Pelanggan BOLMUT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(436, 665, 41, 'Unit Layanan Pelanggan IMANDI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(437, 666, 41, 'Unit Layanan Pelanggan INOBONTO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(438, 667, 41, 'Unit Layanan Pelanggan MODAYAG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(439, 668, 41, 'Unit Layanan Pelanggan MOLIBAGU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(440, 669, 41, 'Unit Layanan  PLTD KOTAMOBAGU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(441, 671, 41, 'Unit Layanan Pelanggan AMPANA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(442, 672, 41, 'Unit Layanan Pelanggan BANGGAI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(443, 673, 41, 'Unit Layanan Pelanggan TOILI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(444, 674, 41, 'Unit Layanan  PLTD LUWUK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(445, 676, 41, 'Unit Layanan Pelanggan AIRMADIDI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(446, 677, 41, 'Unit Layanan Pelanggan AMURANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(447, 678, 41, 'Unit Layanan Pelanggan BITUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(448, 679, 41, 'Unit Layanan Pelanggan KAWANGKOAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(449, 680, 41, 'Unit Layanan Pelanggan MANADO UTARA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(450, 681, 41, 'Unit Layanan Pelanggan MOTOLING', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(451, 682, 41, 'Unit Layanan Pelanggan PANIKI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(452, 683, 41, 'Unit Layanan Pelanggan RATAHAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(453, 684, 41, 'Unit Layanan Pelanggan TOMOHON', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(454, 685, 41, 'Unit Layanan Pelanggan TONDANO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(455, 686, 41, 'Unit Layanan Pelanggan MANADO SELATAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(456, 688, 41, 'Unit Layanan Pelanggan POSO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(457, 689, 41, 'Unit Layanan Pelanggan KAMONJI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(458, 690, 41, 'Unit Layanan Pelanggan KOLONEDALE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(459, 691, 41, 'Unit Layanan Pelanggan PALU KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(460, 692, 41, 'Unit Layanan Pelanggan PARIGI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(461, 693, 41, 'Unit Layanan Pelanggan TENTENA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(462, 694, 41, 'Unit Layanan Pelanggan TAMBU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(463, 695, 41, 'Unit Layanan Pelanggan TAWAELI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(464, 696, 41, 'Unit Layanan Pelanggan DONGGALA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(465, 697, 41, 'Unit Layanan Pelanggan BUNGKU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(466, 698, 41, 'Unit Layanan  PLTD SILAE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(467, 700, 41, 'Unit Layanan Pelanggan BEO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(468, 701, 41, 'Unit Layanan Pelanggan LIRUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(469, 702, 41, 'Unit Layanan Pelanggan MELONGUANE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(470, 703, 41, 'Unit Layanan Pelanggan TAGUnit Layanan ANDANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(471, 704, 41, 'Unit Layanan Pelanggan TAMAKO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(472, 705, 41, 'Unit Layanan Pelanggan PETTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(473, 706, 41, 'Unit Layanan Pelanggan SIAU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(474, 707, 41, 'Unit Layanan  PLTD TAHUNA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(475, 709, 41, 'Unit Layanan Pelanggan BANGKIR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(476, 710, 41, 'Unit Layanan Pelanggan KOTARAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(477, 711, 41, 'Unit Layanan Pelanggan LEOK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(478, 712, 41, 'Unit Layanan Pelanggan MOUTONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(479, 713, 41, 'Unit Layanan  PLTD TOLI-TOLI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(480, 719, 42, 'Unit Layanan Pelanggan PANAKKUKANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(481, 720, 42, 'Unit Layanan Pelanggan MALINO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(482, 721, 42, 'Unit Layanan Pelanggan KALEBAJENG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(483, 722, 42, 'Unit Layanan Pelanggan MATTOANGING', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(484, 723, 42, 'Unit Layanan Pelanggan SUNGGUMINASA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(485, 724, 42, 'Unit Layanan Pelanggan TAKALAR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(486, 726, 42, 'Unit Layanan Pelanggan DAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(487, 727, 42, 'Unit Layanan Pelanggan PANGKEP', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(488, 728, 42, 'Unit Layanan Pelanggan MAROS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(489, 729, 42, 'Unit Layanan Pelanggan KAREBOSI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(490, 731, 42, 'Unit Layanan Pelanggan MATTIROTASI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(491, 732, 42, 'Unit Layanan Pelanggan BARRU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(492, 733, 42, 'Unit Layanan Pelanggan PANGSID', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(493, 734, 42, 'Unit Layanan Pelanggan RAPPANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(494, 735, 42, 'Unit Layanan Pelanggan TANRU TEDONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(495, 736, 42, 'Unit Layanan Pelanggan PAJALESANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(496, 737, 42, 'Unit Layanan Pelanggan WATANG SOPPENG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(497, 739, 42, 'Unit Layanan Pelanggan HASANUDDIN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(498, 740, 42, 'Unit Layanan Pelanggan SENGKANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(499, 741, 42, 'Unit Layanan Pelanggan PARIA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(500, 742, 42, 'Unit Layanan Pelanggan PATANGKAI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(501, 743, 42, 'Unit Layanan Pelanggan Unit Layanan OE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(502, 744, 42, 'Unit Layanan Pelanggan TELLU BOCCOE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(503, 746, 42, 'Unit Layanan Pelanggan WUA-WUA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(504, 747, 42, 'Unit Layanan Pelanggan BENUBENUA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(505, 748, 42, 'Unit Layanan Pelanggan KOLAKA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(506, 749, 42, 'Unit Layanan Pelanggan UNAAHA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(507, 750, 42, 'Unit Layanan Pelanggan KONAWE SELATAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(508, 751, 42, 'Unit Layanan Pelanggan BOMBANA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(509, 752, 42, 'Unit Layanan Pelanggan KOLAKA UTARA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(510, 754, 42, 'Unit Layanan Pelanggan PALOPO KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(511, 755, 42, 'Unit Layanan Pelanggan MASAMBA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(512, 756, 42, 'Unit Layanan Pelanggan RANTEPAO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(513, 757, 42, 'Unit Layanan Pelanggan MAKALE', '2018-11-07 02:02:35', '2018-11-07 02:02:35');
INSERT INTO `ma_rayon` (`id`, `id_ma_area`, `id_ma_unit`, `nama`, `created_at`, `updated_at`) VALUES
(514, 758, 42, 'Unit Layanan Pelanggan BELOPA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(515, 759, 42, 'Unit Layanan Pelanggan MALILI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(516, 760, 42, 'Unit Layanan Pelanggan TOMONI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(517, 762, 42, 'Unit Layanan Pelanggan WATANG SAWITTO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(518, 763, 42, 'Unit Layanan Pelanggan KARIANGO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(519, 764, 42, 'Unit Layanan Pelanggan PEKKABATA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(520, 765, 42, 'Unit Layanan Pelanggan LAKAWAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(521, 766, 42, 'Unit Layanan Pelanggan ENREKANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(522, 768, 42, 'Unit Layanan Pelanggan PANRITA LOPI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(523, 769, 42, 'Unit Layanan Pelanggan SINJAI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(524, 770, 42, 'Unit Layanan Pelanggan JENEPONTO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(525, 771, 42, 'Unit Layanan Pelanggan BANTAENG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(526, 772, 42, 'Unit Layanan Pelanggan SELAYAR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(527, 773, 42, 'Unit Layanan Pelanggan TANETE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(528, 774, 42, 'Unit Layanan Pelanggan KALUMPANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(529, 776, 42, 'Unit Layanan Pelanggan BAU-BAU KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(530, 777, 42, 'Unit Layanan Pelanggan RAHA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(531, 778, 42, 'Unit Layanan Pelanggan WANGI-WANGI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(532, 779, 42, 'Unit Layanan Pelanggan PASAR WAJO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(533, 780, 42, 'Unit Layanan Pelanggan MAWASANGKA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(534, 782, 42, 'Unit Layanan Pelanggan MANAKARRA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(535, 783, 42, 'Unit Layanan Pelanggan MAJENE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(536, 784, 42, 'Unit Layanan Pelanggan WONOMUnit Layanan YO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(537, 785, 42, 'Unit Layanan Pelanggan POLEWALI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(538, 786, 42, 'Unit Layanan Pelanggan MAMASA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(539, 787, 42, 'Unit Layanan Pelanggan PASANGKAYU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(540, 793, 43, 'Unit Layanan Pelanggan KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(541, 794, 43, 'Unit Layanan Pelanggan MEMPAWAH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(542, 795, 43, 'Unit Layanan Pelanggan NGABANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(543, 796, 43, 'Unit Layanan Pelanggan RASAU JAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(544, 797, 43, 'Unit Layanan Pelanggan SIANTAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(545, 798, 43, 'Unit Layanan Pelanggan SUNGAI JAWI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(546, 799, 43, 'Unit Layanan Pelanggan SUNGAI KAKAP', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(547, 801, 43, 'Unit Layanan Pelanggan SINGKAWANG KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(548, 802, 43, 'Unit Layanan Pelanggan BENGKAYANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(549, 803, 43, 'Unit Layanan Pelanggan PEMANGKAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(550, 804, 43, 'Unit Layanan Pelanggan SAMBAS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(551, 805, 43, 'Unit Layanan Pelanggan SEI DURI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(552, 806, 43, 'Unit Layanan Pelanggan SEKURA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(553, 807, 43, 'Unit Layanan  PLTD SAMBAS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(554, 809, 43, 'Unit Layanan Pelanggan SANGGAU KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(555, 810, 43, 'Unit Layanan Pelanggan BALAI KARANGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(556, 811, 43, 'Unit Layanan Pelanggan NANGA PINOH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(557, 812, 43, 'Unit Layanan Pelanggan PUTUSSIBAU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(558, 813, 43, 'Unit Layanan Pelanggan SEKADAU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(559, 814, 43, 'Unit Layanan Pelanggan SINTANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(560, 815, 43, 'Unit Layanan  PLTD MENYURAI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(561, 816, 43, 'Unit Layanan  PLTD SEMBOJA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(562, 818, 43, 'Unit Layanan Pelanggan KETAPANG KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(563, 819, 43, 'Unit Layanan Pelanggan SANDAI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(564, 642, 40, 'Unit Layanan Pelanggan SUKADANA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(565, 821, 43, 'Unit Layanan Pelanggan TUMBANG TITI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(566, 822, 43, 'Unit Layanan  PLTD SUKAHARJA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(567, 828, 44, 'Unit Layanan Pelanggan LAMBUNG MANGKURAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(568, 829, 44, 'Unit Layanan Pelanggan AHMAD YANI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(569, 830, 44, 'Unit Layanan Pelanggan MARTAPURA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(570, 831, 44, 'Unit Layanan Pelanggan BANJARBARU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(571, 832, 44, 'Unit Layanan Pelanggan PELAIHARI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(572, 833, 44, 'Unit Layanan Pelanggan MARABAHAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(573, 834, 44, 'Unit Layanan Pelanggan GAMBUT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(574, 836, 44, 'Unit Layanan Pelanggan TANJUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(575, 837, 44, 'Unit Layanan Pelanggan AMUNTAI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(576, 838, 44, 'Unit Layanan Pelanggan KANDANGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(577, 839, 44, 'Unit Layanan Pelanggan DAHA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(578, 840, 44, 'Unit Layanan Pelanggan RANTAU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(579, 841, 44, 'Unit Layanan Pelanggan BINUANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(580, 842, 44, 'Unit Layanan Pelanggan PARINGIN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(581, 844, 44, 'Unit Layanan Pelanggan BATU LICIN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(582, 845, 44, 'Unit Layanan Pelanggan SATUI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(583, 846, 44, 'Unit Layanan  PLTD KOTA BARU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(584, 847, 44, 'Unit Layanan  PLTD PAGATAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(585, 849, 44, 'Unit Layanan Pelanggan BUNTOK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(586, 850, 44, 'Unit Layanan Pelanggan MUARA TEWEH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(587, 851, 44, 'Unit Layanan Pelanggan PUnit Layanan ANG PISAU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(588, 852, 44, 'Unit Layanan Pelanggan TAMIANG LAYANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(589, 853, 44, 'Unit Layanan Pelanggan PURUK CAHU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(590, 855, 44, 'Unit Layanan Pelanggan PANGKALAN BUN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(591, 856, 44, 'Unit Layanan Pelanggan SAMPIT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(592, 857, 44, 'Unit Layanan Pelanggan KUALA PEMBUANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(593, 858, 44, 'Unit Layanan Pelanggan KASONGAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(594, 859, 44, 'Unit Layanan Pelanggan KUALA KURUN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(595, 860, 44, 'Unit Layanan Pelanggan NANGA BUnit Layanan IK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(596, 861, 44, 'Unit Layanan Pelanggan SUKAMARA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(597, 862, 44, 'Unit Layanan Pelanggan PALANGKARAYA BARAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(598, 863, 44, 'Unit Layanan Pelanggan PALANGKARAYA TIMUR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(599, 864, 44, 'Unit Layanan  PLTD BAAMANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(600, 865, 44, 'Unit Layanan  PLTD KUMAI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(601, 866, 44, 'Unit Layanan  PLTD KAHAYAN BARU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(602, 869, 45, 'Unit Layanan Pelanggan BALIKPAPAN SELATAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(603, 870, 45, 'Unit Layanan Pelanggan BALIKPAPAN UTARA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(604, 871, 45, 'Unit Layanan Pelanggan LONGIKIS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(605, 872, 45, 'Unit Layanan Pelanggan PETUNG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(606, 873, 45, 'Unit Layanan Pelanggan SAMBOJA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(607, 874, 45, 'Unit Layanan Pelanggan TANAH GROGOT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(608, 876, 45, 'Unit Layanan Pelanggan KOTA BANGUN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(609, 877, 45, 'Unit Layanan Pelanggan MELAK', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(610, 878, 45, 'Unit Layanan Pelanggan SAMARINDA ILIR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(611, 879, 45, 'Unit Layanan Pelanggan SAMARINDA KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(612, 880, 45, 'Unit Layanan Pelanggan SAMARINDA SEBERANG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(613, 881, 45, 'Unit Layanan Pelanggan SAMARINDA Unit Layanan U', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(614, 882, 45, 'Unit Layanan Pelanggan TENGGARONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(615, 884, 45, 'Unit Layanan Pelanggan MALINAU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(616, 885, 45, 'Unit Layanan Pelanggan NUNUKAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(617, 886, 45, 'Unit Layanan Pelanggan TANJUNG SELOR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(618, 887, 45, 'Unit Layanan Pelanggan TANJUNG REDEB', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(619, 888, 45, 'Unit Layanan  PLTD BERAU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(620, 890, 45, 'Unit Layanan Pelanggan SANGATTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(621, 891, 45, 'Unit Layanan Pelanggan BONTANG KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(622, 898, 46, 'Unit Layanan Pelanggan MAKO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(623, 899, 46, 'Unit Layanan Pelanggan NAMROLE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(624, 900, 46, 'Unit Layanan Pelanggan BAGUALA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(625, 901, 46, 'Unit Layanan Pelanggan BANDA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(626, 902, 46, 'Unit Layanan Pelanggan HARUKU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(627, 903, 46, 'Unit Layanan Pelanggan HITU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(628, 904, 46, 'Unit Layanan Pelanggan NAMLEA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(629, 905, 46, 'Unit Layanan Pelanggan NUSANIWE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(630, 906, 46, 'Unit Layanan Pelanggan TUnit Layanan EHU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(631, 907, 46, 'Unit Layanan Pelanggan SAPARUA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(632, 908, 46, 'Unit Layanan Pelanggan AMBON KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(633, 910, 46, 'Unit Layanan Pelanggan BUnit Layanan A', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(634, 911, 46, 'Unit Layanan Pelanggan PIRU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(635, 912, 46, 'Unit Layanan Pelanggan MASOHI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(636, 913, 46, 'Unit Layanan Pelanggan KOBISONTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(637, 914, 46, 'Unit Layanan Pelanggan KAIRATU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(638, 916, 46, 'Unit Layanan Pelanggan JAILOLO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(639, 917, 46, 'Unit Layanan Pelanggan TOBELO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(640, 918, 46, 'Unit Layanan Pelanggan SOFIFI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(641, 919, 46, 'Unit Layanan Pelanggan MABA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(642, 920, 46, 'Unit Layanan Pelanggan WEDA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(643, 919, 46, 'Unit Layanan Pelanggan DARUBA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(644, 923, 46, 'Unit Layanan Pelanggan BOBONG', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(645, 924, 46, 'Unit Layanan Pelanggan LAIWUI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(646, 925, 46, 'Unit Layanan Pelanggan SAKETA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(647, 926, 46, 'Unit Layanan Pelanggan DOFA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(648, 927, 46, 'Unit Layanan Pelanggan BACAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(649, 928, 46, 'Unit Layanan Pelanggan SANANA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(650, 929, 46, 'Unit Layanan Pelanggan SOA SIU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(651, 930, 46, 'Unit Layanan Pelanggan TERNATE SELATAN', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(652, 932, 46, 'Unit Layanan Pelanggan ELAT', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(653, 933, 46, 'Unit Layanan Pelanggan MOA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(654, 934, 46, 'Unit Layanan Pelanggan DOBO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(655, 935, 46, 'Unit Layanan Pelanggan TUAL KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(656, 936, 46, 'Unit Layanan Pelanggan SAUMLAKI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(657, 940, 46, 'Unit Layanan  PLTD NAMLEA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(658, 941, 46, 'Unit Layanan  PLTD MASOHI', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(659, 942, 46, 'Unit Layanan  PLTD KAIRATU', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(660, 943, 46, 'Unit Layanan  PLTD HATIVE KECIL', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(661, 944, 46, 'Unit Layanan  PLTD POKA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(662, 945, 46, 'Unit Layanan  PLTD KAYU MERAH', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(663, 946, 46, 'Unit Layanan  PLTD TOBELO', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(664, 947, 46, 'Unit Layanan  PLTD LANGGUR', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(665, 948, 46, 'Unit Layanan PelangganLTU TIDORE', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(666, 953, 47, 'Unit Layanan Pelanggan AGATS', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(667, 954, 47, 'Unit Layanan Pelanggan TIMIKA JAYA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(668, 955, 47, 'Unit Layanan Pelanggan TIMIKA KOTA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(669, 957, 47, 'Unit Layanan Pelanggan JAYAPURA', '2018-11-07 02:02:35', '2018-11-07 02:02:35'),
(670, 958, 47, 'Unit Layanan Pelanggan ABEPURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(671, 959, 47, 'Unit Layanan Pelanggan SENTANI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(672, 960, 47, 'Unit Layanan Pelanggan ARSO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(673, 961, 47, 'Unit Layanan Pelanggan GENYEM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(674, 962, 47, 'Unit Layanan Pelanggan SARMI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(675, 963, 47, 'Unit Layanan Pelanggan WAMENA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(676, 965, 47, 'Unit Layanan Pelanggan SORONG KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(677, 966, 47, 'Unit Layanan Pelanggan WAISAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(678, 967, 47, 'Unit Layanan Pelanggan FAKFAK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(679, 968, 47, 'Unit Layanan Pelanggan KAIMANA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(680, 969, 47, 'Unit Layanan Pelanggan AIMAS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(681, 970, 47, 'Unit Layanan Pelanggan TEMINABUAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(682, 972, 47, 'Unit Layanan Pelanggan BIAK KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(683, 973, 47, 'Unit Layanan Pelanggan WAROPEN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(684, 974, 47, 'Unit Layanan Pelanggan YOMDORI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(685, 975, 47, 'Unit Layanan Pelanggan SERUI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(686, 977, 47, 'Unit Layanan Pelanggan MANOKWARI KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(687, 978, 47, 'Unit Layanan Pelanggan WASIOR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(688, 979, 47, 'Unit Layanan Pelanggan PRAFI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(689, 980, 47, 'Unit Layanan Pelanggan NABIRE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(690, 981, 47, 'Unit Layanan Pelanggan BINTUNI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(691, 983, 47, 'Unit Layanan Pelanggan MERAUKE KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(692, 984, 47, 'Unit Layanan Pelanggan KEPI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(693, 985, 47, 'Unit Layanan Pelanggan KUPRIK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(694, 986, 47, 'Unit Layanan Pelanggan KURIK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(695, 987, 47, 'Unit Layanan Pelanggan TANAH MERAH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(696, 989, 47, 'Unit Layanan Transmisi dan Gardu Induk INDUK JAYAPURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(697, 990, 47, 'Unit Layanan  PLTA ORYA GENYEM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(698, 991, 47, 'Unit Layanan  PLTD JAYAPURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(699, 992, 47, 'Unit Layanan  PLTD SORONG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(700, 993, 47, 'Unit Layanan  PLTD BIAK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(701, 994, 47, 'Unit Layanan  PLTD MANOKWARI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(702, 995, 47, 'Unit Layanan  PLTD MERAUKE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(703, 999, 48, 'Unit Layanan  PLTMH/S TANJUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(704, 1000, 48, 'Unit Layanan  PLTD AMPENAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(705, 1001, 48, 'Unit Layanan  PLTD PAOKMOTONG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(706, 1003, 48, 'Unit Layanan  PLTD TALIWANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(707, 1004, 48, 'Unit Layanan PelangganLTU SUMBAWA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(708, 1005, 48, 'Unit Layanan  PLTD BIMA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(709, 1006, 48, 'Unit Layanan  PLTD LABUAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(710, 1007, 48, 'Unit Layanan  PLTD DOMPU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(711, 1008, 48, 'Unit Layanan Transmisi dan Gardu Induk BIMA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(712, 1011, 48, 'Unit Layanan Pelanggan AMPENAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(713, 1012, 48, 'Unit Layanan Pelanggan CAKRANEGARA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(714, 1013, 48, 'Unit Layanan Pelanggan PRAYA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(715, 1014, 48, 'Unit Layanan Pelanggan PRINGGABAYA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(716, 1015, 48, 'Unit Layanan Pelanggan SELONG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(717, 836, 44, 'Unit Layanan Pelanggan TANJUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(718, 1018, 48, 'Unit Layanan Pelanggan DOMPU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(719, 1019, 48, 'Unit Layanan Pelanggan SAPE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(720, 1020, 48, 'Unit Layanan Pelanggan WOHA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(721, 1021, 48, 'Unit Layanan Pelanggan BIMA KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(722, 1023, 48, 'Unit Layanan Pelanggan ALAS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(723, 1024, 48, 'Unit Layanan Pelanggan TALIWANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(724, 1025, 48, 'Unit Layanan Pelanggan EMPANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(725, 1026, 48, 'Unit Layanan Pelanggan SAMAWA REA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(726, 1030, 49, 'Unit Layanan Pelanggan BAJAWA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(727, 1031, 49, 'Unit Layanan Pelanggan LABUAN BAJO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(728, 1032, 49, 'Unit Layanan Pelanggan RUTENG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(729, 1033, 49, 'Unit Layanan  PLTD ENDE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(730, 1035, 49, 'Unit Layanan Pelanggan ADONARA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(731, 1036, 49, 'Unit Layanan Pelanggan LARANTUKA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(732, 1037, 49, 'Unit Layanan Pelanggan LEMBATA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(733, 1038, 49, 'Unit Layanan  PLTD MAUMERE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(734, 1040, 49, 'Unit Layanan Pelanggan SUMBA BARAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(735, 1041, 49, 'Unit Layanan Pelanggan SUMBA TIMUR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(736, 1042, 49, 'Unit Layanan Pelanggan SUMBA JAYA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(737, 1043, 49, 'Unit Layanan  PLTD WAINGAPU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(738, 1045, 49, 'Unit Layanan Pelanggan KUPANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(739, 1046, 49, 'Unit Layanan Pelanggan OESAO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(740, 1047, 49, 'Unit Layanan Pelanggan SO\"E', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(741, 1048, 49, 'Unit Layanan Pelanggan KEFAMENANU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(742, 1049, 49, 'Unit Layanan Pelanggan ATAMBUA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(743, 1050, 49, 'Unit Layanan Pelanggan KALABAHI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(744, 1051, 49, 'Unit Layanan Pelanggan ROTE NDAO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(745, 1053, 49, 'Unit Layanan  PLTD KUPANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(746, 1054, 49, 'Unit Layanan  PLTD ATAMBUA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(747, 1055, 49, 'Unit Layanan  PLTP MATALOKO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(748, 1056, 49, 'Unit Layanan Transmisi dan Gardu Induk KUPANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(749, 1057, 49, 'Unit Layanan Transmisi dan Gardu Induk FLORES', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(750, 1062, 50, 'Unit Layanan Pelanggan TABING', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(751, 1063, 50, 'Unit Layanan Pelanggan BELANTI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(752, 1064, 50, 'Unit Layanan Pelanggan INDARUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(753, 1065, 50, 'Unit Layanan Pelanggan TUA PEJAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(754, 1066, 50, 'Unit Layanan Pelanggan PAINAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(755, 1067, 50, 'Unit Layanan Pelanggan BALAI SELASA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(756, 1068, 50, 'Unit Layanan Pelanggan LUBUK ALUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(757, 1069, 50, 'Unit Layanan Pelanggan SICINCIN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(758, 1070, 50, 'Unit Layanan Pelanggan PARIAMAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(759, 1071, 50, 'Unit Layanan Pelanggan SUNGAI PENUH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(760, 1072, 50, 'Unit Layanan Pelanggan KERSIK TUO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(761, 1073, 50, 'Unit Layanan Pelanggan KURANJI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(762, 1075, 50, 'Unit Layanan Pelanggan BUKITTINGGI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(763, 1076, 50, 'Unit Layanan Pelanggan BASO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(764, 1077, 50, 'Unit Layanan Pelanggan PADANG PANJANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(765, 1078, 50, 'Unit Layanan Pelanggan LUBUK BASUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(766, 1079, 50, 'Unit Layanan Pelanggan LUBUK SIKAPING', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(767, 1080, 50, 'Unit Layanan Pelanggan SIMPANG EMPAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(768, 1081, 50, 'Unit Layanan Pelanggan KOTO TUO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(769, 1083, 50, 'Unit Layanan Pelanggan SOLOK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(770, 1084, 50, 'Unit Layanan Pelanggan SSINGKARAK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(771, 1085, 50, 'Unit Layanan Pelanggan SILUNGKANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(772, 1086, 50, 'Unit Layanan Pelanggan SAWAH LUNTO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(773, 1087, 50, 'Unit Layanan Pelanggan SIJUNJUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(774, 1088, 50, 'Unit Layanan Pelanggan SITIUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(775, 1089, 50, 'Unit Layanan Pelanggan SUNGAI RUMBAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(776, 1090, 50, 'Unit Layanan Pelanggan KAYU ARO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(777, 1091, 50, 'Unit Layanan Pelanggan MUARA LABUH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(778, 1093, 50, 'Unit Layanan Pelanggan PAYAKUMBUH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(779, 1094, 50, 'Unit Layanan Pelanggan BATUSANGKAR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(780, 1095, 50, 'Unit Layanan Pelanggan LIMA PUnit Layanan UH KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(781, 1096, 50, 'Unit Layanan Pelanggan LINTAU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(782, 1100, 51, 'Unit Layanan Pelanggan KAMPAR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(783, 1101, 51, 'Unit Layanan Pelanggan LIPAT KAIN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(784, 1102, 51, 'Unit Layanan Pelanggan UJUNG BATU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(785, 1103, 51, 'Unit Layanan Pelanggan BANGKINANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(786, 1104, 51, 'Unit Layanan Pelanggan PANAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(787, 1105, 51, 'Unit Layanan Pelanggan PANGKALAN KERINCI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(788, 1106, 51, 'Unit Layanan Pelanggan PASIR PANGARAIAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(789, 1107, 51, 'Unit Layanan Pelanggan PEKANBARU KOTA BARAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(790, 1108, 51, 'Unit Layanan Pelanggan PEKANBARU KOTA TIMUR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(791, 1109, 51, 'Unit Layanan Pelanggan PERAWANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(792, 1110, 51, 'Unit Layanan Pelanggan RUMBAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(793, 1111, 51, 'Unit Layanan Pelanggan SIAK SRI INDRAPURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(794, 1112, 51, 'Unit Layanan Pelanggan SIMPANG TIGA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(795, 1114, 51, 'Unit Layanan  PLTD BAGAN BESAR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(796, 1115, 51, 'Unit Layanan Pelanggan BAGAN BATU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(797, 1116, 51, 'Unit Layanan Pelanggan BAGAN SIAPI API', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(798, 1117, 51, 'Unit Layanan Pelanggan BENGKALIS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(799, 1118, 51, 'Unit Layanan Pelanggan DUMAI KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(800, 1119, 51, 'Unit Layanan Pelanggan DURI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(801, 1120, 51, 'Unit Layanan Pelanggan SELAT PANJANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(802, 1122, 51, 'Unit Layanan Pelanggan BELAKANG PADANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(803, 1123, 51, 'Unit Layanan Pelanggan BINTAN CENTER', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(804, 1124, 51, 'Unit Layanan Pelanggan DABO SINGKEP', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(805, 1125, 51, 'Unit Layanan Pelanggan KIJANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(806, 1126, 51, 'Unit Layanan Pelanggan ANAMBAS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(807, 1127, 51, 'Unit Layanan Pelanggan NATUNA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(808, 1128, 51, 'Unit Layanan Pelanggan TANJUNG BALAI KARIMUN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(809, 1129, 51, 'Unit Layanan Pelanggan TANJUNG BATU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(810, 1130, 51, 'Unit Layanan Pelanggan TANJUNG PINANG KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(811, 1131, 51, 'Unit Layanan Pelanggan TANJUNG UBAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(812, 1133, 51, 'Unit Layanan  PLTD RENGAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(813, 1134, 51, 'Unit Layanan Pelanggan AIR MOLEK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(814, 1135, 51, 'Unit Layanan Pelanggan KUALA ENOK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(815, 1136, 51, 'Unit Layanan Pelanggan TALUK KUANTAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(816, 1137, 51, 'Unit Layanan Pelanggan TEMBILAHAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(817, 1138, 51, 'Unit Layanan Pelanggan RENGAT KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(818, 1142, 51, 'Unit Layanan  PLTD TANJUNG PINANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(819, 1143, 51, 'Unit Layanan PelangganLTU TANJUNG BALAI KARIMUN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(820, 1144, 51, 'Unit Layanan Transmisi dan Gardu Induk BINTAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(821, 1148, 52, 'Unit Layanan Pelanggan MANGGAR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(822, 1149, 52, 'Unit Layanan Pelanggan TANJUNG PANDAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(823, 1151, 52, 'Unit Layanan Pelanggan PANGKALPINANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(824, 1152, 52, 'Unit Layanan Pelanggan SUNGAILIAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(825, 1153, 52, 'Unit Layanan Pelanggan KOBA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(826, 1154, 52, 'Unit Layanan Pelanggan TOBOALI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(827, 1155, 52, 'Unit Layanan Pelanggan MENTOK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(828, 1157, 52, 'Unit Layanan  PLTD MERAWANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(829, 1158, 52, 'Unit Layanan  PLTD PILANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(830, 1161, 53, 'Unit Layanan Pelanggan RIVAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(831, 1162, 53, 'Unit Layanan Pelanggan AMPERA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(832, 1163, 53, 'Unit Layanan Pelanggan SUKARAMI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(833, 1164, 53, 'Unit Layanan Pelanggan KENTEN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(834, 1165, 53, 'Unit Layanan Pelanggan KAYU AGUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(835, 1166, 53, 'Unit Layanan Pelanggan SEKAYU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(836, 1167, 53, 'Unit Layanan Pelanggan MARIANA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(837, 1168, 53, 'Unit Layanan Pelanggan PANGKALAN BALAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(838, 1169, 53, 'Unit Layanan Pelanggan INDERALAYA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(839, 1170, 53, 'Unit Layanan Pelanggan TUGU MUnit Layanan YO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(840, 1172, 53, 'Unit Layanan Pelanggan NUSA INDAH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(841, 1173, 53, 'Unit Layanan Pelanggan TELUK SEGARA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(842, 1174, 53, 'Unit Layanan Pelanggan CURUP', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(843, 1175, 53, 'Unit Layanan Pelanggan KEPAHIANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(844, 1176, 53, 'Unit Layanan Pelanggan MUARA AMAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(845, 1177, 53, 'Unit Layanan Pelanggan TAIS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(846, 1178, 53, 'Unit Layanan Pelanggan ARGA MAKMUR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(847, 1179, 53, 'Unit Layanan Pelanggan MANNA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(848, 1180, 53, 'Unit Layanan Pelanggan BINTUHAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(849, 1181, 53, 'Unit Layanan Pelanggan MUKO MUKO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(850, 1183, 53, 'Unit Layanan Pelanggan KOTA BARU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(851, 1184, 53, 'Unit Layanan Pelanggan SEBERANG KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(852, 1185, 53, 'Unit Layanan Pelanggan TELANAI PURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(853, 1186, 53, 'Unit Layanan Pelanggan KUALA TUNGKAL', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(854, 1187, 53, 'Unit Layanan Pelanggan MUARA BUnit Layanan IAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(855, 1188, 53, 'Unit Layanan Pelanggan MUARA SABAK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(856, 1190, 53, 'Unit Layanan Pelanggan LEMBAYUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(857, 1191, 53, 'Unit Layanan Pelanggan LUBUK LINGGAU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(858, 1192, 53, 'Unit Layanan Pelanggan PAGAR ALAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(859, 1193, 53, 'Unit Layanan Pelanggan MUARA ENIM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(860, 1194, 53, 'Unit Layanan Pelanggan PRABUMUnit Layanan IH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(861, 1195, 53, 'Unit Layanan Pelanggan BATURAJA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(862, 830, 44, 'Unit Layanan Pelanggan MARTAPURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(863, 1197, 53, 'Unit Layanan Pelanggan MUARADUA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(864, 1198, 53, 'Unit Layanan Pelanggan MUARA BELITI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(865, 1199, 53, 'Unit Layanan Pelanggan PENDOPO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(866, 1200, 53, 'Unit Layanan Pelanggan TEBING TINGGI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(867, 1202, 53, 'Unit Layanan Pelanggan SAROLANGUN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(868, 1203, 53, 'Unit Layanan Pelanggan BANGKO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(869, 1204, 53, 'Unit Layanan Pelanggan MUARA TEBO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(870, 1205, 53, 'Unit Layanan Pelanggan RIMBO BUJANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(871, 1206, 53, 'Unit Layanan Pelanggan KOTA BUNGO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(872, 1215, 54, 'Unit Layanan Pelanggan SYIAH KUALA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(873, 1216, 54, 'Unit Layanan Pelanggan JANTHO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(874, 1217, 54, 'Unit Layanan Pelanggan KEUDE BIENG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(875, 1218, 54, 'Unit Layanan Pelanggan LAMBARO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(876, 1219, 54, 'Unit Layanan Pelanggan MERDUATI KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(877, 1220, 54, 'Unit Layanan Pelanggan SABANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(878, 1222, 54, 'Unit Layanan Pelanggan BLANG KEUJEREN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(879, 1223, 54, 'Unit Layanan Pelanggan IDI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(880, 1224, 54, 'Unit Layanan Pelanggan KUALA SIMPANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(881, 1225, 54, 'Unit Layanan Pelanggan KUTACANE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(882, 1226, 54, 'Unit Layanan Pelanggan PEUREUnit Layanan AK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(883, 1227, 54, 'Unit Layanan Pelanggan LANGSA KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(884, 1228, 54, 'Unit Layanan  PLTD KUNING', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(885, 1230, 54, 'Unit Layanan Pelanggan LHOKSEUMAWE KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(886, 1231, 54, 'Unit Layanan Pelanggan BIREUEN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(887, 1232, 54, 'Unit Layanan Pelanggan TAKENGON', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(888, 1233, 54, 'Unit Layanan Pelanggan JANARATA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(889, 1234, 54, 'Unit Layanan Pelanggan SAMALANGA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(890, 1235, 54, 'Unit Layanan Pelanggan LHOKSUKON', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(891, 1236, 54, 'Unit Layanan Pelanggan PANTON LABU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(892, 1237, 54, 'Unit Layanan Pelanggan KRUENG GEUKUEH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(893, 1238, 54, 'Unit Layanan Pelanggan GANDAPURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(894, 1239, 54, 'Unit Layanan Pelanggan GEUDONG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(895, 1240, 54, 'Unit Layanan Pelanggan MATANG GEUnit Layanan UMPANG DUA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(896, 1241, 54, 'Unit Layanan  PLTD AYANGAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(897, 1243, 54, 'Unit Layanan Pelanggan CALANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(898, 1244, 54, 'Unit Layanan Pelanggan JEURAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(899, 1245, 54, 'Unit Layanan Pelanggan MEUnit Layanan ABOH KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(900, 1246, 54, 'Unit Layanan Pelanggan SINABANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(901, 1247, 54, 'Unit Layanan Pelanggan TEUNOM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(902, 1248, 54, 'Unit Layanan  PLTD SEUNEBOK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(903, 1250, 54, 'Unit Layanan Pelanggan SIGLI KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(904, 1251, 54, 'Unit Layanan Pelanggan BEUREUNUN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(905, 1252, 54, 'Unit Layanan Pelanggan MEUREUDU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(906, 1254, 54, 'Unit Layanan Pelanggan SUBUnit Layanan USSALAM KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(907, 1255, 54, 'Unit Layanan Pelanggan SINGKIL', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(908, 1256, 54, 'Unit Layanan Pelanggan RIMO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(909, 1257, 54, 'Unit Layanan Pelanggan KOTA FAJAR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(910, 1258, 54, 'Unit Layanan Pelanggan TAPAK TUAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(911, 1259, 54, 'Unit Layanan Pelanggan LABUHAN HAJI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(912, 1260, 54, 'Unit Layanan Pelanggan BLANG PIDIE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(913, 1261, 54, 'Unit Layanan  PLTD SUAK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(914, 1266, 55, 'Unit Layanan Pelanggan BELAWAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(915, 1267, 55, 'Unit Layanan Pelanggan HELVETIA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(916, 1268, 55, 'Unit Layanan Pelanggan JOHOR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(917, 1269, 55, 'Unit Layanan Pelanggan LABUHAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(918, 1270, 55, 'Unit Layanan Pelanggan MEDAN BARU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(919, 1271, 55, 'Unit Layanan Pelanggan MEDAN KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(920, 1272, 55, 'Unit Layanan Pelanggan MEDAN SELATAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(921, 1273, 55, 'Unit Layanan Pelanggan MEDAN TIMUR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(922, 1274, 55, 'Unit Layanan Pelanggan SUNGGAL', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(923, 1276, 55, 'Unit Layanan Pelanggan BINJAI BARAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(924, 1277, 55, 'Unit Layanan Pelanggan BINJAI KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(925, 1278, 55, 'Unit Layanan Pelanggan BINJAI TIMUR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(926, 1279, 55, 'Unit Layanan Pelanggan BRASTAGI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(927, 1280, 55, 'Unit Layanan Pelanggan GEBANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(928, 1281, 55, 'Unit Layanan Pelanggan KABAN JAHE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(929, 1282, 55, 'Unit Layanan Pelanggan KUALA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(930, 1283, 55, 'Unit Layanan Pelanggan PANGKALAN BRANDAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(931, 1284, 55, 'Unit Layanan Pelanggan PANGKALAN SUSU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(932, 1285, 55, 'Unit Layanan Pelanggan SIDIKALANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(933, 1286, 55, 'Unit Layanan Pelanggan STABAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(934, 1287, 55, 'Unit Layanan Pelanggan TANJUNG PURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(935, 1288, 55, 'Unit Layanan Pelanggan TIGA BINANGA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(936, 1290, 55, 'Unit Layanan Pelanggan DELITUA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(937, 1291, 55, 'Unit Layanan Pelanggan GALANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(938, 1292, 55, 'Unit Layanan Pelanggan LUBUK PAKAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(939, 1293, 55, 'Unit Layanan Pelanggan MEDAN DENAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(940, 1294, 55, 'Unit Layanan Pelanggan PANCUR BATU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(941, 1295, 55, 'Unit Layanan Pelanggan PERBAUNGAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(942, 1296, 55, 'Unit Layanan Pelanggan SEI REMPAH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(943, 1297, 55, 'Unit Layanan Pelanggan TANJUNG MORAWA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(944, 1299, 55, 'Unit Layanan Pelanggan LIMA PUnit Layanan UH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(945, 1300, 55, 'Unit Layanan Pelanggan DOLOK MASIHUnit Layanan ', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(946, 1301, 55, 'Unit Layanan Pelanggan INDRA PURA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(947, 1302, 55, 'Unit Layanan Pelanggan KISARAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(948, 1303, 55, 'Unit Layanan Pelanggan PANGURURAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(949, 1304, 55, 'Unit Layanan Pelanggan PARAPAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(950, 1305, 55, 'Unit Layanan Pelanggan PERDAGANGAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(951, 1306, 55, 'Unit Layanan Pelanggan SIANTAR KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(952, 1307, 55, 'Unit Layanan Pelanggan SIDAMANIK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(953, 1308, 55, 'Unit Layanan Pelanggan TANAH JAWA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(954, 1309, 55, 'Unit Layanan Pelanggan TANJUNG TIRAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(955, 1200, 53, 'Unit Layanan Pelanggan TEBING TINGGI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(956, 1312, 55, 'Unit Layanan Pelanggan SIMPANG KAWAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(957, 1313, 55, 'Unit Layanan Pelanggan AEK KANOPAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(958, 1314, 55, 'Unit Layanan Pelanggan AEK KOTABATU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(959, 1315, 55, 'Unit Layanan Pelanggan AEK NABARA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(960, 1316, 55, 'Unit Layanan Pelanggan KOTA PINANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(961, 1317, 55, 'Unit Layanan Pelanggan LABUHAN BILIK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(962, 1318, 55, 'Unit Layanan Pelanggan RANTAU KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(963, 1319, 55, 'Unit Layanan Pelanggan TANJUNG BALAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(964, 1321, 55, 'Unit Layanan Pelanggan BALIGE', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(965, 1322, 55, 'Unit Layanan Pelanggan BARUS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(966, 1323, 55, 'Unit Layanan Pelanggan DOLOK SANGGUnit Layanan ', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(967, 1324, 55, 'Unit Layanan Pelanggan PORSEA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(968, 1325, 55, 'Unit Layanan Pelanggan SIBOLGA KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(969, 1326, 55, 'Unit Layanan Pelanggan SIBORONG BORONG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(970, 1327, 55, 'Unit Layanan Pelanggan TARUTUNG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(971, 1329, 55, 'Unit Layanan Pelanggan GUNUNG TUA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(972, 1330, 55, 'Unit Layanan Pelanggan KOTA NOPAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(973, 1331, 55, 'Unit Layanan Pelanggan NATAL', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(974, 1332, 55, 'Unit Layanan Pelanggan PENYABUNGAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(975, 1333, 55, 'Unit Layanan Pelanggan SIBUHUAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(976, 1334, 55, 'Unit Layanan Pelanggan SIDEMPUAN KOTA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(977, 1335, 55, 'Unit Layanan Pelanggan SIPIROK', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(978, 1337, 55, 'Unit Layanan Pelanggan NIAS BARAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(979, 1338, 55, 'Unit Layanan Pelanggan GUNUNG SITOLI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(980, 1339, 55, 'Unit Layanan Pelanggan TELUK DALAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(981, 1340, 55, 'Unit Layanan  PLTD GUNUNG SITOLI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(982, 1341, 55, 'Unit Layanan  PLTD TELUK DALAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(983, 1374, 62, 'Unit Layanan Transmisi dan Gardu Induk AUR DURI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(984, 1375, 62, 'Unit Layanan Transmisi dan Gardu Induk MUARA BUNGO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(985, 1377, 62, 'Unit Layanan Transmisi dan Gardu Induk MEUnit Layanan ABOH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(986, 1378, 62, 'Unit Layanan Transmisi dan Gardu Induk BANDA ACEH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(987, 1379, 62, 'Unit Layanan Transmisi dan Gardu Induk LANGSA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(988, 1381, 62, 'Unit Layanan Transmisi dan Gardu Induk NIAS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(989, 1382, 62, 'Unit Layanan Transmisi dan Gardu Induk GLUGUR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(990, 1383, 62, 'Unit Layanan Transmisi dan Gardu Induk PAYA PASIR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(991, 1384, 62, 'Unit Layanan Transmisi dan Gardu Induk SEI ROTAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(992, 1385, 62, 'Unit Layanan Transmisi dan Gardu Induk BINJAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(993, 1387, 62, 'Unit Layanan Transmisi dan Gardu Induk DOLOK SANGGUnit Layanan ', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(994, 1388, 62, 'Unit Layanan Transmisi dan Gardu Induk KISARAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(995, 1389, 62, 'Unit Layanan Transmisi dan Gardu Induk SIDIKALANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(996, 1390, 62, 'Unit Layanan Transmisi dan Gardu Induk SIBOLGA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(997, 1391, 62, 'Unit Layanan Transmisi dan Gardu Induk TOBA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(998, 1393, 62, 'Unit Layanan Transmisi dan Gardu Induk PAYAKUMBUH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(999, 1394, 62, 'Unit Layanan Transmisi dan Gardu Induk KILIRANJAO', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1000, 1395, 62, 'Unit Layanan Transmisi dan Gardu Induk PADANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1001, 1396, 62, 'Unit Layanan Transmisi dan Gardu Induk PARIAMAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1002, 1397, 62, 'Unit Layanan Transmisi dan Gardu Induk BUKIT TINGGI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1003, 1399, 62, 'Unit Layanan Transmisi dan Gardu Induk KERAMASAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1004, 1400, 62, 'Unit Layanan Transmisi dan Gardu Induk BOOM BARU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1005, 1401, 62, 'Unit Layanan Transmisi dan Gardu Induk BORANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1006, 1402, 62, 'Unit Layanan Transmisi dan Gardu Induk PRABUMUnit Layanan IH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1007, 1404, 62, 'Unit Layanan Transmisi dan Gardu Induk PAGELARAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1008, 1405, 62, 'Unit Layanan Transmisi dan Gardu Induk TARAHAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1009, 1406, 62, 'Unit Layanan Transmisi dan Gardu Induk TEGINENENG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1010, 1407, 62, 'Unit Layanan Transmisi dan Gardu Induk KOTA BUMI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1011, 1409, 62, 'Unit Layanan Transmisi dan Gardu Induk PASIR PUTIH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1012, 1410, 62, 'Unit Layanan Transmisi dan Gardu Induk DURI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1013, 1411, 62, 'Unit Layanan Transmisi dan Gardu Induk TELUK LEMBU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1014, 1413, 62, 'Unit Layanan Transmisi dan Gardu Induk BATURAJA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1015, 1414, 62, 'Unit Layanan Transmisi dan Gardu Induk LAHAT', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1016, 1415, 62, 'Unit Layanan Transmisi dan Gardu Induk PEKALONGAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1017, 1422, 64, 'Unit Layanan  PLTD TANJUNG KARANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1018, 1423, 64, 'Unit Layanan  PLTP/A TANGGAMUS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1019, 1424, 64, 'Unit Layanan  PLTD TEGINENENG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1020, 1425, 64, 'Unit Layanan  PLTA WAY BESAI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1021, 1427, 64, 'Unit Layanan  PLTA TES', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1022, 1428, 64, 'Unit Layanan  PLTA MUSI', '2018-11-07 02:02:36', '2018-11-07 02:02:36');
INSERT INTO `ma_rayon` (`id`, `id_ma_area`, `id_ma_unit`, `nama`, `created_at`, `updated_at`) VALUES
(1023, 1430, 64, 'Unit Layanan  PLTG/U INDERALAYA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1024, 1431, 64, 'Unit Layanan  PLTG/U KERAMASAN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1025, 1432, 64, 'Unit Layanan  PLTD/G MERAH MATA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1026, 1434, 64, 'Unit Layanan  PLTD/G PAYO SELINCAH', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1027, 1435, 64, 'Unit Layanan  PLTMG SUNGAI GELAM', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1028, 1445, 65, 'Unit Layanan  PLTG GLUGUR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1029, 1446, 65, 'Unit Layanan  PLTD TITI KUNING', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1030, 1447, 65, 'Unit Layanan  PLTG PAYA PASIR', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1031, 1449, 65, 'Unit Layanan  PLTA KOTO PANJANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1032, 1450, 65, 'Unit Layanan  PLTG TELUK LEMBU', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1033, 1451, 65, 'Unit Layanan  PLTG DURI', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1034, 1453, 65, 'Unit Layanan  PLTA SIPANSIHAPORAS', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1035, 1454, 65, 'Unit Layanan  PLTA RENUN', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1036, 1458, 65, 'Unit Layanan  PLTD COT TRUENG', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1037, 1459, 65, 'Unit Layanan  PLTD LUENG BATA', '2018-11-07 02:02:36', '2018-11-07 02:02:36'),
(1038, 1460, 65, 'Unit Layanan  PLTD PUnit Layanan O PISANG', '2018-11-07 02:02:36', '2018-11-07 02:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `ma_satuan`
--

CREATE TABLE `ma_satuan` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_satuan`
--

INSERT INTO `ma_satuan` (`id`, `label`, `created_at`, `updated_at`) VALUES
(1, 'Persen', '2018-10-22 20:22:43', '2018-10-22 20:22:43'),
(2, 'Hari', '2018-10-22 20:22:51', '2018-10-22 20:22:51'),
(3, 'kwh', '2018-10-23 00:26:06', '2018-10-23 00:26:15'),
(4, 'kali', '2018-10-23 00:27:13', '2018-10-23 00:27:13'),
(5, 'TWh', '2018-10-23 18:37:55', '2018-10-23 18:37:56'),
(6, 'Rupiah', '2018-10-23 18:40:03', '2018-10-23 18:40:03'),
(7, 'jumlah', '2018-10-23 18:59:03', '2018-10-23 18:59:03');

-- --------------------------------------------------------

--
-- Table structure for table `ma_tipe_laporan`
--

CREATE TABLE `ma_tipe_laporan` (
  `id` int(11) NOT NULL,
  `tipe_laporan` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_tipe_laporan`
--

INSERT INTO `ma_tipe_laporan` (`id`, `tipe_laporan`, `created_at`, `updated_at`) VALUES
(1, 'Harian', '2018-10-22 20:23:03', '2018-10-22 20:23:03'),
(2, 'Mingguan', '2018-10-22 20:23:10', '2018-10-22 20:23:10'),
(3, 'Bulanan', '2018-10-22 20:23:14', '2018-10-22 20:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `ma_unit`
--

CREATE TABLE `ma_unit` (
  `id` int(11) NOT NULL,
  `business_area` varchar(10) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `lat` double NOT NULL,
  `long` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_unit`
--

INSERT INTO `ma_unit` (`id`, `business_area`, `nama`, `lat`, `long`, `created_at`, `updated_at`) VALUES
(2, '6301', 'Wilayah Sumatera Barat', 0, 0, '2018-10-22 18:29:09', '2018-10-22 18:29:09'),
(5, '7501', 'Wilayah Maluku dan Maluku Utara', 0, 0, '2018-10-23 18:29:17', '2018-10-23 18:29:17'),
(6, '5601', 'Unit Induk Distribusi Banten', -6.376974, 105.828506, '2018-10-23 18:33:27', '2018-12-03 02:35:32'),
(7, '5001', 'UID JAKARTA RAYA', -6.3787137, 106.774124, '2018-11-06 21:24:53', '2018-12-03 02:34:58'),
(8, '5501', 'UID BALI', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(9, '1000', 'KANTOR PUSAT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(10, '4K00', 'UIP NUSRA', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(11, '4R00', 'UIP KALBAGBAR', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(12, '4I00', 'UIP KALBAGTENG', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(13, '4J00', 'UIP KALBAGTIM', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(14, '4A00', 'UIP KIT SUM', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(15, '4C00', 'UIP SUMBAGSEL', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(16, '4Q00', 'UIP SUMBAGTENG', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(17, '4B00', 'UIP SUMBAGUT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(18, '4D00', 'UIP ISJ', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(19, '4E00', 'UIP JBB', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(20, '4F00', 'UIP JBT I', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(21, '4P00', 'UIP JBT II', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(22, '4G00', 'UIP JBTB I', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(23, '4H00', 'UIP JBTB II', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(24, '4N00', 'UIP PAPUA', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(25, '4L00', 'UIP SUnit Layanan BAGUT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(26, '4M00', 'UIP SUnit Layanan BAGSEL', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(27, '4O01', 'UIP MALUKU', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(28, '3500', 'UIT JBT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(29, '3400', 'UIT JBB', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(30, '3600', 'UIT JBTB', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(31, '2400', 'UIKL KALIMANTAN', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(32, '2500', 'UIKL SUnit Layanan AWESI', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(33, '3300', 'UIP2B', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(34, '5600', 'UID BANTEN', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(35, '5400', 'UID JAYA', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(36, '5500', 'UID BALI', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(37, '5200', 'UID JATENG DAN DIY', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(38, '5100', 'UID JATIM', -7.250445, 112.768845, '2018-11-06 21:24:53', '2018-12-03 02:36:16'),
(39, '5300', 'UID JABAR', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(40, '6700', 'UID LAMPUNG', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(41, '7300', 'UIW SUnit Layanan UTTENGGO', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(42, '7400', 'UIW SUnit Layanan SELRABAR', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(43, '6800', 'UIW KALBAR', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(44, '7100', 'UIW KALSELTENG', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(45, '7200', 'UIW KALTIMRA', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(46, '7500', 'UIW MMU', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(47, '7600', 'UIW P2B', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(48, '7700', 'UIW NTB', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(49, '7800', 'UIW NTT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(50, '6300', 'UIW SUMBAR', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(51, '6400', 'UIW RIAU DAN KEPRI', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(52, '6600', 'UIW BABEL', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(53, '6500', 'UIW S2JB', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(54, '6100', 'UIW ACEH', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(55, '6200', 'UIW SUMUT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(56, '8200', 'PUSDIKLAT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(57, '8600', 'PUSERTIF', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(58, '8500', 'PUSMANPRO', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(59, '8300', 'PUSENLIS', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(60, '8100', 'PUSLITBANG', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(61, '8400', 'PUSHARLIS', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(62, '3200', 'UIP3B SUMATERA', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(63, '2200', 'UIK TJB', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(64, '2100', 'UIK SUMBAGSEL', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53'),
(65, '2000', 'UIK SUMBAGUT', 0, 0, '2018-11-06 21:24:53', '2018-11-06 21:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_08_07_145904_add_table_cms_apicustom', 1),
(2, '2016_08_07_150834_add_table_cms_dashboard', 1),
(3, '2016_08_07_151210_add_table_cms_logs', 1),
(4, '2016_08_07_151211_add_details_cms_logs', 1),
(5, '2016_08_07_152014_add_table_cms_privileges', 1),
(6, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(7, '2016_08_07_152320_add_table_cms_settings', 1),
(8, '2016_08_07_152421_add_table_cms_users', 1),
(9, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(10, '2016_08_07_154624_add_table_cms_moduls', 1),
(11, '2016_08_17_225409_add_status_cms_users', 1),
(12, '2016_08_20_125418_add_table_cms_notifications', 1),
(13, '2016_09_04_033706_add_table_cms_email_queues', 1),
(14, '2016_09_16_035347_add_group_setting', 1),
(15, '2016_09_16_045425_add_label_setting', 1),
(16, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(17, '2016_10_01_141740_add_method_type_apicustom', 1),
(18, '2016_10_01_141846_add_parameters_apicustom', 1),
(19, '2016_10_01_141934_add_responses_apicustom', 1),
(20, '2016_10_01_144826_add_table_apikey', 1),
(21, '2016_11_14_141657_create_cms_menus', 1),
(22, '2016_11_15_132350_create_cms_email_templates', 1),
(23, '2016_11_15_190410_create_cms_statistics', 1),
(24, '2016_11_17_102740_create_cms_statistic_components', 1),
(25, '2017_06_06_164501_add_deleted_at_cms_moduls', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tr_lag`
--

CREATE TABLE `tr_lag` (
  `id` int(11) NOT NULL,
  `id_ma_lag` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `realisasi` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_lag`
--

INSERT INTO `tr_lag` (`id`, `id_ma_lag`, `tanggal`, `target`, `realisasi`, `created_at`, `updated_at`) VALUES
(115, 34, '2018-10-23', 80, 75, '2018-10-25 19:03:21', '2018-10-25 21:15:33'),
(116, 34, '2018-11-23', 90, 80, '2018-10-25 19:03:21', '2018-10-25 23:29:31'),
(117, 34, '2018-12-23', 100, 90, '2018-10-25 19:03:21', '2018-12-04 07:59:46');

-- --------------------------------------------------------

--
-- Table structure for table `tr_lead`
--

CREATE TABLE `tr_lead` (
  `id` int(11) NOT NULL,
  `id_ma_lead` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `realisasi` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_lead`
--

INSERT INTO `tr_lead` (`id`, `id_ma_lead`, `tanggal`, `target`, `realisasi`, `created_at`, `updated_at`) VALUES
(1, 8, '2018-10-01', 70, NULL, '2018-10-26 00:08:03', '2018-10-26 00:12:41'),
(2, 8, '2018-10-02', 75, NULL, '2018-10-26 00:08:03', '2018-10-26 00:12:54'),
(3, 8, '2018-10-03', 80, NULL, '2018-10-26 00:08:03', '2018-10-26 00:12:54'),
(4, 8, '2018-10-04', 85, NULL, '2018-10-26 00:08:03', '2018-10-26 00:12:54'),
(5, 8, '2018-10-05', 90, NULL, '2018-10-26 00:08:03', '2018-10-26 00:12:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds_map`
--
ALTER TABLE `ds_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_area`
--
ALTER TABLE `ma_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_lag`
--
ALTER TABLE `ma_lag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_lead`
--
ALTER TABLE `ma_lead`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_lm`
--
ALTER TABLE `ma_lm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_polarisasi`
--
ALTER TABLE `ma_polarisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_rayon`
--
ALTER TABLE `ma_rayon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_satuan`
--
ALTER TABLE `ma_satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_tipe_laporan`
--
ALTER TABLE `ma_tipe_laporan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ma_unit`
--
ALTER TABLE `ma_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_lag`
--
ALTER TABLE `tr_lag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_lead`
--
ALTER TABLE `tr_lead`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ds_map`
--
ALTER TABLE `ds_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ma_area`
--
ALTER TABLE `ma_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1461;

--
-- AUTO_INCREMENT for table `ma_lag`
--
ALTER TABLE `ma_lag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `ma_lead`
--
ALTER TABLE `ma_lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ma_lm`
--
ALTER TABLE `ma_lm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ma_polarisasi`
--
ALTER TABLE `ma_polarisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ma_rayon`
--
ALTER TABLE `ma_rayon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1039;

--
-- AUTO_INCREMENT for table `ma_satuan`
--
ALTER TABLE `ma_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ma_tipe_laporan`
--
ALTER TABLE `ma_tipe_laporan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ma_unit`
--
ALTER TABLE `ma_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tr_lag`
--
ALTER TABLE `tr_lag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `tr_lead`
--
ALTER TABLE `tr_lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
