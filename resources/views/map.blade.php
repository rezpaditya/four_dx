<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->

{{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> --}}

<style>
    td.details-control {
    background: url('{{ asset("details_open.png") }}') no-repeat center center;
    cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('{{ asset("details_close.png") }}') no-repeat center center;
    }

    #example { 
    border-collapse: separate; 
    border-spacing: 10px; 
    *border-collapse: expression('separate', cellSpacing = '10px');
    }
</style>

@if($st_u == 'unit')
    <div id="container" style="width:100%; height:400px;"></div>
    <br>
@endif

<!-- WIG Filter -->
<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapse1"><i class="fa fa-search"></i> Filter</a>
        </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
            <form method="post" action="{{route('get.index')}}" >
                @csrf
                @if($st_u == 'unit')
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select id="unit" name="unit" class="form-control select2" required>
                                <option value="">--Pilih Unit--</option>
                                @foreach ($select_unit as $u)
                                    <option value={{$u['id']}} {{($p_usr['unit']==$u['id'])?'selected':''}}>{{$u['nama']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-sm-3">
                        <select id="wig" name="wig" class="form-control select2" required>
                            <option value="">--Pilih WIG--</option>
                            @foreach ($wigs as $k => $wig)
                                <option class="{{$wig['id_ma_unit']}}" value={{$wig['id']}} {{($filter==$wig['id'])?'selected':''}}>{{$wig['nama']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button class="btn btn-success"><i class="fa fa-search"></i> Filter</button>
            </form>
        </div>
        </div>
    </div>
</div>
<!-- WIG Filter-End -->
<br>
@if($filter)
<div class="row">
    <div class="col-md-4" style="padding-left:0">
        <div id="container-speed" class="container"></div>
        <div id="container-polar" class="container">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a class="btn btn-success"><i class="fa fa-eye"></i> Lihat Detail</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div id="container-line" ></div>
    </div>
</div>
@endif
{{-- <div style="width:100%; height:25px;"></div>
<div class="row">
    <div class="col-md-12">
    <div class="panel">
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>WIG</th>
                    <th>Go to</th>
                </tr>
            </thead>
        </table>
    </div>
    </div>
</div> --}}

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<!-- ADD A PAGINATION -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

<script src="http://code.highcharts.com/maps/modules/map.js"></script>
{{-- <script src="https://code.highcharts.com/maps/highmaps.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.12/proj4-src.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>

<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="{{url('/assets/js/jquery.chained.min.js')}}"></script>

<style>
.container {
	width: 100%;
	float: left;
	height: 200px;
}
.highcharts-yaxis-grid .highcharts-grid-line {
	
}

@media (max-width: 600px) {
	.outer {
		width: 100%;
		height: 400px;
	}
	.container {
		width: 300px;
		float: none;
		margin: 0 auto;
	}

}
</style>

<script>

$("#wig").chained("#unit");

</script>

<script>
//MAP
Highcharts.mapChart('container', {
    chart: {
        map: 'countries/id/id-all'
    },

    title: {
        text: 'Strategic Map'
    },

    subtitle: {
        text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/id/id-all.js">Indonesia</a>'
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        },
        enableMouseWheelZoom: false
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: false
            },
            point: {
                events: {
                    click: function () {
                        location.href = '#' + this.name;
                    }
                }
            }
        }
    },

    tooltip: {
        headerFormat: '',
        pointFormat: '<b>{point.name}</b><br>Lat: {point.lat}, Lon: {point.lon}'
    },

    series: [{
            name: 'Basemap',
            borderColor: '#fff',
            nullColor: Highcharts.getOptions().colors[7],
            colors: Highcharts.getOptions().colors[5],
            showInLegend: false,
            },
            {
            type: 'mappoint',
            name: 'Cities',
            color: Highcharts.getOptions().colors[3],
            showInLegend: false,
            data: <?php echo json_encode($unit); ?>
            }]
});
</script>

<script>
//GAUGE
    var gaugeOptions = {

    chart: {
        type: 'solidgauge'
    },

    title: null,

    pane: {
        center: ['50%', '85%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },

    tooltip: {
        enabled: false
    },

    // the value axis
    yAxis: {
        stops: [
            [0.1, '#DF5353'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#55BF3B'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

// The speed gauge
var chartSpeed = Highcharts.chart('container-speed', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: 'Pencapaian WIG'
        }
    },

    credits: {
        enabled: false
    },

    exporting: { enabled: false },

    series: [{
        name: 'Speed',
        data: [<?php echo $chart['gauge']?>],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                   '<span style="font-size:12px;color:silver">%</span></div>'
        },
        tooltip: {
            valueSuffix: ' %'
        }
    }]

}));

</script>

<script>
//LINE CHART
Highcharts.chart('container-line', {

    title: {
        text: 'Progress Pencapaian WIG'
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Precentage'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    xAxis: {
      categories: <?php echo json_encode($chart['tanggal'])?>
    },

    series: <?php echo json_encode($chart['line'])?>,

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
</script>

<script>
// POLAR CHART
//     Highchart
</script>

<script>
function format ( d ) {
    console.log(d);
    var tbl = '<table cellpadding="5" cellspacing="10" border="0" style="margin-left:5.5%;">';
    var tbody = '';
    d.lead.forEach(element => {
        console.log(element.pencapaian);
        var indikator = '';
        if(element.pencapaian >= 20){
            indikator = '<td><img src="http://localhost:8000/like.png"/ width=25></td>';
        }else{
            indikator = '<td><img src="http://localhost:8000/sad.png"/ width=25></td>';
        }
        console.log(indikator);
        tbody = tbody +
                '<tr>'+
                    indikator+
                    '<td>&nbsp;&nbsp;&nbsp;</td>'+
                    '<td>'+element.nama+'</td>'+
                '</tr>'
    });

    tbl = tbl + tbody + '</table>';
    return  tbl;
}

$(document).ready(function() {
    var table = $('#example').DataTable( {
        "ajax": "http://localhost:8000/api/getWigLead/9",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "nama" },
            { "data": "goto" }
        ],
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bAutoWidth": false,
        "order": [[1, 'asc']]
    } );
     
    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );
</script>

@endsection