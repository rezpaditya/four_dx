@extends('crudbooster::admin_template')
@section('content')

 <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#wig" data-toggle="tab">WIG</a></li>
              <li><a href="#lead_measure" data-toggle="tab">Lead Measure</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="wig">
              <form id='form-table' method='post' action='/saveProgressWig' id="form_table">
                    <div class="box-body" id="parent-form-area">
                        <div class="form-group header-group-0 " id="form-group-nama" style="">
                            <label class="control-label col-sm-2">Pilih WIG</label>
                            <div class="col-sm-10">
                                <select class="form-control select2" name="id_ma_lag" id="nama_wig">
                                    <option>** Please select a WIG</option>
                                    @foreach($wig as $row)
                                    <option value='{{$row->id}}' data-tes='custome_attr'>{{$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div id='desc_wig' style="display:none">
                            <div class="form-group header-group-0 " id="" style="">
                                <label class="control-label col-sm-2">WIG</label>
                                <div class="col-sm-10">
                                    <label id='wig_nama'>-- Nama WIG --</label>
                                </div>
                            </div>
                            <div class="form-group header-group-0 " id="" style="">
                                <label class="control-label col-sm-2">Satuan</label>
                                <div class="col-sm-10">
                                    <label id='wig_satuan'>-- Satuan --</label>
                                </div>
                            </div>
                            <div class="form-group header-group-0 " id="" style="">
                                <label class="control-label col-sm-2">Polarisasi</label>
                                <div class="col-sm-10">
                                    <label id='wig_polarisasi'>-- Polarisasi --</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <input type='hidden' name='button_name' value=''/>
                    <input type='hidden' name='_token' value='{{csrf_token()}}'/>
                    <div class="box">
                        <div class="box-header">
                        <h3 class="box-title">Target dan Realisasi</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Target</th>
                                <th>Satuan</th>
                                <th>Realisasi</th>
                                <th>Satuan</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                <div class='panel-footer'>
                    <input type='submit' class='btn btn-success' value='Save changes'/>
                </div>
                </form><!--END FORM TABLE-->
                </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="lead_measure">
                <form id='form-table' method='post' action='/saveProgressLead'>
                    <div class="box-body" id="parent-form-area">
                            <div class="form-group header-group-0 " id="form-group-nama" style="">
                                <label class="control-label col-sm-2">Pilih WIG</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="id_ma_lead" id="nama_lead" data-search="true">
                                    <option>** Please select a WIG</option>
                                    @foreach($lead as $row)
                                    <option value='{{$row->id}}' data-tes='asd'>{{$row->nama}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                        <br><br>
                    <input type='hidden' name='button_name' value=''/>
                    <input type='hidden' name='_token' value='{{csrf_token()}}'/>
                    <div class="box">
                            <div class="box-header">
                            <h3 class="box-title">Target dan Realisasi</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Target</th>
                                <th>Realisasi</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                <div class='panel-footer'>
                    <input type='submit' class='btn btn-success' value='Save changes'/>
                </div>
                </form><!--END FORM TABLE-->
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>

@endsection